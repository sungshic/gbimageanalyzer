#!/bin/bash


# set here home directory where the jehep.jar file is located 
JEHEP_HOME=`pwd`

# set jython home
export JYTHON_HOME=$JEHEP_HOME"/lib/jython"
 
################## do not edit ###############################
JAVA_HEAP_SIZE=512
CLASSPATH=$JEHEP_HOME:$CLASSPATH


# Add in your .jar files first
for i in $JEHEP_HOME/lib/*/*.jar
do
      CLASSPATH="$i":$CLASSPATH
done


# convert the unix path to windows
if [ "$OSTYPE" = "cygwin32" ] || [ "$OSTYPE" = "cygwin" ] ; then
   CLASSPATH=`cygpath --path --windows "$CLASSPATH"`
fi


if [ -z $JAVA_HOME ]; then
        java -mx${JAVA_HEAP_SIZE}m -cp $CLASSPATH -Dpython.home=${JYTHON_HOME} -Djehep.home=$JEHEP_HOME jport.Main $1 &
else
        $JAVA_HOME/bin/java -mx${JAVA_HEAP_SIZE}m -cp $CLASSPATH -Dpython.home=${JYTHON_HOME} -Djehep.home=$JEHEP_HOME jport.Main $1 &
fi
