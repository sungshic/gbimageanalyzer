#!/bin/bash

REQUIRED_VERSION=7
VER=`java -version 2>&1 | grep "java version" | awk '{print $3}' | tr -d \" | awk '{split($0, array, ".")} END{print array[2]}'`


# check java version
if [[ $VER -lt $REQUIRED_VERSION ]]; then
    echo "-> Detected java version $VER"
    echo "-> Java version is lower than required  1.$REQUIRED_VERSION"
    echo "-> Consider using jHepWork 3.4 compiled using  Java 1.6. Exit now!"
    exit;
fi



# set here home directory where the jehep.jar file is located 
JEHEP_HOME=`pwd`

# set jython home
export JYTHON_HOME=$JEHEP_HOME"/lib/jython"
 
################## do not edit ###############################
JAVA_HEAP_SIZE=512
CLASSPATH=$JEHEP_HOME:$CLASSPATH


# Add in your .jar files first
for i in $JEHEP_HOME/lib/*/*.jar
do
      CLASSPATH="$i":$CLASSPATH
done


# convert the unix path to windows
if [ "$OSTYPE" = "cygwin32" ] || [ "$OSTYPE" = "cygwin" ] ; then
   CLASSPATH=`cygpath --path --windows "$CLASSPATH"`
fi

LIBJEHEP=$JEHEP_HOME"/lib/native/linux-i586/"
OS=`uname`
arch=`uname -m`


if [ "$OS" = "Darwin" ]; then
  echo "Running on Mac.."
  LIBJEHEP=$JEHEP_HOME"/lib/native/macosx/"
fi

if [ "$OS" == "Linux" ]; then
if  [ $arch = i386 ]; then
            LIBJEHEP=$JEHEP_HOME"/lib/native/linux-i586/"
            echo "Running on Linux i386 .."
     elif [ $arch = "i486" ]; then
            LIBJEHEP=$JEHEP_HOME"/lib/native/linux-i586/"
            echo "Running on Linux i486 .."
     elif [ $arch = "i586" ]; then
            LIBJEHEP=$JEHEP_HOME"/lib/native/linux-i586/"
            echo "Running on Linux i586 .."          
     elif [ $arch = "i686" ]; then
            LIBJEHEP=$JEHEP_HOME"/lib/native/linux-i586/"
            echo "Running on Linux i686 .."
     elif [ $arch = "x86_64" ]; then
            LIBJEHEP=$JEHEP_HOME"/lib/native/linux-amd64/"
            echo "Running on Linux x86_64 .."
     else
        echo "Unsupported Architecture"
fi
fi

# some computers may run 32 bit java even on 64 bit
if [ $arch = "x86_64" ]; then
  java -d64 -version > /tmp/log_jhepwork 2>&1
  javacheck=`cat /tmp/log_jhepwork`
  if `echo ${javacheck} | grep "not supported" 1>/dev/null 2>&1`
  then
    LIBJEHEP=$JEHEP_HOME"/lib/native/linux-i586/"
    echo "Running a 32-bit JVM "
  else
    echo "Running a 64-bit JVM " 
fi
fi




OPTJJ="-Djava.library.path=$LIBJEHEP -Dlog4j.configuration=$JEHEP_HOME/log4j.properties"
if [ -z $JAVA_HOME ]; then
        java -mx${JAVA_HEAP_SIZE}m -cp $CLASSPATH $OPTJJ -Dpython.home=${JYTHON_HOME} -Djehep.home=$JEHEP_HOME jehep.ui.mainGUI  $1 &
else
        $JAVA_HOME/bin/java -mx${JAVA_HEAP_SIZE}m -cp $CLASSPATH $OPTJJ -Dpython.home=${JYTHON_HOME} -Djehep.home=$JEHEP_HOME jehep.ui.mainGUI $1 &
fi
