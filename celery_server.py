from __future__ import absolute_import

from celery import Celery

#celery = Celery(main='ghostos.mpa_node.celery', broker='amqp://celery@localhost:5672', backend='amqp://', include=['ghostos.mpa_node.tasks_server'])
#celery = Celery(main='ghostos.mpa_node.celery', broker='amqp://', backend='amqp://', include=['celery_server_task'])
celery = Celery(main='ghostos.gbimageanalyzer.celery_server', broker='amqp://grabia:grabia@localhost', backend='rpc://', include=['celery_server_task'])

# Optional configuration, see the application user guide.
celery.conf.update(
        CELERY_TASK_RESULT_EXPIRES=3600,
)

#if __name__ == '__main__':
#        celery.start()
