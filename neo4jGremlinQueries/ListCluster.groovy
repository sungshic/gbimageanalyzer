// neo4j.groovy
//import org.neo4j.kernel.EmbeddedReadOnlyGraphDatabase
//db = new EmbeddedReadOnlyGraphDatabase('/usr/local/Cellar/neo4j/community-1.8-unix/libexec/data/graphbak1.db')
//g = new Neo4jGraph(db)
//g.config.set_logger(DEBUG)

def createImageNode(uuid, contentType, filepath, offsetX, offsetY, landmarkFilepath, metadataFilepath, timestamp) {
	imageNode = g.vertices.create(name="image")
	imageNode.neo4jId = imageNode.eid
	imageNode.uuid = uuid
	imageNode.contentType = contentType
	imageNode.filepath = filepath
//  imageNode.ijip = ijip
	imageNode.landmarkOffsetX = offsetX
	imageNode.landmarkOffsetY = offsetY
	imageNode.landmarkRefFilepath = landmarkFilepath
	imageNode.metadataFilepath = metadataFilepath
	imageNode.timestamp = timestamp
	imageNode.save()
	return imageNode
}

def removeEdgesByLabelStr(Object str) {
	results =
		g.E.has("label", str) 	// get all edges with the specified label
		.each{g.removeEdge(it)} // and remove them all
	return results
}

def removeANodeAndConnectedEdgesByVertexId(Object id) {
	vertex = g.v(id)
	g.removeVertex(vertex) // this calls Blueprints' removeVertex to remove the node and all the connected edges
}

def removeNodesAndConnectedEdgesByImageId(Object id) {
	g.v(id).in('belongsToImage').in('cluster_has_blob') // get all cluster vertices belonging to the image
	.each{g.removeVertex(it)} // remove all the clusters and connected edges

	g.v(id).in('belongsToImage') // get all blob vertices belonging to the image
	.each{g.removeVertex(it)} // remove all the blobs and connected edges
}

def removeInterFrameEdgesByImageId(Object id) {
	g.v(id).in('belongsToImage').bothE.has('label','at_t1_subsequently_coincides_at_t2') // get all the relevant edges
	.each{g.removeEdge(it)} // remove them all
	
	g.v(id).in('belongsToImage').in('cluster_has_blob').bothE.has('label','cluster_corresponds_to') // get all the relevant edges
	.each{g.removeEdge(it)} // remove them all
}

def removeIntraFrameEdgesByImageId(Object id) {
	g.v(id).in('belongsToImage').bothE.has('label','at_t1_subsequently_coincides_at_t2') // get all the relevant edges
	.each{g.removeEdge(it)} // remove them all
	
	g.v(id).in('belongsToImage').in('cluster_has_blob').bothE.has('label','cluster_corresponds_to') // get all the relevant edges
	.each{g.removeEdge(it)} // remove them all
}

def removeBlobNodesByImageId(Object id) {
	g.v(id).in('belongsToImage').bothE.has('label','at_t1_subsequently_coincides_at_t2') // get all the relevant edges
	.each{g.removeEdge(it)} // remove them all
	
	g.v(id).in('belongsToImage').in('cluster_has_blob').bothE.has('label','cluster_corresponds_to') // get all the relevant edges
	.each{g.removeEdge(it)} // remove them all
}

def listClusterMemberIdsByNodeId(id, maxClusteringDist) {
    results =
//            Find the Bacterium node and save it as u
           g.v(id).as('u').sideEffect{u = it}
//            Save the edge before the condition as l
              .bothE('at_t1_is_near_at_t1').sideEffect{l = it}
	      .bothV.sideEffect{x = it}
	      .filter{it != u && l.weight < maxClusteringDist && u != x}.dedup() // return hitheto unseen nodes connected with weights less than or equal to 10 regardless of the direction to which they are connected
	      .loop('u'){it != null && it.loops < 20}{it.object.id != id} // walk the graph till there is no new node
	      .id._()
    solutions = results.toList()
}

def listClusterByNodeId(id, maxClusteringDist) {
    results =
//            Find the Bacterium node and save it as u
           g.v(id).as('u').sideEffect{u = it}
//            Save the edge before the condition as l
              .bothE('at_t1_is_near_at_t1').sideEffect{l = it}
	      .bothV.sideEffect{x = it}
	      .filter{it != u && l.weight <= maxClusteringDist && u != x}.dedup() // return hitheto unseen nodes connected with weights less thanor equal to 10 regardless of the direction to which they are connected
	      .loop('u'){it != null && it.loops < 20}{it.object.id != id} // walk the graph till there is no new node
//	      .path //{it.id}
//.filter{it.label == 'at_t1_is_near_at_t1' & it.weight < 1500}
//            Save the edge to be filtered as x
//              .bothV.bothE.sideEffect{x = it}.filter{l != x}
//            Filter based on the "condition" closure
//              .filter {it.condition == null || this.evaluate("${it.condition}")}
//            Keep going until we find an ending
//              .bothV.filter{it != u}
//	      .loop('u'){it.loops < 5 && !it.object.id in [id]}
//.loop('u') {it != u && it.loops < 50}
//            Format our results
//            .paths {it.id}
    solutions = results.toList()

    return solutions
}

def listAllFluoImgNodesLinkedToBFImgNode(Object nodeId) {
	results =
		g.v(nodeId).out('bfImg_has_fluoImg')
	return results.toList()
}
		
def listAllClusterNodesByImgUUID(Object imgUUID) {
	results = 
		g.V.filter{it.name == 'image'}.filter{it.uuid == imgUUID}.out('image_has_cluster')

	return results.toList()
}

def listAllBlobNodesByClusterUUID(clusterUUID) {
	results = 
		g.V.filter{it.uuid == clusterUUID}.out('cluster_has_blob')

	return results.toList()
}

def listAllClusterNodesByExpIdImgIdx(Object expId, Object temporalRef) {
	
	results = 
		g.v(expId).in('belongsToExp') 
		//.filter{it.name == 'image' && it.contentType == 'BF' && it.timestamp == temporalRef}
		.filter{it.name == 'image' && it.timestamp == temporalRef}
		.in('belongsToImage')
		.in('cluster_has_blob').dedup()
	return results.toList()
}

def listAllTemporalRefsByExpUUIDImgIdx(Object exp_uuid) {
    results =
        g.V.filter{it.uuid == exp_uuid}.in('belongsToExp').filter{it.name == 'image'}
    return results.toList();
}

def listAllBlobNodesByExpUUIDImgIdx(Object exp_uuid, Object temporalRef) {
    results =
        g.V.filter{it.uuid == exp_uuid}.in('belongsToExp').filter{it.name == 'image' && it.timestamp.toInteger() == temporalRef}.in('belongsToImage')
    return results.toList();
}

def listAllBlobNodesByImgUUID(Object uuid) {
    results =
        g.V.filter{it.uuid == uuid}.in('belongsToImage') 
        //g.V.filter{it.uuid == 'fd66b9a6-9b78-482d-a0c4-8cd26615033a'}.in('belongsToImage') 
    return results.toList();
}

def getT2ImageByT1ImgUUID(t1imgUUID) {
    results =
        g.V.filter{it.uuid == t1imgUUID}.out('image_has_cluster').out('cluster_corresponds_to').in('image_has_cluster').dedup().uuid
        //g.V.filter{it.uuid == 'fd66b9a6-9b78-482d-a0c4-8cd26615033a'}.in('belongsToImage') 
    return results.toList();
}

def getT1ImageByT2ImgUUID(t2imgUUID) {
    results =
        g.V.filter{it.uuid == t2imgUUID}.out('image_has_cluster').in('cluster_corresponds_to').in('image_has_cluster').dedup().uuid
        //g.V.filter{it.uuid == 'fd66b9a6-9b78-482d-a0c4-8cd26615033a'}.in('belongsToImage') 
    return results.toList();
}

def getNodeByUUID(Object uuid) {
    results =
        g.V.filter{it.uuid == uuid} 
        //g.V.filter{it.uuid == 'fd66b9a6-9b78-482d-a0c4-8cd26615033a'}.in('belongsToImage') 
    return results.toList();
}

def getClusterByBlobUUID(Object blobuuid) {
    results =
        g.V.filter{it.uuid == blobuuid}.in('cluster_has_blob') 
        //g.V.filter{it.uuid == 'fd66b9a6-9b78-482d-a0c4-8cd26615033a'}.in('belongsToImage') 
    return results.toList();
}

def getBlobNodeByEID(Object eid) {
    results =
        g.V.filter{it.name == 'blob' && it.neo4jId == eid} 
        //g.V.filter{it.uuid == 'fd66b9a6-9b78-482d-a0c4-8cd26615033a'}.in('belongsToImage') 
    return results.toList();
}

def getExpNodeByImgUUID(Object uuid) {
    results =
        g.V.filter{it.uuid == uuid}.out('belongsToExp')
        //g.V.filter{it.uuid == 'fd66b9a6-9b78-482d-a0c4-8cd26615033a'}.in('belongsToImage') 
    return results.toList();
}

def getExpNodeByImageId(Object neo4jId) {
    results =
        g.V.filter{it.neo4jId == neo4jId}.out('belongsToExp') 
        //g.V.filter{it.uuid == 'fd66b9a6-9b78-482d-a0c4-8cd26615033a'}.in('belongsToImage') 
    return results.toList();
}

def getStatsNodesByImgUUID(Object uuid) {
    results =
        g.V.filter{it.uuid == uuid}.out('image_has_stats') 
        //g.V.filter{it.uuid == 'fd66b9a6-9b78-482d-a0c4-8cd26615033a'}.in('belongsToImage') 
    return results.toList();
}

def getLinkedImageNodeByUUID(Object uuid) {
    results =
        g.V.filter{it.uuid == uuid}.out('bfImg_has_fluoImg') 
        //g.V.filter{it.uuid == 'fd66b9a6-9b78-482d-a0c4-8cd26615033a'}.in('belongsToImage') 
    return results.toList();
}

def getImageNodeByBlobUUID(blobUUID) {
    results =
	g.V.filter{it.uuid == blobUUID}.out('belongsToImage')

    return results;
}


// return blobs belonging to the image with the given uuid
        //g.V.has('uuid', uuid).in('belongsToImage') # return blobs belonging to the image with the given uuid
        //g.V.has('uuid', uuid).in('belongsToImage') # return blobs belonging to the image with the given uuid


//filter{it.name == 'image' && it.contentType == 'BF' && it.timestamp == temporalRef}
def listAllBlobNodesByExpIdImgIdx2(Object expId, Object temporalRef) {
    def _listAllBlobNodesByExpIdImgIdx = {
	    results = g.v(expId).in('belongsToExp').filter{it.name == 'image' && it.timestamp == temporalRef}.in('belongsToImage')
	    return results.toList();
	}
	def transaction = { final Closure closure ->
	    try {
	        results = closure();
	        return results;
	    } catch (e) {
	        throw e;
	    }
	}
	return transaction(_listAllBlobNodesByExpIdImgIdx);

}

def getCellStatsByUUID(cell_uuid) {
	results = 
		g.V.filter{it.uuid == cell_uuid}.out('blob_has_stats')
	return results.toList()
}

def listAllBlobStatsByClusterId(Object clusterNodeId) {
	
	results = 
		g.v(clusterNodeId)
		.out('cluster_has_blob')
		.out('blob_has_stats')
	return results.toList()
}

def listAllTemporalClusterPairsByExpIdImgRef(Object expId, Object temporalRef) {
// this function returns a list of vertices paired up in a running order. 
// For example, vertex at idx 0 is paired up with the one at idx 1, 2 with 3, 3, with 4, ... etc.
// This way of serialization is due to the limitations current gremlin-groovy to python binding via bulbflow
// bulbflow implementation only allows vertices and edges as return types. 
	results = 
		g.V.filter{it.name == 'cluster'}
		.filter{it.expRef == expId && it.imageRef == temporalRef}
		.out('cluster_corresponds_to')
		.path{}{it}{it}.scatter.filter{it != null}
		//.paths{}{it}{it}
	return results.toList()
}

// this function returns a list of vertices in a temporal order.
def listTemporalClusterChainByClusterNodeId(Object clusterId, int n) {
	results =
		g.v(clusterId).out('cluster_corresponds_to').loop(1){it.loops <= n}{true}.path.reverse()._()[0].scatter
	return results.toList()
}

def listTemporalKeyClusterChainByClusterNodeId(Object clusterId, int n) {
// this function returns in a temporal order a list of vertices (of type cluster), which have fluorescence measurement.
	results =
		g.v(clusterId)
		.out('cluster_corresponds_to') // walk the temporally linked clusters
		.loop(1){it.loops <= n}{true} // loop for n times
		.path // compile the graph paths
		.reverse()._()[0].scatter // return the last item
		.filter{it.out('cluster_has_blob').out('belongsToImage').out('bfImg_has_fluoImg').count() > 0} // filter only those with fluorescence measurement

	return results.toList()
}

def findColocalizedClusterSets(clusterUUID) {
	//results = g.V.filter{it.name=='cluster'}.filter{it.uuid==uuid}.out('cluster_corresponds_to').loop(1){it.loops < maxChainLen}{it.object.name=='cluster'}.sort{it.timestamp}.sort{it.weight}

	x = []; // colocalized cluster set at time t1
	y = []; // colocalized cluster set at time t2
	// the cluster set x temporally corresponds to the cluster set y
	g.V.filter{it.uuid==clusterUUID}.as('x').out('cluster_corresponds_to').in('cluster_corresponds_to').dedup().loop('x'){true}{true}.sort()._().store(x).out('cluster_corresponds_to').dedup().store(y).iterate();

	return [x.uuid, y.uuid] 
}

def findCorrespondingClusterChain(maxChainLen, uuid) {
	//results = g.V.filter{it.name=='cluster'}.filter{it.uuid==uuid}.out('cluster_corresponds_to').loop(1){it.loops < maxChainLen}{it.object.name=='cluster'}.sort{it.timestamp}.sort{it.weight}

	//return results.toList()
	x=[]; // initialize list
	g.V.filter{it.uuid==uuid}.store(x).next(); // add the current cluster to the list
	g.V.filter{it.uuid==uuid}.out('cluster_corresponds_to').loop(1){it.loops < maxChainLen}{it.object.name=='cluster'}.store(x).iterate();
	return x //.toList()
}

def findCorrespondingBlobChain(maxChainLen, uuidlist) {
// returns top a maximum of 'maxChainLen' vertices connected via 'at_t1_subsequently_coincides_at_t2' edges to the vertice with uuid
	
//	results = g.V.filter{it.uuid in uuidlist.toArray()}.out('at_t1_temporally_corresponds_at_t2').loop(1){it.loops <= maxChainLen}{true}._().filter{it.outE.has('label','at_t1_temporally_corresponds_at_t2').hasNext() == false}.path
results = g.V.filter{it.uuid in uuidlist.toArray()}.
outE('at_t1_temporally_corresponds_at_t2').has('weight').inV._().loop(4){it.loops <= maxChainLen}{true}._().filter{it.outE.has('label','at_t1_temporally_corresponds_at_t2').hasNext() == false}.path
//._().groupBy{it}{it.outE('at_t1_subsequently_coincides_at_t2').sort{-it.weight}._()[0..1].inV}.cap._().next()
////	results = g.v([2579, 2605].toArray())._().groupBy{it}{it.outE('at_t1_subsequently_coincides_at_t2').sort{-it.weight}._()[0..1].inV}.cap._().next()

	return results.toList()
}


def findCorrespondingBlob(Object howMany, Object vIds) {
// returns top 'howMany' vertices connected via 'at_t1_subsequently_coincides_at_t2' edges to the vertices in the list 'vIds'
	def testArr = [2579, 2572]
	println 'there!'
	//println vIds
	//return vIds

	//def m = [:]

	
	results = g.v(vIds.toArray())._().groupBy{it}{it.outE('at_t1_subsequently_coincides_at_t2').sort{-it.weight}._()[0..1].inV}.cap._().next()
////	results = g.v([2579, 2605].toArray())._().groupBy{it}{it.outE('at_t1_subsequently_coincides_at_t2').sort{-it.weight}._()[0..1].inV}.cap._().next()

	return results.values()._().scatter
	//results = g.v(2579).outE('at_t1_subsequently_coincides_at_t2').sort{-it.weight}._()[0..1].inV.toList()
	
	//return results.toList()
	//return results.values()._().gather.toList()[0]

	// return results.toList() //testArr.toList() // m

//.toList() //get(m.keySet().toList()[0]).toList()

	//results = 
	//	g.v(vIds.toArray())._().groupBy{it}{it.outE('at_t1_subsequently_coincides_at_t2').sort{-it.weight}._()[0..howMany-1].inV}.cap.scatter
	//return results.values()
}

def findConsensusCluster(Object vIds) {
// get the list of clusters in 'cluster_has_blob' relations to vertices with vIds, count repeat clusters, 
// sort in a descending order, to return the top consensus cluster

	def results = g.v(vIds.toArray())._().in('cluster_has_blob')._().groupCount().cap.next()

	return results.sort{-it.value}.keySet()._()[0..0]
}

def listMotherGenerationCells() {
	emitted_list = []
	not_emitted_list = []
	results = g.V.filter{it.nodetype=='blob'}.filter{it.inE.has('label','at_t1_temporally_corresponds_at_t2').count() == 0}.sideEffect{it.filter{it.out('at_t1_temporally_corresponds_at_t2').count() == 2}.count() > 0?it.path().store(not_emitted_list).next():false}.as('x').out('at_t1_temporally_corresponds_at_t2').loop('x'){it.object.outE.has('label', 'at_t1_temporally_corresponds_at_t2').count() == 1 & it.object.in('at_t1_temporally_corresponds_at_t2').outE.has('label',  'at_t1_temporally_corresponds_at_t2').count() <= 1}{it.object.ifThenElse{it.out('at_t1_temporally_corresponds_at_t2').count() == 2 & it.in('at_t1_temporally_corresponds_at_t2').out('at_t1_temporally_corresponds_at_t2').count() <= 1}{it.store(emitted_list).next() in emitted_list}{false}.equals(true._())}.path._() 
	//g.V.filter{it.uuid==uuid}.store(x).next(); // add the current cluster to the list
	//g.V.filter{it.uuid==uuid}.out('cluster_corresponds_to').loop(1){it.loops < maxChainLen}{it.object.name=='cluster'}.store(x).iterate();
	return results.toList() + not_emitted_list.toList() //.toList()
}

def listCellsWithStats() {
	results = g.V.filter{it.outE.has('label', 'blob_has_stats').count() != 0};

	return results.toList()
}

def listNextGenerationCells(uuidlist) {

	results = g.V.filter{it.uuid in uuidlist}.ifThenElse{it.out('at_t1_temporally_corresponds_at_t2').count() == 2}{it._()}{it.as('x').out('at_t1_temporally_corresponds_at_t2').loop('x'){it.object.out('at_t1_temporally_corresponds_at_t2').count() < 2}{it.object.out('at_t1_temporally_corresponds_at_t2').count() == 2}._()}.as('y').out('at_t1_temporally_corresponds_at_t2').loop('y'){it.object.out('at_t1_temporally_corresponds_at_t2').count() < 2}{it.object.out('at_t1_temporally_corresponds_at_t2').count() == 2}.path()

	return results.toList()
}

def stepBackNGenerations(cell_uuid, n_gens) {
	results = g.V.filter{it.uuid == cell_uuid}.as('y')._().as('x').in('at_t1_temporally_corresponds_at_t2').sideEffect{println it.neo4jId}.loop('x'){it.object.out('at_t1_temporally_corresponds_at_t2').count() < 2}{it.object.out('at_t1_temporally_corresponds_at_t2').count() == 2}.sideEffect{println it.neo4jId}.in('at_t1_temporally_corresponds_at_t2').sideEffect{println 'before loop y'; println it}._().loop('y'){it.loops < n_gens+1}{it.loops == n_gens +1}.sideEffect{println it.neo4jId}.ifThenElse{it.out('at_t1_temporally_corresponds_at_t2').count() == 1}{it.out('at_t1_temporally_corresponds_at_t2').next()}{it._()}.dedup()

	return results.toList()
}

def getPeers(cell_uuid) {
	results = g.V.filter{it.uuid == cell_uuid}.as('x').in('at_t1_temporally_corresponds_at_t2').loop('x'){it.object.out('at_t1_temporally_corresponds_at_t2').count() < 2}{it.object.out('at_t1_temporally_corresponds_at_t2').count() == 1}

	return results.toList()
}

def getCellStats(cuuid_list) {
	results = g.V.filter{it.uuid in cuuid_list}.out('blob_has_stats')

	return results.toList()
}

def saveGDBIntoFile(filepath) {
    outstream = new FileOutputStream(filepath)
    GraphSONWriter.outputGraph(g, outstream, GraphSONMode.EXTENDED)
}

def loadGDBFromFile(filepath) {
    instream = new FileInputStream(filepath)
    GraphSONReader.inputGraph(g, instream)
}

def clearGDB() {
    results = g.V.remove()
    return results
}
