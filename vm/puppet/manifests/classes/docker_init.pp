#include apt

class init {

        group { "puppet":
                ensure => "present"
        }

	# in order to avoid fully qualified paths to binaries
	#exec { path => ['/usr/bin'], }

        exec { "install hiera dependencies":
                command => "sudo gem install hiera hiera-puppet"
        }

	# add docker gpg key
	exec { "add the gpg key for docker v1.2":
		command => "sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 36A1D7869245C8950F966E92D8576A8BA88D21E9"
	}
	
	# add docker repo
	exec { "add docker repo for installing docker v1.2 on ubuntu 14.04":
		command => "sudo echo deb https://get.docker.com/ubuntu docker main >> /etc/apt/sources.list" #sources.list.d/docker.list"
	}
        #
        # install https dependency for apt modules
        exec { apt-https-dependency:
                command => "sudo /usr/bin/apt-get install -qqy apt-transport-https"
        }

        # update the apt-get repositories
        exec { update-apt:
                command => "sudo /usr/bin/apt-get -y update",
		require => Exec[apt-https-dependency]
        }

	#virtuqlenv
	#exec { pyenv:
	#	creates => "/ghostos/pyenv",
	#	command => "virtualenv /ghostos/pyenv",
	#	require => [Class['python'], Package['python-virtualenv']]
	#}

	# puppet modules
	#class { 'docker': }
	#class { 'rabbitmq': }
	#class { 'openvpn': }
	#class { 'python': 
	#	version => 'system',
	#	pip => true,
	#	dev => true,
	#	virtualenv => true,}
	
        # install apt-get module dependencies
        package {
                #["lxc-docker", "rabbitmq-server", "openvpn", "easy-rsa", "python-pip", "python-dev", "python-virtualenv"]:
        #        ["easy-rsa"]:
		["libdc1394-22-dev", "python-opencv", "python-matplotlib", "python-ipdb", "python-networkx", "maven","openjdk-7-jdk", "libpython2.7-dev", "libgeos-dev", "tmux", "ipython"]:
                ensure => installed,
                install_options => ['--force-yes'],
                require => Exec[update-apt] # the system update needs to run first
        }

        # update the apt-get repositories
        exec { install-bulb:
                command => "sudo pip install https://github.com/espeed/bulbs/tarball/master", # this installs bulb 
		require => Package['libpython2.7-dev']
        }

        # install "pip install" dependencies
        package {
		["celery", "cmd2", "mpldatacursor"]:
                ensure => installed,
                provider => pip,
		require => [Package['python-matplotlib']]
                #require => [Class['python']] #, Exec[pyenv]],
		#install_options => [ {'-e' => '/ghostos/pyenv'} ]
        }

}

