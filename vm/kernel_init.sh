#!/bin/sh

#OVPN_DATA="ovpn-data"

#echo `ifconfig eth0 2>/dev/null|awk '/inet addr:/ {print $2}'|sed 's/addr://'`
#ETH0_IP_ADDR=`ip route get 8.8.8.8 | awk 'NR==1 {print $NF}'`
OVPN_DATA="ovpn-data"
ETH0_IP_ADDR=`ip route get 8.8.8.8 | awk 'NR==1 {print $NF}'`
if [ ! -e /etc/_KERNEL_INIT ]; then
	# stop and remove any existing containers
	docker stop $(docker ps -a -q)
	docker rm $(docker ps -a -q)

	# build containers from Dockerfile
	docker build -t gbia .

	# create an ovpn data volume container
	#echo 'create' $OVPN_DATA 'volume container out of the busybox base image.'
	#docker run --name $OVPN_DATA -v /etc/openvpn busybox
	
	# copy easy-rsa keys and certificate config to the right place
	#echo 'copying easy-rsa settings to the openvpn config directory.'
	#docker run --volumes-from $OVPN_DATA --rm gos_kernel cp -r /ghostos/easy-rsa /etc/openvpn

	# initialize the ovpn data container for openvpn configuration files and certificates
	#echo 'initialize the' $OVPN_DATA 'container for openvpn configuration files and certificates.'
	#docker run --volumes-from $OVPN_DATA --rm gos_kernel ovpn_genconfig -u udp://${ETH0_IP_ADDR}:1194
	#echo 'run ovpn_initpki'
	#docker run --volumes-from $OVPN_DATA --rm -it gos_kernel ovpn_initpki

	#echo 'starting ovpn server...'
	#docker run --volumes-from $OVPN_DATA --rm gos_kernel ovpn_run
	
	# run and linke containers
	#docker run --volumes-from $OVPN_DATA -d -p 1194:1194/udp --privileged --name gos_kernel_instance gos_kernel:latest /bash/sh
	#docker run -d -p 1194:1194/udp --privileged --name gos_kernel_instance gos_kernel:latest /bin/bash
	docker run --privileged -e DISPLAY=192.168.59.3:0 -p 8182:8182 -it gbia:latest /bin/bash
	#docker run -d --name gosdev_mpa_node gosdev_mpa_node:latest

	# start docker container
	#docker start gos_kernel_instance

	#sudo touch /etc/_KERNEL_INIT
fi

