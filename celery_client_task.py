from __future__ import absolute_import

from gbimageanalyzer.celery_client import celery
import json
import pickle
import numpy as np
#from celery import celery
#from .test_condor_client import run_test

import subprocess
import logging

#import tifffile as tff
logger = logging.getLogger('grabia_app.gbimageanalyzer.celery_client_task')

@celery.task
def _run_test():
    print 'not implemented yet'
    return None

_run_test.name = 'ghostos.gbimageanalyzer.celery_server_task._run_test'

def run_test():
    return _run_test.delay()

@celery.task
def _doClearGDBData():
    pass

_doClearGDBData.name = 'ghostos.gbimageanalyzer.celery_server_task.doClearGDBData'

def doClearGDBData():
    _doClearGDBData.delay()

@celery.task
def _saveGDBIntoFile(exp_uuid):
    pass

_saveGDBIntoFile.name = 'ghostos.gbimageanalyzer.celery_server_task.saveGDBIntoFile'

def saveGDBIntoFile(exp_uuid):
    _saveGDBIntoFile.delay(json.dumps(exp_uuid))

@celery.task
def _loadFileToGDB(filepath):
    pass

_loadFileToGDB.name = 'ghostos.gbimageanalyzer.celery_server_task.loadFileToGDB'

def loadFileToGDB(filepath):
    _loadFileToGDB.delay(json.dumps(filepath))

@celery.task
def _getGDBFiles():
    pass

_getGDBFiles.name = 'ghostos.gbimageanalyzer.celery_server_task.getGDBFiles'

def getGDBFiles():
    task_handle = _getGDBFiles.delay()
    filelist = json.loads(task_handle.get()) # wait till celery task is done
    return filelist

@celery.task
def _createExperiment(exp_uuid, exp_title, metadata_filepath, exp_desc):
    pass
_createExperiment.name = 'ghostos.gbimageanalyzer.celery_server_task.createExperiment'
def createExperiment(exp_uuid, exp_title, metadata_filepath, exp_desc):
    logger.debug('entering createExperiment()')
    task_handle = _createExperiment.delay(json.dumps(exp_uuid), json.dumps(exp_title), json.dumps(metadata_filepath), json.dumps(exp_desc))
    exp_node = task_handle.get() # wait till celery task is done
    return json.loads(exp_node)

@celery.task
def _doProcessGDBDataSingle(exp_uuid, exp_data, previmg_uuid=None):
    pass

_doProcessGDBDataSingle.name = 'ghostos.gbimageanalyzer.celery_server_task.doProcessGDBDataSingle'

def doProcessGDBDataSingle(exp_uuid, exp_data, previmg_uuid=None):
    if previmg_uuid:
        previmg_uuid = json.dumps(previmg_uuid)
    task_handle = _doProcessGDBDataSingle.delay(json.dumps(exp_uuid), pickle.dumps(exp_data), previmg_uuid)
    previmg_uuid = task_handle.get() # wait till celery task is done
    return json.loads(previmg_uuid)


@celery.task
def _doProcessGDBData(exp_data):
    pass

_doProcessGDBData.name = 'ghostos.gbimageanalyzer.celery_server_task.doProcessGDBData'

def doProcessGDBData(exp_data):
    return _doProcessGDBData.delay(pickle.dumps(exp_data))


# @celery.task
# def _processInterFrameFeatureRelations(imgt1_uuid, imgt2_uuid):
#     pass
#
# _processInterFrameFeatureRelations.name = 'celery_server_task.processInterFrameFeatureRelations'
#
# def processInterFrameFeatureRelations(imgt1_uuid, imgt2_uuid):
#     return _processInterFrameFeatureRelations.delay(json.dumps(imgt1_uuid), json.dumps(imgt2_uuid))
#
#
# @celery.task
# def _processIntraFrameFeatureRelations(refimg_uuid, max_clustering_distance):
#     pass
#
# _processIntraFrameFeatureRelations.name = 'celery_server_task.processIntraFrameFeatureRelations'
#
# def processIntraFrameFeatureRelations(refimg_uuid, max_clustering_distance):
#     return _processIntraFrameFeatureRelations.delay(json.dumps(refimg_uuid), json.dumps(max_clustering_distance))


# @celery.task
# def _createImageBlobs(refimg_uuid, img_info_list, roi_stats, xoff, yoff, timestamp):
#     pass
#
# _createImageBlobs.name = 'celery_server_task.createImageBlobs'
#
# def createImageBlobs(refimg_uuid, img_info_list, roi_stats, xoff, yoff, timestamp):
#     return _createImageBlobs.delay(json.dumps(refimg_uuid), json.dumps(img_info_list), json.dumps(roi_stats), xoff, yoff, timestamp)


@celery.task
def _getMotherGenerationCells():
    pass

_getMotherGenerationCells.name = 'ghostos.gbimageanalyzer.celery_server_task.getMotherGenerationCells'

def getMotherGenerationCells():
    logger.debug('entering getMotherGenerationCells()')
    task_handle = _getMotherGenerationCells.delay()
    mcells = json.loads(task_handle.get()) # wait till celery task is done
    return mcells


@celery.task
def _getCellsWithStats():
    pass

_getCellsWithStats.name = 'ghostos.gbimageanalyzer.celery_server_task.getCellsWithStats'

def getCellsWithStats():
    logger.debug('entering getCellsWithStats()')
    task_handle = _getCellsWithStats.delay()
    celldictlist = json.loads(task_handle.get()) # wait till celery task is done
    return celldictlist

@celery.task
def _getCellStats(cur_gen):
    pass

_getCellStats.name = 'ghostos.gbimageanalyzer.celery_server_task.getCellStats'

def getCellStats(cur_gen):
    logger.debug('entering getCellStats()')
    task_handle = _getCellStats.delay(json.dumps(cur_gen))
    stats_lt = json.loads(task_handle.get()) # wait till celery task is done
    return stats_lt

@celery.task
def _getNextGenerationCells(cuuid_list):
    pass

_getNextGenerationCells.name = 'ghostos.gbimageanalyzer.celery_server_task.getNextGenerationCells'

def getNextGenerationCells(cuuid_list):
    logger.debug('entering getNextGenerationCells()')
    task_handle = _getNextGenerationCells.delay(json.dumps(cuuid_list))
    nth_gen_clist = json.loads(task_handle.get()) # wait till celery task is done
    return nth_gen_clist

@celery.task
def _getCNodeByUUID(uuid):
    pass
_getCNodeByUUID.name = 'ghostos.gbimageanalyzer.celery_server_task.getCNodeByUUID'
def getCNodeByUUID(uuid):
    logger.debug('entering getNodeByUUID('+str(uuid)+')')
    task_handle = _getCNodeByUUID.delay(json.dumps(uuid))
    cnode = json.loads(task_handle.get()) # wait till celery task is done
    return cnode


@celery.task
def _getSubCNodeList(cnode_uuid, weightlimit):
    pass
_getSubCNodeList.name = 'ghostos.gbimageanalyzer.celery_server_task.getSubCNodeList'
def getSubCNodeList(cnode_uuid, weightlimit):
    logger.debug('entering getSubCNodeList('+str(cnode_uuid)+','+str(weightlimit)+')')
    task_handle = _getSubCNodeList.delay(json.dumps(cnode_uuid), weightlimit)
    subcnode_list = json.loads(task_handle.get()) # wait till celery task is done
    return subcnode_list

@celery.task
def _getImageNodeByBlobUUID(buuid):
    pass
_getImageNodeByBlobUUID.name = 'ghostos.gbimageanalyzer.celery_server_task.getImageNodeByBlobUUID'
def getImageNodeByBlobUUID(buuid):
    logger.debug('entering getImageNodeByBlobUUID('+str(buuid)+')')
    task_handle = _getImageNodeByBlobUUID.delay(json.dumps(buuid))
    imgnode = json.loads(task_handle.get()) # wait till celery task is done
    return imgnode


@celery.task
def _getCellStatsByUUID(uuid):
    pass

_getCellStatsByUUID.name = 'ghostos.gbimageanalyzer.celery_server_task.getCellStatsByUUID'

def getCellStatsByUUID(uuid):
    logger.debug('entering getCellStatsByUUID()')
    task_handle = _getCellStatsByUUID.delay(json.dumps(uuid))
    cellstats = json.loads(task_handle.get()) # wait till celery task is done
    return cellstats

@celery.task
def _getTemporalCellChainsByInitCellUUIDs(uuid_list, chain_limit):
    pass
_getTemporalCellChainsByInitCellUUIDs.name = 'ghostos.gbimageanalyzer.celery_server_task.getTemporalCellChainsByInitCellUUIDs'
def getTemporalCellChainsByInitCellUUIDs(uuid_list, chain_limit):
    logger.debug('entering getTemporalCellChainsByInitCellUUIDs()')
    task_handle = _getTemporalCellChainsByInitCellUUIDs.delay(json.dumps(uuid_list), chain_limit)
    cell_chains = json.loads(task_handle.get()) # wait till celery task is done
    return cell_chains


@celery.task
def _getBlobUUIDsByExpImgIdx(exp_uuid, temporalidx):
    pass
_getBlobUUIDsByExpImgIdx.name = 'ghostos.gbimageanalyzer.celery_server_task.getBlobUUIDsByExpImgIdx'
def getBlobUUIDsByExpImgIdx(exp_uuid, temporalidx):
    logger.debug('entering getBlobUUIDsByExpImgIdx('+str(exp_uuid)+','+str(temporalidx)+')')
    task_handle = _getBlobUUIDsByExpImgIdx.delay(json.dumps(exp_uuid), temporalidx)
    uuid_list = json.loads(task_handle.get()) # wait till celery task is done
    return uuid_list

##############################################################################################
# cv2 task delegation to ghostos
# used pickle for serialization sincle json can't handle image array type
##############################################################################################
@celery.task
def _cv2EqualizeHistAndCvtGrayToRGB(img):
    pass
_cv2EqualizeHistAndCvtGrayToRGB.name = 'ghostos.gbimageanalyzer.celery_server_task.cv2EqualizeHistAndCvtGrayToRGB'
def cv2EqualizeHistAndCvtGrayToRGB(img):
    logger.debug('entering cv2EqualizeHistAndCvtGrayToRGB()')
    task_handle = _cv2EqualizeHistAndCvtGrayToRGB.delay(pickle.dumps(img))
    rgb_img = pickle.loads(task_handle.get()) # wait till celery task is done
    return rgb_img

@celery.task
def _cv2CropImg(img, pt1, pt2, frame_color, frame_thickness):
    pass
_cv2CropImg.name = 'ghostos.gbimageanalyzer.celery_server_task.cv2CropImg'
def cv2CropImg(img, pt1, pt2, frame_color, frame_thickness):
    logger.debug('entering cv2CropImg()')
    task_handle = _cv2CropImg.delay(pickle.dumps(img), pt1, pt2, frame_color, frame_thickness)
    rgb_img = pickle.loads(task_handle.get()) # wait till celery task is done
    return rgb_img

@celery.task
def _cv2ImDecode(img_buf):
    pass
_cv2ImDecode.name = 'ghostos.gbimageanalyzer.celery_server_task.cv2ImDecode'
def cv2ImReadGrayscale(filepath):
    logger.debug('entering cv2ImReadGrayscale()')
    with open(filepath, 'r') as f:
        img_buf = np.asarray(bytearray(f.read()), dtype=np.uint8)
        task_handle = _cv2ImDecode.delay(pickle.dumps(img_buf))
        cur_baseimg = pickle.loads(task_handle.get()) # wait till celery task is done
        return cur_baseimg
    return None


####################
# methods requiring access to the dot library
####################
@celery.task
def _nxGetGraphvizDotLayout(graph_data):
    pass
_nxGetGraphvizDotLayout.name = 'ghostos.gbimageanalyzer.celery_server_task.nxGetGraphvizDotLayout'
def nxGetGraphvizDotLayout(graph_data):
    logger.debug('entering nxGetGraphvizDotLayout()')
    task_handle = _nxGetGraphvizDotLayout.delay(pickle.dumps(graph_data))
    layout_pos = pickle.loads(task_handle.get()) # wait till celery task is done
    return layout_pos



