FROM ubuntu:14.04
MAINTAINER Sunny Park <sunny.park@ncl.ac.uk>

ENV DEBIAN_FRONTEND noninteractive

RUN mkdir /gbia
#RUN mkdir /root/.ssh/

# ghostos kernel
ADD . /gbia

# REPOS
RUN apt-get -y update
#RUN apt-get install -q -y puppetmaster librarian-puppet git curl python-virtualenv # puppet dependencies
RUN apt-get install -q -y puppet librarian-puppet git curl python-virtualenv # puppet dependencies
RUN puppet resource package puppet ensure=latest
RUN cp /gbia/vm/Puppetfile /
RUN librarian-puppet install --clean --verbose
RUN puppet module install puppetlabs-stdlib # this will install modules from Puppetfile
RUN puppet apply /gbia/vm/puppet/manifests/docker.pp --modulepath /modules # this will allow packages to be installed according to manifest files 

RUN FACTER_fqdn=localhost # && puppet apply /gbia/vm/puppet/manifests/docker.pp --verbose --detail --detailed-exitcodes

# Internally uses port 1194, remap using docker
#EXPOSE 1194/udp
EXPOSE 8182

RUN apt-get install -y -q python-software-properties
#RUN add-apt-repository -y "deb http://archive.ubuntu.com/ubuntu $(lsb_release -sc) universe"
#RUN add-apt-repository -y ppa:chris-lea/node.js
RUN apt-get -y update

# INSTALL
RUN apt-get install -y -q build-essential openssl libreadline6 libreadline6-dev curl git-core zlib1g zlib1g-dev libssl-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt-dev autoconf libc6-dev ncurses-dev automake libtool bison subversion pkg-config libmysqlclient-dev libpq-dev make wget unzip git vim nano nodejs mysql-client mysql-server gawk libgdbm-dev libffi-dev python-networkx

RUN pip install -r /gbia/requirements.txt
RUN git clone https://github.com/tinkerpop/rexster.git /gbia/rexster
RUN mvn clean install -f /gbia/rexster/pom.xml
RUN ln /dev/null /dev/raw1394 # this will suppress libdc1394 error: Failed to initialize libdc1394 upon importing cv2 in python

#RUN chmod +x /ghostos/local/bin/docker_init.sh
#RUN /ghostos/local/bin/docker_init.sh
#CMD ["dmsetup mknodes"] # this command makes it possible for docker to map another docker container's filesystem under /dev/mapper
#CMD ["/ghostos/local/bin/docker_init.sh"]
#CMD ["/bin/bash"]
#CMD ["service docker start"]
#CMD ["/start-server.sh"]
#CMD ["/bin/bash"]
