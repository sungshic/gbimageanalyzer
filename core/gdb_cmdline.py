#!/usr/bin/env python
# set virtual env
#activate_env_file = '/Users/spark/pyvirtualenv/env/bin/activate_this.py'
import os
#activate_env_file = str(os.path.realpath(os.path.dirname('${SCRIPT_PATH}'))) + '/../venv/bin/activate_this.py'
#execfile(activate_env_file, dict(__file__=activate_env_file))

import sys
####sys.path.insert(0, '/Users/spark/Documents/workspace/image_analysis/scripts/')
sys.path.append('gbimageanalyzer/core/')


import json
#import geojson
import pickle # for marshalling 

import getopt
import shapely # for geometric analysis
from shapely.geometry import Polygon, box
from shapely import affinity
#import gbimageanalyzer.core.dographdb as gdb
import dographdb as gdb
from temporalCorrespondences import setClusterNodes, setClusterNodesByImgUUID, setClusterCorrespondencesByUUID, setCorrespondingBlobClustersByUUID, setSingleCellCorrespondencesByGBIAClusters
import traceback
from os.path import expanduser
import logging
# create logger with 'grabia_app'
logger = logging.getLogger('gdb_cmdline')
logger.setLevel(logging.DEBUG)
# create file handler which logs even debug messages
logfile_dir = expanduser("~")+'/.grabia/log/'
logfile_name = 'gdb_cmdline.log'
if not os.path.exists(logfile_dir):
    os.makedirs(logfile_dir)
fh = logging.FileHandler(logfile_dir+logfile_name)
fh.setLevel(logging.DEBUG)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
# add the handlers to the logger
logger.addHandler(fh)
logger.addHandler(ch)
logger.debug('gdb_cmdline logger initialized')

def main(argv=None):
    if argv is None:
        argv = sys.argv
    gdbcmd = ''

    try:
        logger.debug('gdb_cmdline called')
        opts, args = getopt.getopt(argv[1:], "c:", ["cmd"])
        #print opts

        for opt, arg in opts:
            if opt in ("-c", "--cmd"):
                val = executeCmd(arg, argv[3:])
                #print 'val:'
                #print val
                #return val
                sys.stdout.write(json.dumps(val))
                sys.stdout.flush()
                sys.exit(0)
    except Exception:
        print(traceback.format_exc())
        print ("Usage: gdb_cmdline.py -c <cmd> [arguments]")

def gdb_issueUUID(argv):
    uuid = gdb.issueUUID()
    return json.dumps(uuid)

def gdb_updateImageNodeByUUID(argv):
    imgUUID = argv[0]
    newUUID = argv[1]
    contentType = argv[2]
    filepath = argv[3]
    width = argv[4]
    height = argv[5]
    offsetX = argv[6]
    offsetY = argv[7]
    landmarkFilepath = argv[8]
    metadataFilepath = argv[9]
    timestamp = argv[10]

    imageNode = gdb.getNodeByUUID(imgUUID)
    gdb.updateImageNode(imageNode, newUUID, contentType, filepath, width, height, offsetX, offsetY, landmarkFilepath, metadataFilepath, timestamp)

def gdb_createImageNode(argv):
    uuid = argv[0]
    contentType = argv[1]
    filepath = argv[2]
    width = argv[3]
    height = argv[4]
    offsetX = argv[5]
    offsetY = argv[6]
    landmarkFilepath = argv[7]
    metadataFilepath = argv[8]
    timestamp = argv[9]

    imageNode = gdb.createImageNode(uuid, contentType, filepath, width, height, offsetX, offsetY, landmarkFilepath, metadataFilepath, timestamp)


def gdb_createLinkedImageNodesForExp(argv):
    uuid1 = argv[0]
    uuid2 = argv[1]
    uuidExp = argv[2]
    contentType1 = argv[3]
    contentType2 = argv[4]
    filepath1 = argv[5]
    filepath2 = argv[6]
    #ijip = argv[7]
    xOff = int(argv[7])
    yOff = int(argv[8])
    landmarkFilepath = argv[9]
    metadataFilepath = argv[10]
    timestamp = int(argv[11])
    statsDict = json.loads(argv[12])

    expNode = gdb.getNodeByUUID(uuidExp)

    imageNode1 = gdb.createImageNode(uuid1, contentType1, filepath1, xOff, yOff, landmarkFilepath, metadataFilepath, timestamp)

    imageNode2 = gdb.createImageNode(uuid2, contentType2, filepath2, xOff, yOff, landmarkFilepath, metadataFilepath, timestamp)
    gdb.linkImageToExp(imageNode1, expNode)
    gdb.linkImageToExp(imageNode2, expNode)
    gdb.linkBFImageToFluoImage(imageNode1, imageNode2)

    # create fluorescence image stats node
    imageStatsNode = gdb.addImageStatsNode(imageNode2, statsDict, 'gfp background')

def gdb_linkBFImageToFluoImage(argv):
    bfImgUUID = argv[0]
    fluoImgUUID = argv[1]

    bfImgNode = gdb.getNodeByUUID(bfImgUUID)
    fluoImgNode = gdb.getNodeByUUID(fluoImgUUID)
    gdb.linkBFImageToFluoImage(bfImgNode, fluoImgNode)

def gdb_linkImageToExp(argv):
    imgUUID = argv[0]
    expUUID = argv[1]

    imgNode = gdb.getNodeByUUID(imgUUID)
    expNode = gdb.getNodeByUUID(expUUID)

    gdb.linkImageToExp(imgNode, expNode)

def gdb_getNodeByUUID(argv):
    logger.debug('entering gdb_getNodeByUUID()')
    uuid = argv[0]
    node = gdb.getNodeByUUID(uuid)

    if node:
        logger.debug('gdb_getNodeByUUID: gdb has a node under uuid '+str(uuid))
        return json.dumps(node._data) # serializes the dict structure containing all the properties the node has
    else:
        logger.debug('gdb_getNodeByUUID: gdb does not have a node under uuid '+str(uuid))
        return json.dumps(None)
    # not implemented yet. Currently, I don't see any need for this get method where a binary structure (ie. node) cannot be returned.

def gdb_getImageNodeByUUID(argv):
    uuid1 = argv[0]
    # not implemented yet. Currently, I don't see any need for this get method where a binary structure (ie. node) cannot be returned.

def gdb_getImageNodeByBlobUUID(argv):
    blobUUID = argv[0]
    node = gdb.getImageNodeByBlobUUID(blobUUID)

    if node:
        return json.dumps(node._data) # serializes the dict structure containing all the properties the node has
    else:
        return json.dumps(None)



def gdb_createBlobNodeForImage(argv):
    imageUUID = argv[0]
    blobUUID = argv[1]
    xOff = argv[2]
    yOff = argv[3]
    roicoords = argv[4]
    timestamp = argv[5]
    statslist = json.loads(argv[6])

    imageNode = gdb.getNodeByUUID(imageUUID)
    fluoImgNode = gdb.getLinkedImageNodeByUUID(imageUUID)

    blobNode = gdb.createBlobNode(blobUUID, xOff, yOff, roicoords, timestamp)
    gdb.linkBlobToImage(blobNode, imageNode)

    # for each channel and associated stats, add blob-stat-node
    for chstat in statslist:
        #gdb.addBlobStatsNode(imageStatsNode, blobNode, statsData, 'gfp')
        statuuid = chstat['statuuid']
        imageStatsNode = gdb.getNodeByUUID(statuuid)
        gdb.addBlobStatsNode(imageStatsNode, blobNode, chstat['stat'], chstat['ch'])
    return blobNode.eid

def gdb_createExperimentNode(argv):
    logger.debug('entering gdb_createExperimentNode()')
    nodeUUID = argv[0]
    expType = argv[1] # e.g. "Timelapse microscopy"
    metadataFilepath = argv[2]
    details = argv[3]

    expNode = gdb.createExperimentNode(nodeUUID, expType, metadataFilepath, details)
    logger.debug('gdb_createExperimentNode: experiment node created with uuid '+str(nodeUUID))
    return expNode.eid

##def gdb_createClusterNodeByImgUUID(argv):
##	clusterUUIDList = json.dumps(argv[0]) # get a list of UUIDs for blobs comprising a cluster
##	imgUUID = argv[1]
##	clusterIdx = argv[2]
##	clusterUUID = argv[3]
##
##	cluster = [gdb.getNodeByUUID(blobUUID) for blobUUID in clusterUUIDList] # get a list of cluster node structures from uuid
##
##	clusterNode = gdb.createClusterNodeByImgUUID(cluster, imgUUID, clusterIdx, clusterUUID)



def gdb_linkBlob_is_near_Blob(argv):
    blob1_eid = argv[0]
    blob2_eid = argv[1]
    weight = argv[2]
    blob1 = gdb.getBlobNodeByEID(blob1_eid)
    blob2 = gdb.getBlobNodeByEID(blob2_eid)

    gdb.linkBlob_is_near_Blob(blob1, blob2, weight)

def gdb_linkBlob_subsequently_coincides_Blob(argv):
    blob1_eid = argv[0]
    blob2_eid = argv[1]
    overlap = float(argv[2])
    blob1 = gdb.getBlobNodeByEID(blob1_eid)
    blob2 = gdb.getBlobNodeByEID(blob2_eid)

    gdb.linkBlob_subsequently_coincides_Blob(blob1, blob2, overlap)

#get a list blobs EIDs from experiment id and image sequence index
def gdb_getBlobsByExpImgIdx(argv):
    expId = int(argv[0]) # command line invoking through subprocess.call only allows strings as arguments
    imgIdx = int(argv[1])

    blobs =  gdb.getBlobsByExpImgIdx(expId, imgIdx)
    # convert blobs into a list blobs eid
    if blobs:
        blobmap_list = [b.map() for b in blobs]
    else:
        blobmap_list = []

    return json.dumps(blobmap_list)

#get a list blobs EIDs from image uuid
def gdb_getBlobsByImgUUID(argv):
    uuid = str(argv[0])
    #print 'debug this'

    blobs = gdb.getBlobsByImgUUID(uuid)
    # convert blobs into a list blobs eid
    if blobs:
        blobmap_list = [b.map() for b in blobs]
    else:
        blobmap_list = []
    #print 'debug'
    #print blobs_eids.__class__
    #print blobs_eids
    #print 'debug'

    return json.dumps(blobmap_list)

def gdb_getLinkedFluoImageNodes(argv):
    imgUUID = argv[0]

    fluoImgNodeDicts = gdb.getLinkedFluoImagesByUUID(imgUUID)

    return json.dumps(fluoImgNodeDicts)

def gdb_getLinkedFluoImageUUIDs(argv):
    imgUUID = argv[0]

    fluoImgUUIDs = gdb.getLinkedFluoImageUUIDs(imgUUID)

    return json.dumps(fluoImgUUIDs)


# link clusters of blobs in one images to corresponding clusters of blobs in the subsequence image
def gdb_linkCorrespondingBlobClustersByImgUUID(argv):
    uuid = argv[0]

    setCorrespondingBlobClustersByUUID(gdb, uuid)

# initializeClusterNodes for the image with the uuid
def gdb_setClusterNodesByImgUUID(argv):
    imgUUID = argv[0]
    maxClusteringDist = int(argv[1])

    setClusterNodesByImgUUID(gdb, imgUUID, maxClusteringDist)

def _getAreaFromPolygonBounds(bounds):
    width = bounds.maxx - bounds.minx
    height = bounds.maxy - bounds.miny

    return width*height

def gdb_processIntraFrameFeatureRelations(argv):
    imgUUID = argv[0]
    maxClusteringDist = int(argv[1])

    imgNode = gdb.getNodeByUUID(imgUUID)

    blobs = list(imgNode.inV('belongsToImage')) # get all blobs that belong to this imgNode

    cmpblobs = list(blobs)
    for blob1 in blobs:
        cmpblobs.remove(blob1)
        # deserialize the blob's polygonal coordinate points
        #roi1Coords = zip(*geojson.loads(blob1.roi)['coordinates']) # put xCoords in idx 0 yCoords in idx 1
        #roi1coordlist = zip(roi1Coords[0],roi1Coords[1])
        roi1coordlist = json.loads(blob1.roi)
        #roi1 = PolygonRoi(Polygon(roi1Coords[0], roi1Coords[1], len(roi1Coords[0])), Roi.POLYGON)
        roi1 = Polygon(roi1coordlist)
        for blob2 in cmpblobs: # [blob for blob in blobs if blob != blob1]:
            #roi2Coords = zip(*geojson.loads(blob2.roi)['coordinates']) # put xCoords in idx 0 yCoords in idx 1
            #roi2coordlist = zip(roi2Coords[0],roi2Coords[1])
            roi2coordlist = json.loads(blob2.roi)
            #roi2 = PolygonRoi(Polygon(roi2Coords[0], roi2Coords[1], len(roi2Coords[0])), Roi.POLYGON)
            roi2 = Polygon(roi2coordlist)

            #blob1Rect = roi1.getBounds()
            #blob2Rect = roi2.getBounds()
            #blob1Rect = roi1.bounds
            #blob2Rect = roi2.bounds
            unionRect = roi1.union(roi2).bounds #blob1Rect.createUnion(blob2Rect)

            #polygonUnionBounds = roi1.union(roi2).envelope.bounds

            # calculate the geometrical proximity measure between two rectangles
            #areaProximity = (unionRect.getWidth() * unionRect.getHeight()) - (blob1Rect.getWidth() * blob1Rect.getHeight() + blob2Rect.getWidth() * blob2Rect.getHeight())
            #areaProximity = _getAreaFromPolygonBounds(polygonUnionBounds) - ( _getAreaFromPolygonBounds(roi1.bounds) + _getAreaFromPolygonBounds(roi2.bounds) )

            polygonDistance = roi1.distance(roi2)
            # print 'Area proximity: ' + str(areaProximity)
            #pu = [blob1Rect.getX(), blob1Rect.getY()]
            #pv = [blob2Rect.getX(), blob2Rect.getY()]
            #dist = float(math.sqrt(sum(((a-b)**2 for a,b in zip(pu,pv))))) # find the distance between points pu & pv

            if (polygonDistance <= maxClusteringDist):
                gdb.linkBlob_is_near_Blob(blob1, blob2, polygonDistance)
                gdb.linkBlob_is_near_Blob(blob2, blob1, polygonDistance)

        # end of for
    # end of for

    # set cluster nodes
    setClusterNodesByImgUUID(gdb, imgUUID, maxClusteringDist)

# process geometrical relationships among blobs between different frames
def gdb_processInterFrameFeatureRelations(argv):
    imgt1UUID = argv[0]
    imgt2UUID = argv[1]
    ##imgNodeAt_t1 = gdb.getNodeByUUID(imgt1UUID)
    ##imgNodeAt_t2 = gdb.getNodeByUUID(imgt2UUID)
    ##blobs = list(imgNodeAt_t2.inV('belongsToImage')) # get all blobs that belong to this imageNode
    ##prevBlobs = list(imgNodeAt_t1.inV('belongsToImage'))
    ##listCounter = list(prevBlobs)
    ##ldOffX1 = int(imgNodeAt_t1.landmarkOffsetX)
    ##ldOffY1 = int(imgNodeAt_t1.landmarkOffsetY)
    ##ldOffX2 = int(imgNodeAt_t2.landmarkOffsetX)
    ##ldOffY2 = int(imgNodeAt_t2.landmarkOffsetY)
    ##for blob1 in prevBlobs:
    ##	listCounter.remove(blob1)
    ##	#roi1Coords = zip(*geojson.loads(blob1.roi)['coordinates']) # put xCoords in idx 0 yCoords in idx 1
    ##	roi1coordlist = json.loads(blob1.roi)
    ##	#roi1 = PolygonRoi(Polygon(roi1Coords[0], roi1Coords[1], len(roi1Coords[0])), Roi.POLYGON)
    ##	roi1 = Polygon(roi1coordlist)
    ##
    ##	#blob1Rect = roi1.getBounds()
    ##	#blob1Rect.translate(-ldOffX1, -ldOffY1)
    ##	roi1_aligned = shapely.affinity.translate(roi1, -ldOffX1, -ldOffY1)
    ##	for blob2 in blobs:
    ##		#roi2Coords = zip(*geojson.loads(blob2.roi)['coordinates']) # put xCoords in idx 0 yCoords in idx 1
    ##		roi2coordlist = json.loads(blob2.roi)
    ##		#roi2 = PolygonRoi(Polygon(roi2Coords[0], roi2Coords[1], len(roi2Coords[0])), Roi.POLYGON)
    ##		roi2 = Polygon(roi2coordlist)
    ##
    ##		#blob2Rect = roi2.getBounds()
    ##		#blob2Rect.translate(-ldOffX2, -ldOffY2)
    ##		roi2_aligned = shapely.affinity.translate(roi2, -ldOffX2, -ldOffY2)
    ##		if roi1_aligned.intersects(roi2_aligned):
    ##			overlapPolygon = roi1.intersection(roi2)
    ##			overlapSize = overlapPolygon.area #overlapRect.getWidth() * overlapRect.getHeight()
    ##			gdb.linkBlob_subsequently_coincides_Blob(blob1, blob2, overlapSize)

    # set temporal correspondence relationships between cluster at time t1 to those at time t2
    gbiaclist1, gbiaclist2 = setClusterCorrespondencesByUUID(gdb, imgt1UUID, imgt2UUID) #expId, imgNodeAt_t1.timestamp)
    setSingleCellCorrespondencesByGBIAClusters(gbiaclist1, gbiaclist2)

# end of def


# process geometrical relationships among blobs belonging to two subsequent images
def gdb_processGeoRelForBlobs(argv):
    curImageUUID = argv[0]
    prevImageUUID = argv[1]

    ##### process geometrical relationships among blobs
    #imageNode = gdb.getNodeByUUID(imageUUID)
    #blobs = list(imageNode.inV('belongsToImage'))  # get all blobs that belong to this imageNode
    blobs = gdb.getBlobsByImgUUID(curImageUUID)

    print 'Processing blobs in the same frame'
    print blobs
    cmpblobs = list(blobs)
    for blob1 in blobs:
        print len(cmpblobs)
        cmpblobs.remove(blob1)
        # sys.stdout.write(str(len(blobs)) + ' ')
        # perform deserialization of the object
        #roi1Coords = zip(*geojson.loads(blob1.roi)['coordinates'])  # put xCoords in idx 0 yCoords in idx 1
        #roi1 = PolygonRoi(Polygon(roi1Coords[0], roi1Coords[1], len(roi1Coords[0])), Roi.POLYGON)
        roi1 = Polygon(json.loads(blob1.roi)) # based on coords in format of a list of (x,y) tuples
        for blob2 in cmpblobs:  # [blob for blob in blobs if blob != blob1]:
            #roi2Coords = zip(*geojson.loads(blob2.roi)['coordinates'])  # put xCoords in idx 0 yCoords in idx 1
            #roi2 = PolygonRoi(Polygon(roi2Coords[0], roi2Coords[1], len(roi2Coords[0])), Roi.POLYGON)
            roi2 = Polygon(json.loads(blob2.roi))
            #blob1Rect = roi1.getBounds()
            b1bounds = roi1.bounds
            blob1Rect = box(b1bounds[0],b1bounds[1],b1bounds[2],b1bounds[3])

            #blob2Rect = roi2.getBounds()
            b2bounds = roi2.bounds
            blob2Rect = box(b2bounds[0],b2bounds[1],b2bounds[2],b2bounds[3])
            #unionRect = blob1Rect.createUnion(blob2Rect)
            unionRoi = roi1.union(roi2)
            #areaProximity = (unionRect.getWidth() * unionRect.getHeight()) - (blob1Rect.getWidth() * blob1Rect.getHeight() + blob2Rect.getWidth() * blob2Rect.getHeight())
            areaProximity = unionRoi.area - blob1Rect.area + blob2Rect.area

            # print 'Area proximity: ' + str(areaProximity)
            #pu = [blob1Rect.getX(), blob1Rect.getY()]
            #pv = [blob2Rect.getX(), blob2Rect.getY()]

            # dist = float(math.sqrt(sum(((a-b)**2 for a,b in zip(pu,pv))))) # find the distance between points pu & pv
            if (areaProximity <= 3000):
                gdb.linkBlob_is_near_Blob(blob1, blob2, areaProximity)

    print 'blobs processed'
    print cmpblobs
    print ''  # print newline

    if curImageUUID != '' and prevImageUUID != '': #idx > 0:
        print 'Processing subsequently coincidental blobs'
        blobs = gdb.getBlobsByImgUUID(curImageUUID) # get all blobs that belong to this imageNode
        prevBlobs = gdb.getBlobsByImgUUID(prevImageUUID)

        listCounter = list(prevBlobs)
        for blob1 in prevBlobs:
            print len(listCounter)
            listCounter.remove(blob1)
            #roi1Coords = zip(*geojson.loads(blob1.roi)['coordinates'])  # put xCoords in idx 0 yCoords in idx 1
            #roi1 = PolygonRoi(Polygon(roi1Coords[0], roi1Coords[1], len(roi1Coords[0])), Roi.POLYGON)
            roi1 = Polygon(json.loads(blob1.roi)) # based on coords in format of a list of (x,y) tuples
            for blob2 in blobs:
                #roi2Coords = zip(*geojson.loads(blob2.roi)['coordinates'])  # put xCoords in idx 0 yCoords in idx 1
                #roi2 = PolygonRoi(Polygon(roi2Coords[0], roi2Coords[1], len(roi2Coords[0])), Roi.POLYGON)
                roi2 = Polygon(json.loads(blob2.roi))
                #blob1Rect = roi1.getBounds()
                b1bounds = roi1.bounds
                blob1Rect = box(b1bounds[0],b1bounds[1],b1bounds[2],b1bounds[3])
                #blob1Rect.translate(-blob1.landmarkOffsetX, -blob1.landmarkOffsetY)
                blob1Rect = shapely.affinity.translate(blob1Rect, -int(blob1.landmarkOffsetX), -int(blob1.landmarkOffsetY))

                #blob2Rect = roi2.getBounds()
                b2bounds = roi2.bounds
                blob2Rect = box(b2bounds[0],b2bounds[1],b2bounds[2],b2bounds[3])
                #blob2Rect.translate(-blob2.landmarkOffsetX, -blob2.landmarkOffsetY)
                blob2Rect = shapely.affinity.translate(blob2Rect, -int(blob2.landmarkOffsetX), -int(blob2.landmarkOffsetY))
                if blob1Rect.intersects(blob2Rect):
                    overlapRect = blob1Rect.intersection(blob2Rect)
                    overlapSize = overlapRect.area
                    gdb.linkBlob_subsequently_coincides_Blob(blob1, blob2, overlapSize)
    # initialize cluster node
    setClusterNodesByImgUUID(gdb, curImageUUID)

    if prevImageUUID != '':
        setCorrespondingBlobClustersByUUID(gdb, prevImageUUID)
    # store ref to the current image node for next round
    prevImageUUID = curImageUUID

def gdb_addImageStatsNode(argv):
    imgUUID = argv[0]
    statUUID = argv[1]
    statsData = argv[2]
    statsType = argv[3]

    imgNode = gdb.getNodeByUUID(imgUUID)
    statsNodeDict = gdb.addImageStatsNode(imgNode, statUUID, statsData, statsType)

    return json.dumps(statsNodeDict)

def gdb_getExpNodeByImageUUID(argv):
    imgUUID = argv[0]

    expNode = gdb.getExpNodeByImageUUID(imgUUID)

    return json.dumps(expNode.map())

def gdb_getClusterList(argv):
    expNodeId = argv[0]
    tempIdx = argv[1]

    clist = gdb.getClusterList(expNodeId, tempIdx)

    cmap_list = [c.map() for c in clist]
    return json.dumps(cmap_list) # serialize

def gdb_getTemporalClusterChain(argv):
    cid = int(argv[0])
    chainlimit = int(argv[1])
    iskeyframesonly = bool(int(argv[2])) # int conversion was necessary, due to bool('False') returning True, a python oddity I must say

    if iskeyframesonly:
        clist = gdb.getTemporalKeyClusterChain(cid, chainlimit)
    else:
        clist = gdb.getTemporalClusterChain(cid, chainlimit)

    cmap_list = [c.map() for c in clist]
    return json.dumps(cmap_list) # serialize

def gdb_getTemporalCellChainsByInitCellUUIDs(argv):
    uuidlist = json.loads(argv[0])
    maxchainlen = int(argv[1])

    cellchains = gdb.doFindCorrespondingBlobChainsByUUIDs(uuidlist, maxchainlen)
    if cellchains:
        return json.dumps(cellchains)
    else:
        return json.dumps([])


def gdb_getClusterStats(argv):
    nodeid = int(argv[0])

    vlist = gdb.getClusterStats(nodeid)

    vmap_list = [v.map() for v in vlist]
    return json.dumps(vmap_list)

def gdb_getCellStatsByUUID(argv):
    cell_uuid = str(argv[0])

    cell_stats = gdb.getCellStatsByUUID(cell_uuid)

    cmap_list = [c.map() for c in cell_stats]
    return json.dumps(cmap_list)

def gdb_getClusterTimestamp(argv):
    cnode_uuid = str(argv[0])
    cnode = gdb.getNodeByUUID(cnode_uuid)

    timestamp = gdb.getClusterTimestamp(cnode)

    return json.dumps(timestamp)

gdb_cmds = {
    'issueUUID' : gdb_issueUUID,
    'updateImageNodeByUUID' : gdb_updateImageNodeByUUID,
    'createImageNode': gdb_createImageNode,
    'createLinkedImageNodesForExp' : gdb_createLinkedImageNodesForExp,
    'createBlobNodeForImage' : gdb_createBlobNodeForImage,
    'createExperimentNode' : gdb_createExperimentNode,
    'linkBFImageToFluoImage' : gdb_linkBFImageToFluoImage,
    'linkBlob_is_near_Blob' : gdb_linkBlob_is_near_Blob,
    'linkBlob_subsequently_coincides_Blob' : gdb_linkBlob_subsequently_coincides_Blob,
    'linkImageToExp' : gdb_linkImageToExp,
    'getBlobsByExpImgIdx' : gdb_getBlobsByExpImgIdx,
    'getBlobsByImgUUID' : gdb_getBlobsByImgUUID,
    'getLinkedFluoImageUUIDs' : gdb_getLinkedFluoImageUUIDs,
    'getNodeByUUID' : gdb_getNodeByUUID,
    'getLinkedFluoImageNodes' : gdb_getLinkedFluoImageNodes,
    'setClusterNodesByImgUUID' : gdb_setClusterNodesByImgUUID,
    'getImageNodeByBlobUUID' : gdb_getImageNodeByBlobUUID,
    'linkCorrespondingBlobClustersByImgUUID' : gdb_linkCorrespondingBlobClustersByImgUUID,
    'processGeoRelForBlobs' : gdb_processGeoRelForBlobs,
    'processIntraFrameFeatureRelations' : gdb_processIntraFrameFeatureRelations,
    'processInterFrameFeatureRelations' : gdb_processInterFrameFeatureRelations,
    'addImageStatsNode' : gdb_addImageStatsNode,

    'getExpNodeByImageUUID' : gdb_getExpNodeByImageUUID,
    'getClusterList' : gdb_getClusterList,
    'getTemporalClusterChain' : gdb_getTemporalClusterChain,
    'getTemporalCellChainsByInitCellUUIDs' : gdb_getTemporalCellChainsByInitCellUUIDs,
    'getClusterStats' : gdb_getClusterStats,
    'getClusterTimestamp' : gdb_getClusterTimestamp,
    'getCellStatsByUUID' : gdb_getCellStatsByUUID,
}

def executeCmd(cmdType, argv):
    return gdb_cmds[cmdType](argv)

if __name__ == "__main__":
    main()
