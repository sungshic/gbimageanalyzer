#import sys
#activate_env_file = '/Users/spark/pyvirtualenv/env/bin/activate_this.py'
#execfile(activate_env_file, dict(__file__=activate_env_file))
#from bulbs.neo4jserver import Graph, Config, NEO4J_URI
#g.scripts.update('/Users/spark/Documents/workspace/gbimageanalyzer/neo4jGremlinQueries/ListCluster.groovy')
#g.scripts.refresh()
import subprocess
import json
import re
import logging
import os

#cmd_header = ['python', 'gbimageanalyzer/core/gdb_cmdline.py', '-c']
#cmd_header = ['gbimageanalyzer/core/gdb_cmdline.py', '-c']
#cmd_header = ['gdb_cmdline', '-c']
script_path = str(os.path.realpath(os.path.dirname('${SCRIPT_PATH}')))
cmd_header = [script_path + '/../venv/bin/python', script_path + '/gbimageanalyzer/core/gdb_cmdline.py', '-c']
logger = logging.getLogger('grabia_app.gbimageanalyzer.core.gdb_jy_api')
logger.debug('gdb_jy_api initialized')
env_dict = dict(os.environ)
logger.debug(str(env_dict))
logger.debug('script_path: ' + str(os.path.realpath(os.path.dirname('${SCRIPT_PATH}'))))

def gdb_issueUUID():
    cmd_body = ['issueUUID']
    retval = subprocess.check_output(cmd_header + cmd_body)
    # the initial function call to gdb_cmdline.py is supposed to return a json encoded dict list, which must in turn be further encoded to include backslashes by the call to subprocess, resulting in a double json encoded string. So the json.loads call is made twice in the following.
    decodedval = json.loads(retval) # decode the json string into a json string without all the backslashes
    uuid = json.loads(decodedval) # decode the json string into a uuid string
    return uuid

def gdb_createImageNode(imgUUID, chName, filepath, width, height, offsetX, offsetY, landmarkFilepath, metadataFilepath, timestamp):
    cmd_body = ['createImageNode', imgUUID, chName, filepath, str(width), str(height), str(offsetX), str(offsetY), landmarkFilepath, metadataFilepath, str(timestamp)]
    return subprocess.call(cmd_header + cmd_body)

def gdb_updateImageNodeByUUID(imgUUID, newUUID, chName, filepath, width, height, offsetX, offsetY, landmarkFilepath, metadataFilepath, timestamp):
    cmd_body = ['updateImageNodeByUUID', imgUUID, newUUID, chName, filepath, str(width), str(height), str(offsetX), str(offsetY), landmarkFilepath, metadataFilepath, str(timestamp)]
    return subprocess.call(cmd_header + cmd_body)

def gdb_createLinkedImageNodesForExp(uuid1, uuid2, uuidExp, contentType1, contentType2, filepath1, filepath2, xOff, yOff, landmarkFilepath, metadataFilepath, timestamp, statsDict):
    cmd_body = ['createLinkedImageNodesForExp', uuid1, uuid2, uuidExp, contentType1, contentType2, filepath1, filepath2, str(xOff), str(yOff), landmarkFilepath, metadataFilepath, str(timestamp), json.dumps(statsDict)]
    subprocess.check_output(cmd_header + cmd_body)

def gdb_createExperimentNode(uuid, expType, metadataFilepath, details):
    logger.debug('jy entering gdb_createExperimentNode()')
    cmd_body = ['createExperimentNode', uuid, expType, metadataFilepath, details]
    retval = subprocess.call(cmd_header + cmd_body, env=env_dict)
    logger.debug('gdb_createExperimentNode: experiment node created with uuid '+str(uuid) + ' retval:' + str(retval))
    return retval

def gdb_createBlobNodeForImage(imgUUID, blobUUID, xOff, yOff, roi, timestamp, statslist):
    # xOff, yOff and timestamp are numbers
    # roi is in geojson format
    cmd_body = ['createBlobNodeForImage', imgUUID, blobUUID, str(xOff), str(yOff), roi, str(timestamp), json.dumps(statslist)]
    return subprocess.call(cmd_header + cmd_body)

def gdb_getBlobsByExpImgIdx(expId, imgIdx):
    cmd_body = ['getBlobsByExpImgIdx', str(expId), str(imgIdx)]
    retval = subprocess.check_output(cmd_header + cmd_body)
    decodedval = json.loads(retval) # decode the json string into a json string without all the backslashes
    nodedictlist = json.loads(decodedval) # decode the json string into dict list
    return nodedictlist

# receives a list of integers encoded in json string as retval
def gdb_getBlobsByImgUUID(uuid):
    cmd_body = ['getBlobsByImgUUID', uuid]
    retval = subprocess.check_output(cmd_header + cmd_body)
    decodedval = json.loads(retval) # decode the json string into a list of numbers
    nodedictlist = json.loads(decodedval) # decode the json string into dict list
    return nodedictlist

def gdb_getLinkedFluoImageNodes(bfImgUUID):
    cmd_body = ['getLinkedFluoImageNodes', bfImgUUID]
    retval = subprocess.check_output(cmd_header + cmd_body)
    # the initial function call to gdb_cmdline.py is supposed to return a json encoded dict list, which must in turn be further encoded to include backslashes by the call to subprocess, resulting in a double json encoded string. So the json.loads call is made twice in the following.
    decodedval = json.loads(retval) # decode the json string into a json string without all the backslashes
    nodedictlist = json.loads(decodedval) # decode the json string into dict list
    return nodedictlist

def gdb_getLinkedFluoImageUUIDs(bfImgUUID):
    cmd_body = ['getLinkedFluoImageUUIDs', bfImgUUID]
    retval = subprocess.check_output(cmd_header + cmd_body)
    decodedval = json.loads(retval) # decode the json string into a list of UUIDs
    return decodedval

def gdb_getNodeByUUID(uuid):
    cmd_body = ['getNodeByUUID', uuid]
    logger.debug('gdb_getNodeByUUID: calling with uuid '+str(uuid)+' '+str(cmd_header) + ' ' + str(cmd_body))
    retval = subprocess.check_output(cmd_header + cmd_body) #, env=env_dict)
    logger.debug('gdb_getNodeByUUID: subprocess returned with '+str(retval))
    # the initial function call to gdb_cmdline.py is supposed to return a json encoded dict, which must in turn be further encoded to include backslashes by the call to subprocess, resulting in a double json encoded string. So the json.loads call is made twice in the following.
    decodedval = json.loads(retval) # decode the json string into a json string without backslashes
    nodedict = json.loads(decodedval) # decode the json string into dict
    return nodedict

def gdb_getImageNodeByBlobUUID(blobUUID):
    cmd_body = ['getImageNodeByBlobUUID', blobUUID]
    retval = subprocess.check_output(cmd_header + cmd_body)
    # the initial function call to gdb_cmdline.py is supposed to return a json encoded dict, which must in turn be further encoded to include backslashes by the call to subprocess, resulting in a double json encoded string. So the json.loads call is made twice in the following.
    decodedval = json.loads(retval) # decode the json string into a json string without backslashes
    nodedict = json.loads(decodedval) # decode the json string into dict
    return nodedict


def gdb_linkBFImageToFluoImage(bfImgUUID, fluoImgUUID):
    cmd_body = ['linkBFImageToFluoImage', bfImgUUID, fluoImgUUID]
    return subprocess.call(cmd_header + cmd_body)

def gdb_linkBlob_is_near_Blob(blob1_eid, blob2_eid, areaProximity):
    cmd_body = ['linkBlob_is_near_Blob', blob1_eid, blob2_eid, str(areaProximity)]
    return subprocess.call(cmd_header + cmd_body)

def gdb_linkBlob_subsequently_coincides_Blob(blob1_eid, blob2_eid, overlapSize):
    cmd_body = ['linkBlob_subsequently_coincides_Blob', blob1_eid, blob2_eid, str(overlapSize)]
    return subprocess.call(cmd_header + cmd_body)

# link clusters of blobs in one images (uuid) to corresponding clusters of blobs in the temporally subsequence image
def gdb_linkCorrespondingBlobClustersByImgUUID(uuid):
    cmd_body = ['linkCorrespondingBlobClustersByImgUUID', uuid]
    #setCorrespondingBlobClustersByUUID(gdb, uuid1, uuid2)
    subprocess.call(cmd_header + cmd_body)

def gdb_linkImageToExp(imgUUID, expUUID):
    cmd_body = ['linkImageToExp', imgUUID, expUUID]
    #setCorrespondingBlobClustersByUUID(gdb, uuid1, uuid2)
    subprocess.call(cmd_header + cmd_body)

# initialize clusters of blobs for the image identified by uuid
def gdb_setClusterNodesByImgUUID(uuid):
    cmd_body = ['setClusterNodesByImgUUID', uuid]
    subprocess.call(cmd_header + cmd_body)

def gdb_processIntraFrameFeatureRelations(uuid, maxClusteringDist):
    cmd_body = ['processIntraFrameFeatureRelations', uuid, str(maxClusteringDist)]
    subprocess.call(cmd_header + cmd_body)

def gdb_processInterFrameFeatureRelations(imgt1uuid, imgt2uuid):
    cmd_body = ['processInterFrameFeatureRelations', imgt1uuid, imgt2uuid]
    subprocess.call(cmd_header + cmd_body)

# process geographical relationships among blobs between two images
def gdb_processGeoRelForBlobs(curImageUUID, prevImageUUID):
    cmd_body = ['processGeoRelForBlobs', curImageUUID, prevImageUUID]
    subprocess.call(cmd_header + cmd_body)

# add a node carrying statistics on an image
def gdb_addImageStatsNode(imgUUID, statUUID, statsData, statsType):
    cmd_body = ['addImageStatsNode', imgUUID, statUUID, statsData, statsType]
    #subprocess.call(cmd_header + cmd_body)
    retval = subprocess.check_output(cmd_header + cmd_body)
    # the initial function call to gdb_cmdline.py is supposed to return a json encoded dict list, which must in turn be further encoded to include backslashes by the call to subprocess, resulting in a double json encoded string. So the json.loads call is made twice in the following.
    decodedval = json.loads(retval) # decode the json string into a json string without all the backslashes
    nodedict = json.loads(decodedval) # decode the json string into a node dict
    return nodedict


# get the relevant experimental node to which an image with imgUUID belongs to
def gdb_getExpNodeByImageUUID(imgUUID):
    cmd_body = ['getExpNodeByImageUUID', imgUUID]
    #subprocess.call(cmd_header + cmd_body)
    retval = subprocess.check_output(cmd_header + cmd_body)
    # the initial function call to gdb_cmdline.py is supposed to return a json encoded dict list, which must in turn be further encoded to include backslashes by the call to subprocess, resulting in a double json encoded string. So the json.loads call is made twice in the following.
    decodedval = json.loads(retval) # decode the json string into a json string without all the backslashes
    nodedict = json.loads(decodedval) # decode the json string into a node dict
    return nodedict

# return all cluster nodes for [tmpIdx]th image sequence in the experiment 
# ID'ed by expNodeId
def gdb_getClusterList(expNodeId, tempIdx):
    cmd_body = ['getClusterList', str(expNodeId), str(tempIdx)]
    retval = subprocess.check_output(cmd_header + cmd_body)
    # check_output adds unwanted string in front of the actual string returned
    # by the python function 'getClusterList'. After much guess work, the unwanted
    # header string are the argument strings (expNodeId and tempIdx).
    # Very suspicious, as the header string interferes with the json marsharlling
    # a quick and dirty solution: use regular expression to filter the header string out
    ####retval = re.search('.+(\"\[.*\]\")', retval).group()
    # the initial function call to gdb_cmdline.py is supposed to return a json encoded dict list, which must in turn be further encoded to include backslashes by the call to subprocess, resulting in a double json encoded string. So the json.loads call is made twice in the following.
    # the root cause of these spurious retval was due to print statements made to stdout affecting the retval
    decodedval = json.loads(retval) # decode the json string into a json string without all the backslashes
    nodedictlist = json.loads(decodedval) # decode the json string into a node dict
    return nodedictlist

def gdb_getTemporalClusterChain(cid, chainlimit, iskeyframesonly):
    cmd_body = ['getTemporalClusterChain', str(cid), str(chainlimit), str(int(iskeyframesonly))]
    retval = subprocess.check_output(cmd_header + cmd_body)
    # check_output adds unwanted string in front of the actual string returned
    # by the python function 'getClusterList'. After much guess work, the unwanted
    # header string are the argument strings (expNodeId and tempIdx).
    # Very suspicious, as the header string interferes with the json marsharlling
    # a quick and dirty solution: use regular expression to filter the header string out
    #regsearch = re.search('.*(\"\[.+\]\")', retval)
    #if regsearch:
    #	retval = regsearch.group()
    # the root cause of these spurious retval was due to print statements made to stdout affecting the retval

    # the initial function call to gdb_cmdline.py is supposed to return a json encoded dict list, which must in turn be further encoded to include backslashes by the call to subprocess, resulting in a double json encoded string. So the json.loads call is made twice in the following.
    decodedval = json.loads(retval) # decode the json string into a json string without all the backslashes
    nodedictlist = json.loads(decodedval) # decode the json string into a node dict
    return nodedictlist

def gdb_getTemporalCellChainsByInitCellUUIDs(uuidlist, maxchainlen):
    cmd_body = ['getTemporalCellChainsByInitCellUUIDs', str(json.dumps(uuidlist)), str(maxchainlen)]
    retval = subprocess.check_output(cmd_header + cmd_body)

    decodedval = json.loads(retval) # decode the json string into a json string without all the backslashes
    chainlistdict = json.loads(decodedval) # decode the json string into a dict of chain lists
    return chainlistdict

def gdb_getCellStatsByUUID(cell_uuid):
    cmd_body = ['getCellStatsByUUID', str(cell_uuid)]
    retval = subprocess.check_output(cmd_header + cmd_body)

    decodedval = json.loads(retval) # decode the json string into a json string without all the backslashes
    cellstats = json.loads(decodedval) # decode the json string into a list containing a cell stats
    if len(cellstats) > 0:
        return cellstats[0]
    else:
        return None

def gdb_getClusterStats(nodeid):
    cmd_body = ['getClusterStats', str(nodeid)]
    retval = subprocess.check_output(cmd_header + cmd_body)
    # check_output adds unwanted string in front of the actual string returned
    # by the python function 'getClusterList'. After much guess work, the unwanted
    # header string are the argument strings (expNodeId and tempIdx).
    # Very suspicious, as the header string interferes with the json marsharlling
    # a quick and dirty solution: use regular expression to filter the header string out
    #regsearch = re.search('.*(\"\[.+\]\")', retval)
    #if regsearch:
    #	retval = regsearch.group()
    # the root cause of these spurious retval was due to print statements made to stdout affecting the retval

    # the initial function call to gdb_cmdline.py is supposed to return a json encoded dict list, which must in turn be further encoded to include backslashes by the call to subprocess, resulting in a double json encoded string. So the json.loads call is made twice in the following.
    decodedval = json.loads(retval) # decode the json string into a json string without all the backslashes
    nodedictlist = json.loads(decodedval) # decode the json string into a node dict
    return nodedictlist


def gdb_getClusterTimestamp(node_uuid):
    cmd_body = ['getClusterTimestamp', str(node_uuid)]
    retval = subprocess.check_output(cmd_header + cmd_body)
    timestamp = int(json.loads(json.loads(retval)))
    return timestamp

