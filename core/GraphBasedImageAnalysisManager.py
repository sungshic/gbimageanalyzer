import sys
import time
#sys.path.append('../lib/ij.jar')
#sys.path.append('../lib/AdaptiveThr')
sys.path.append('/grabia/gbimageanalyzer/lib/ij.jar')
#sys.path.append('/Applications/Fiji.app/plugins/AdaptiveThr') #/adaptiveThr_.class')
sys.path.append('/grabia/gbimageanalyzer/lib/OME/loci-common.jar')
#from ij import * 
#from ij.gui import *
#from ij.process import *
#from ij.measure import *
#from ij.util import *
#from ij.plugin import *
#from ij.plugin.filter import *
#from ij.plugin.frame import *
#from ij.io import *
#from java.lang import *
#from java.awt import *
#from java.awt.image import *
#from java.awt.geom import *
#from java.util import *
#from java.io import *

#sys.path.append('./jars/Micro-Manager/MMAcqEngine.jar')
#sys.path.append('./jars/Micro-Manager/MMCoreJ.jar')
#sys.path.append('./jars/Micro-Manager/MMJ_.jar')

#sys.path.append('./jars/OME/bio-formats.jar')
#sys.path.append('./lib/AdaptiveThr')
#sys.path.append('./jars/OME/ome-xml.jar')
#sys.path.append('./jars/OME/loci_plugins.jar')

#sys.path.append('~/Documents/workspace/GraphBasedImageAnalyser/jars/OME/loci_tools.jar')]
#activate_env_file = '/Users/spark/pyvirtualenv/env/bin/activate_this.py'
#execfile(activate_env_file, dict(__file__=activate_env_file))

import logging
#from loci.plugins.util import ImageProcessorReader
#from loci.formats import ChannelMerger, ChannelSeparator
from ij import IJ, ImageStack, VirtualStack, ImagePlus, CompositeImage
from ij.gui import Roi, PolygonRoi
from ij.plugin.filter import ParticleAnalyzer
from ij.plugin.frame import RoiManager
from ij.measure import ResultsTable, Measurements

from ij.io import PluginClassLoader


from java.lang import Double
from java.awt import Rectangle, Polygon, Color

import json
import alignedcrop as ssd # misc functions for finding alignment offset of an image w.r.t. a landmark image.
#import dographdb as gdb # wrappers for graph-db gremlin-groovy stored procedures 
import gdb_jy_api as gdb_jy
#import geojson # for marshalling Polygon in and out of graph-db
import pickle # for marshalling in and out of graph-db
from ImageVSManager import ImageVSManager #as mmImgManage

import uuid


class GDBVisualizer(object):
    def __init__(self):
        try:
            self.logger = logging.getLogger('grabia_app.gbimageanalyzer.core.GDBVisualizer')
            print 'initialized'

        except Exception as inst:
            print type(inst)
            print inst.args
            print inst
            raise # re-raise Exception

    def enhanceImageContrast(self, imp):
        process_name = "Enhance Contrast..."
        process_options = "saturated=0.4 normalize"
        IJ.run(imp, process_name, process_options)
        return imp

    def instantiatePolygonRoiFromCoordslist(self, roicoords):
        # roicoords is a list of coordinate tuples
        xcoords, ycoords = zip(*roicoords)

        proi = PolygonRoi(list(xcoords),list(ycoords), len(xcoords), Roi.POLYGON)

        return proi

    def getImagePlusObjByFilepath(self, filepath, gain=1.0):
        self.logger.debug('cur file: ' + filepath)
        if (filepath != None):
            img = IJ.openImage(filepath)
            imp = img.createImagePlus()
            ip = img.getProcessor()
            if gain != 1.0:
                ip.multiply(gain)
            imp.setProcessor(filepath, ip)

            return imp
        else:
            return None


    def showRoiOnImageByUUID(self, roiUUID, imgUUID=None): #, imgUUID):
        roinode = gdb_jy.gdb_getNodeByUUID(roiUUID)
        if imgUUID:
            imgnode = gdb_jy.gdb_getNodeByUUID(imgUUID)
        else:
            if roinode['name'] == 'cluster':
                imgnode = gdb_jy.gdb_getNodeByUUID(roinode['imageRefUUID'])
            elif roinode['name'] == 'blob':
                imgnode = gdb_jy.gdb_getImageNodeByBlobUUID(roinode['uuid'])

        roi = self.instantiatePolygonRoiFromCoordslist(json.loads(roinode['roi']))

        imp = self.getImagePlusObjByFilepath(imgnode['filepath'])
        imp = self.enhanceImageContrast(imp)

        imp.setRoi(roi)
        imp.show()
        return imp

    def addRoiOnImageByUUID(self, imp, roiUUID):
        roinode = gdb_jy.gdb_getNodeByUUID(roiUUID)
        roi = self.instantiatePolygonRoiFromCoordslist(json.loads(roinode['roi']))
        self.addRoiOnImage(imp, roi)

    def addRoiOnImageByCoords(self, imp, coordslist):
        roi = self.instantiatePolygonRoiFromCoordslist(coordslist)
        self.addRoiOnImage(imp, roi)

    def addRoiOnImage(self, imp, roi):
        imp.setRoi(roi)
        imp.show()


class GraphBasedImageAnalysisManager(object):
    __DEFAULT_LANDMARK_FRACTION__ = 8
    __mmImgManager__ = None
    __pluginclassloader__ = None
    cur_img_idx = 0
    logger = logging.getLogger('grabia_app.gbimageanalyzer.core.GraphBasedImageAnalysisManager')

    def __init__(self):
        try:
                        #pcloader = IJ.getClassLoader()
                        #self.__pluginclassloader = PluginClassLoader('../lib/AdaptiveThr',True)
           self.logger.debug('initialized')

        except Exception as inst:
            print type(inst)
            print inst.args
            print inst
            raise # re-raise Exception

    # end of def

    def __initImgManager__(self, baseDir, landmarkFilename):
        self.__mmImgManager__ = ImageVSManager(baseDir, landmarkFilename)


    def alignTwoImages(self, imp_t1, imp_t2, fraction=__DEFAULT_LANDMARK_FRACTION__):
        xoff, yoff = ssd.findTwoImageOffset(imp_t1, imp_t2, fraction)
        self.logger.debug(super.__class__.__name__ + ': landmark offset: ' + str(xOff) + ', ' + str(yOff))
        return xOff, yOff


    def alignImageToLandmark(self, landmarkImpReduced, imageImp, fraction=__DEFAULT_LANDMARK_FRACTION__):
        xOff, yOff = ssd.findLandmarkOffsetFullSearch(landmarkImpReduced, imageImp, fraction)
        self.logger.debug(super.__class__.__name__ + ': landmark offset: ' + str(xOff) + ', ' + str(yOff))
        return xOff, yOff

    def alignImageToLandmarkByCorrelation(self, landmarkImpReduced, imageImp, fraction=__DEFAULT_LANDMARK_FRACTION__):
        xOff, yOff = ssd.findLandmarkOffsetFullSearch2(landmarkImpReduced, imageImp, fraction)
        self.logger.debug(super.__class__.__name__ + ': landmark offset: ' + str(xOff) + ', ' + str(yOff))
        return xOff, yOff

    default_steps =   [
        ("Enhance Contrast...", "saturated=0.4 normalize"),
        ("Subtract Background...", "rolling=50 light"),
        ("Make Binary", ""),
        ("Erode", ""),
        ("Erode", ""),
        ("Dilate", ""),
        ("Dilate", "")
    ]

    def executeImageProcessingPipeline(self, imp, processes=default_steps, is_auto=True):
        for step in processes:
            process_name = step[0]
            process_options = step[1]
            self.logger.debug('executing ' + str(process_name) + ' with options: ' + str(process_options))
            IJ.run(imp, process_name, process_options)
        imp, roiM = self.detectBlobs(imp) #, particle_min=250)

        impcheck = imp.duplicate()
        impcheck.show()
        if is_auto:
            time.sleep(3)
        else:
            raw_input("Please press enter to continue...")
        impcheck.close()

        return imp, roiM


    def instantiatePolygonRoiFromCoordslist(self, roicoords):
        # roicoords is a list of coordinate tuples
        xcoords, ycoords = zip(*roicoords)

        proi = PolygonRoi(list(xcoords),list(ycoords), len(xcoords), Roi.POLYGON)

        return proi

    def detectBlobs(self, imp, particle_min=250):

        #IJ.run(imp, "Enhance Contrast...", "saturated=0.4 normalize")
        #IJ.run(imp, "Subtract Background...", "rolling=50 light")
        #IJ.run(imp, "Make Binary", "")
        #IJ.run(imp, "Erode", "")
        #IJ.run(imp, "Erode", "")

        #IJ.run(imp, "Dilate", "")
        #IJ.run(imp, "Dilate", "")

        # count blobs
        # Create a table to store the results
        table = ResultsTable()

        #roiM = RoiManager.getInstance()
        # Create a hidden ROI manager, to store a ROI for each blob or cell
        #if (roiM == None):
        roiM = RoiManager(False)

        # Create a ParticleAnalyzer, with arguments:
        # 1. options (could be SHOW_ROI_MASKS, SHOW_OUTLINES, SHOW_MASKS, SHOW_NONE, ADD_TO_MANAGER, and others; combined with bitwise-or)
        # 2. measurement options (see [http://rsb.info.nih.gov/ij/developer/api/ij/measure/Measurements.html Measurements])
        # 3. a ResultsTable to store the measurements
        # 4. The minimum size of a particle to consider for measurement
        # 5. The maximum size (idem)
        # 6. The minimum circularity of a particle
        # 7. The maximum circularity
        #option = ParticleAnalyzer.ADD_TO_MANAGER | ParticleAnalyzer.EXCLUDE_EDGE_PARTICLES | ParticleAnalyzer.SHOW_OVERLAY_OUTLINES
        option = ParticleAnalyzer.ADD_TO_MANAGER | ParticleAnalyzer.SHOW_OVERLAY_OUTLINES | ParticleAnalyzer.SHOW_RESULTS
        #print 'Option is: ' + str(option)
        pa = ParticleAnalyzer(option, Measurements.AREA, table, 350, Double.POSITIVE_INFINITY, 0.0, 1.0)

        pa.setRoiManager(roiM)

        IJ.run(imp, "Overlay Options...", "stroke=yellow width=1 fill=none show");
        #IJ.run(imp, "Analyze Particles...", "size="+str(particle_min)+"-Infinity circularity=0.00-1.00 show=[Overlay Outlines] exclude add")
        IJ.run(imp, "Analyze Particles...", "size="+str(particle_min)+"-Infinity circularity=0.00-1.00 show=[Overlay Outlines] add")
        #roiM.runCommand("show all without labels") #showAll(RoiManager.NO_LABELS)

        return imp, roiM
    # end of def


    gRoiM = None
    gRoiP = None
    def createImageBlobs(self, imageUUID, roiM, xOff, yOff, timestamp):
        self.gRoiM = roiM

        self.logger.debug('image node uuid: ' + str(imageUUID))

        #fluoImgNodes = gdb.getLinkedFluoImages(imageNode.eid) # return a list of fluorescent images linked to the current bf image
        fluoImgNodes = gdb_jy.gdb_getLinkedFluoImageNodes(imageUUID) # return a list of fluorescent images linked to the current bf image
        fluoImps = []
        # gather some general stats on fluorescent images
        for imgNode in fluoImgNodes:
            self.logger.debug('fluoImg Filepath:' + imgNode['filepath'])
            imp = self.__mmImgManager__.getImagePlusObjByFilepath(imgNode['filepath'], 1)
            ####IJ.run(imp, "Enhance Contrast...", "saturated=0.4 normalize")
            # create a graph node for image level statistics
            stats = imp.getStatistics(Measurements.MEAN)
            statsData  = pickle.dumps(dict(mean=stats.mean))
            statUUID = gdb_jy.gdb_issueUUID() # generate a unique id for the stat
            imgStatsNode = gdb_jy.gdb_addImageStatsNode(imgNode['uuid'], statUUID, statsData, imgNode['contentType']+' background')
            fluoImps.append(dict(fluoImp=imp, imgNode=imgNode, contentType=imgNode['contentType'], statsNode=imgStatsNode))

        # process all ROIs
        for roi in roiM.getRoisAsArray():
            #print('DEBUG printing roi polygon:')
            p = roi.getPolygon()
            #geop = geojson.Polygon(zip(p.xpoints, p.ypoints))
            #print p
            self.gRoiP = p
            coordlist = zip(p.xpoints, p.ypoints)
            #### create a blob node
            # geop is serialized by geojson.dumps
            #blobNode = gdb.createBlobNode(xOff, yOff, geojson.dumps(geop), timestamp)

            # add to graph db
            #gdb.linkBlobToImage(blobNode, imageNode)

            roiRect = roi.getBounds()
            roiRectArea = roiRect.width * roiRect.height
            # imp.getRoi().getImage().show()
            # stats = imp.getStatistics(Measurements.AREA)
            # areas.append(stats.area)
            #print roiRect
            statslist = []
            for fluoImp in fluoImps:
                imp = fluoImp['fluoImp']
                fluoImgNode = fluoImp['imgNode']
                imgStatsNode = fluoImp['statsNode']
                chName = fluoImp['contentType']
                imp.setRoi(roi)
                # get ROI (blob-level) statistics and marshal them for storing in graph db
                stats = imp.getStatistics(Measurements.MEAN | Measurements.MEDIAN | Measurements.AREA | Measurements.STD_DEV)
                statsData = pickle.dumps(dict(mean=stats.mean, median=stats.median, area=stats.area, stdDev=stats.stdDev))

                channelStat = {}
                channelStat['ch'] = chName
                channelStat['stat'] = statsData
                channelStat['statuuid'] = imgStatsNode['uuid']
                statslist.append(channelStat)
                # store in graph db the blob level statistics
                #gdb.addBlobStatsNode(imgStatsNode, blobNode, statsData, chName)

            # end for
            blobUUID = gdb_jy.gdb_issueUUID() # generate a unique id for the stat
            gdb_jy.gdb_createBlobNodeForImage(imageUUID, blobUUID, xOff, yOff, json.dumps(coordlist), timestamp, statslist)
        # end for
    # end of def

    # img_arg_set is a list of dict(img_filepath=basedir+filename, chname=chname, img_uuid=chimg_uuid, timestamp=timestamp)
    def parseImageRois(self, roiM, img_arg_set): # img_filepath_list, chname_list):
#        self.gRoiM = roiM
        #img_filepath_list = [item['img_filepath'] for item in img_arg_set]
        #chname_list = [item['chname'] for item in img_arg_set]
        #uuid_list = [item['img_uuid'] for item in img_arg_set]
        #timestamps = [item['timestamp'] for item in img_arg_set]

        self.logger.debug('entering parseImageRois()')

        #fluoImgNodes = gdb.getLinkedFluoImages(imageNode.eid) # return a list of fluorescent images linked to the current bf image
#        fluoImgNodes = gdb_jy.gdb_getLinkedFluoImageNodes(imageUUID) # return a list of fluorescent images linked to the current bf image
        imps_list = []
        img_stats_list = []
        # gather some general stats on fluorescent images
        #for imgNode in fluoImgNodes:
        for idx, img_metadata in enumerate(img_arg_set): #img_filepath_list):
            filepath = img_metadata['img_filepath']
            chname = img_metadata['chname']
            img_uuid = img_metadata['img_uuid']
            img_timestamp = img_metadata['timestamp']

            self.logger.debug('img filepath:' + filepath) #imgNode['filepath'])
            imp = self.__mmImgManager__.getImagePlusObjByFilepath(filepath, 1) #imgNode['filepath'], 1)
            ####IJ.run(imp, "Enhance Contrast...", "saturated=0.4 normalize")
            # create a graph node for image level statistics
            stats = imp.getStatistics(Measurements.MEAN)
            #img_stats_data  = pickle.dumps(dict(mean=stats.mean))
            img_metadata['mean'] = stats.mean #dict(filepath=filepath, mean=stats.mean, chname=chname, uuid=img_uuid, timestamp=img_timestamp)
            img_stats_data  = img_metadata
            #stat_UUID = gdb_jy.gdb_issueUUID() # generate a unique id for the stat
            #imgStatsNode = gdb_jy.gdb_addImageStatsNode(imgNode['uuid'], statUUID, statsData, imgNode['contentType']+' background')
            imps_list.append(imp) #(dict(fluoimp=imp, img_stats=img_stats_data))
            img_stats_list.append(img_stats_data)

        # process all ROIs
        roi_stats = []
        for roi in roiM.getRoisAsArray():
            #print('DEBUG printing roi polygon:')
            polygon = roi.getPolygon()
            #geop = geojson.Polygon(zip(p.xpoints, p.ypoints))
            #print p
            #self.gRoiP = polygon
            roi_coordlist = zip(polygon.xpoints, polygon.ypoints)
            #### create a blob node
            # geop is serialized by geojson.dumps
            #blobNode = gdb.createBlobNode(xOff, yOff, geojson.dumps(geop), timestamp)

            # add to graph db
            #gdb.linkBlobToImage(blobNode, imageNode)

            roiRect = roi.getBounds()
            roiRectArea = roiRect.width * roiRect.height
            # imp.getRoi().getImage().show()
            # stats = imp.getStatistics(Measurements.AREA)
            # areas.append(stats.area)
            #print roiRect
            roi_ch_statslist = []
            for idx, imp in enumerate(imps_list[1:]):
                #imp = fluoimp['fluoimp']
                imp.setRoi(roi) # set a roi in the image
                # get ROI (blob-level) statistics and marshal them for storing in graph db
                stats = imp.getStatistics(Measurements.MEAN | Measurements.MEDIAN | Measurements.AREA | Measurements.STD_DEV)
                roi_statsdata = pickle.dumps(dict(mean=stats.mean, median=stats.median, area=stats.area, std_dev=stats.stdDev))

                channelstat = {}
                channelstat['ch'] = img_stats_list[idx]['chname'] #chname_list[idx] #chName
                channelstat['stats'] = roi_statsdata
                roi_ch_statslist.append(channelstat)
                # store in graph db the blob level statistics
                #gdb.addBlobStatsNode(imgStatsNode, blobNode, statsData, chName)

            roi_stats.append(dict(polygon=roi_coordlist, ch_stats=roi_ch_statslist))
            # end for
        # end for
        #ret_data = dict(roi_stats=json.dumps(roi_stats), img_stats=json.dumps(img_stats))
        ret_data = dict(roi_stats=roi_stats, img_stats=img_stats_list)
        return ret_data
    # end of def

    # process geometrical relationships among blobs within a frame
    def processIntraFrameFeatureRelations(self, imgUUID, maxClusteringDist):
        gdb_jy.gdb_processIntraFrameFeatureRelations(imgUUID, maxClusteringDist)
    # end of def

    # process geometrical relationships among blobs between different frames
    def processInterFrameFeatureRelations(self, imgt1UUID, imgt2UUID): #expId, imgNodeAt_t1, imgNodeAt_t2):
        gdb_jy.gdb_processInterFrameFeatureRelations(imgt1UUID, imgt2UUID)
    # end of def

    def getRefreshLandmark(self, imp, xoff, yoff, xwidth, ywidth):
        self.logger.debug('entering getRefreshLandmark()')
        imp.setRoi(xoff, yoff, xwidth, ywidth)
        landmarkmatch = imp.duplicate()
        landmarkmatch_reduced = ssd.preprocessLandmarkImage(landmarkmatch, self.__DEFAULT_LANDMARK_FRACTION__)
        self.logger.debug('exiting getRefreshLandmark()')
        return landmarkmatch_reduced

    def getImageStatsForAnalysis(self, baseDir, landmarkFilename, fromIdx, toIdx, maxClusteringDist, is_landmark_refreshed=True):
        self.logger.debug('entering processImageAnalysisSet()')
        #self.__mmImgManager__ = ImageVSManager(baseDir)
        self.__initImgManager__(baseDir, landmarkFilename)
        self.logger.debug('processImageAnalysisSet: image manager initialized.')
        numChs = self.__mmImgManager__.numChannels()
        numFrames = self.__mmImgManager__.numFrames()
        if numFrames > (toIdx - fromIdx): # + 1):
            numFrames = toIdx - fromIdx # + 1

        #prevImgNode = None
        #curImgNode = None

        # create experiment graph node
        metadataFilepath = 'tbd'
        expDetails = ''
        expUUID = self.__mmImgManager__.getHeaderUUID()

        landmarkImp = self.__mmImgManager__.getLandmarkImage()
        #landmark2Ip = self.__mmImgManager__.getLandmarkImage()

        landmarkImp.getProcessor().blurGaussian(2)
        landmarkImpReduced = ssd.preprocessLandmarkImage(landmarkImp, self.__DEFAULT_LANDMARK_FRACTION__)
        #landmark2Ip.blurGaussian(2)
        #landmark1Ip = ssd.normalizeIp(landmark1Ip)
        #landmark2Ip = ssd.normalizeIp(landmark2Ip)

        stack = ImageStack(self.__mmImgManager__.width(), self.__mmImgManager__.height())
        landmarkWidth = landmarkImp.getWidth()
        landmarkHeight = landmarkImp.getHeight()

        refImgUUID = None
        curImgUUID = None
        prevImgUUID = None
        image_stats_list = []
        for t in range(fromIdx, toIdx):
            # initialize local variables
            self.cur_img_idx = t
            curImps = []
            filepaths = []
            chnames = []
            xOff = 0
            yOff = 0
            timestamp = -1

            img_arg_set = []

            for c in range(0, numChs):
                curImp, filename = self.__mmImgManager__.getImagePlusObj(0, c, t)
                #print '############### filename: ', filename, curImp

                if curImp != None:
                    basedir = self.__mmImgManager__.__baseDir__
                    chname = self.__mmImgManager__.__chNames__[c]
                    chimg_uuid = self.__mmImgManager__.getUUID(0, c, t)
                    timestamp = self.__mmImgManager__.getFrameInterval_ms()*t/1000
                    chnames.append(chname)

                    imgwidth = curImp.getWidth()
                    imgheight = curImp.getHeight()
                    curImps.append(curImp)
                    filepaths.append(basedir+filename)
                    if c == 0: # find alignment landmark offset for the first BF frame
                        tmpImp = curImp.duplicate()
                        IJ.run(tmpImp, "Enhance Contrast...", "saturated=0.4 normalize")
                        xOff, yOff = self.alignImageToLandmarkByCorrelation(landmarkImpReduced, tmpImp, self.__DEFAULT_LANDMARK_FRACTION__)
                        self.logger.debug('processImageAnalysisSet: landmark offset for ' + str(filename) + ': (' + str(xOff) + ', ' + str(yOff) + ')')
                        if is_landmark_refreshed:
                            self.logger.debug('processImageAnalysisSet: refreshing landmark')
                            landmarkImpReduced = self.getRefreshLandmark(tmpImp, xOff, yOff, landmarkWidth, landmarkHeight)
                            self.logger.debug('processImageAnalysisSet: landmark refreshed')
                            #landmarkImpReduced.show()

                    landmarkOffsetX = xOff
                    landmarkOffsetY = yOff

                    if c == 0:
                        #print filename
                        tmpImp = curImp.duplicate()
                        IJ.run(tmpImp, "Enhance Contrast...", "saturated=0.4 normalize")
                        curIp = tmpImp.getProcessor()
                        curIp.setColor(Color.magenta)
                        curIp.setLineWidth(1)
                        curIp.drawRect(landmarkOffsetX, landmarkOffsetY, landmarkWidth, landmarkHeight)
                        tmpImp.setRoi(landmarkOffsetX, landmarkOffsetY, landmarkWidth, landmarkHeight)
                        #tmpImp.show()
                        curIp.setRoi(landmarkOffsetX, landmarkOffsetY, landmarkWidth, landmarkHeight)
                        stack.addSlice(None, curIp)
                        #cropIp = curIp.crop()
                        #ip = curImp.getProcessor().convertToRGB()
                        #curImp.setProcessor(None, ip)

                    img_arg_set.append(dict(img_filepath=basedir+filename, chname=chname, img_uuid=chimg_uuid, timestamp=timestamp, landmarkOffset=(xOff, yOff), img_size=(imgwidth, imgheight)))
            # end of for

            # execute the image processing pipeline on the bright field image
            # the image processing pipeline also returns a ROI manager listing blobs
            curImps[0], roiM = self.executeImageProcessingPipeline(curImps[0])

            curOverlay = curImps[0].getOverlay()
            #print curOverlay.size()
            curIp = curImps[0].getProcessor()
            curIp.drawOverlay(curOverlay)
            stack.addSlice(None, curIp)

            # parse image roi data
            #img_arg_set = dict(img_filepath_list=filepaths[1:], chname_list=chnames[1:], img_uuid_list=chimg_uuid, timestamps=timestamps)
            image_stats = self.parseImageRois(roiM, img_arg_set) # img_filepath_list=filepaths[1:], chname_list=chnames[1:])
            image_stats['bf_filepath']  = filepaths[0] # filepath to the brightfield image (This would be Nikon TiE series specific)
            image_stats_list.append(image_stats)
            print json.dumps(dict(exp_uuid=expUUID, seq_idx=t, data=image_stats))

            # end of for
            ##showImp = ImagePlus("stacks", stack)
            ##showImp.show()
        return True
        #return json.dumps(dict(exp_uuid=expUUID, data=image_stats_list))
    # end of def



    def processImageAnalysisSet(self, baseDir, landmarkFilename, fromIdx, toIdx, maxClusteringDist, is_landmark_refreshed=True):
        self.logger.debug('entering processImageAnalysisSet()')
        #self.__mmImgManager__ = ImageVSManager(baseDir)
        self.__initImgManager__(baseDir, landmarkFilename)
        self.logger.debug('processImageAnalysisSet: image manager initialized.')
        numChs = self.__mmImgManager__.numChannels()
        numFrames = self.__mmImgManager__.numFrames()
        if numFrames > (toIdx - fromIdx + 1):
            numFrames = toIdx - fromIdx + 1

        #prevImgNode = None
        #curImgNode = None

        # create experiment graph node
        metadataFilepath = 'tbd'
        expDetails = ''
        expUUID = self.__mmImgManager__.getHeaderUUID()
        gdb_jy.gdb_createExperimentNode(expUUID, "Timelapse microscopy", metadataFilepath, expDetails)
        self.logger.debug('processImageAnalysisSet: exp node created')

        landmarkImp = self.__mmImgManager__.getLandmarkImage()
        #landmark2Ip = self.__mmImgManager__.getLandmarkImage()

        landmarkImp.getProcessor().blurGaussian(2)
        landmarkImpReduced = ssd.preprocessLandmarkImage(landmarkImp, self.__DEFAULT_LANDMARK_FRACTION__)
        #landmark2Ip.blurGaussian(2)
        #landmark1Ip = ssd.normalizeIp(landmark1Ip)
        #landmark2Ip = ssd.normalizeIp(landmark2Ip)

        stack = ImageStack(self.__mmImgManager__.width(), self.__mmImgManager__.height())
        landmarkWidth = landmarkImp.getWidth()
        landmarkHeight = landmarkImp.getHeight()

        refImgUUID = None
        curImgUUID = None
        prevImgUUID = None
        for t in range(fromIdx, numFrames):
            # initialize local variables
            self.cur_img_idx = t
            curImps = []
            filenames = []
            xOff = 0
            yOff = 0
            timestamp = -1

            for c in range(0, numChs):
                curImp, filename = self.__mmImgManager__.getImagePlusObj(0, c, t)

                if curImp != None:
                    curImps.append(curImp)
                    filenames.append(filename)
                    if c == 0: # find alignment landmark offset for the first BF frame
                        tmpImp = curImp.duplicate()
                        IJ.run(tmpImp, "Enhance Contrast...", "saturated=0.4 normalize")
                        xOff, yOff = self.alignImageToLandmarkByCorrelation(landmarkImpReduced, tmpImp, self.__DEFAULT_LANDMARK_FRACTION__)
                        self.logger.debug('processImageAnalysisSet: landmark offset for ' + str(filename) + ': (' + str(xOff) + ', ' + str(yOff) + ')')
                        if is_landmark_refreshed:
                            self.logger.debug('processImageAnalysisSet: refreshing landmark')
                            landmarkImpReduced = self.getRefreshLandmark(tmpImp, xOff, yOff, landmarkWidth, landmarkHeight)
                            self.logger.debug('processImageAnalysisSet: landmark refreshed')
                            #landmarkImpReduced.show()


                    #### create image nodes in graph db
                    uuidStr = self.__mmImgManager__.getUUID(0, c, t)
                    chName = self.__mmImgManager__.__chNames__[c]
                    timestamp = self.__mmImgManager__.getFrameInterval_ms()*t/1000
                    self.logger.debug('processImageAnalysisSet: checking for an existing imaging node: '+str(uuidStr))
                    existingNode = gdb_jy.gdb_getNodeByUUID(uuidStr)
                    imgwidth = curImp.getWidth()
                    imgheight = curImp.getHeight()
                    if existingNode != None:
                        self.logger.debug('processImageAnalysisSet: existing imaging node found: '+str(uuidStr))
                        curImgUUID = uuidStr
                        gdb_jy.gdb_updateImageNodeByUUID(uuidStr, uuidStr, chName, baseDir + filename, imgwidth, imgheight, xOff, yOff, self.__mmImgManager__.getLandmarkFilepath(), self.__mmImgManager__.getMetadataFilepath(), timestamp)
                        gdb_jy.gdb_linkImageToExp(curImgUUID, expUUID)
                    else:
                        curImgUUID = uuidStr
                        self.logger.debug('processImageAnalysisSet: creating an image node (uuid: ' + str(uuidStr) + ')')
                        gdb_jy.gdb_createImageNode(uuidStr, chName, baseDir + filename, imgwidth, imgheight, xOff, yOff, self.__mmImgManager__.getLandmarkFilepath(), self.__mmImgManager__.getMetadataFilepath(), timestamp)
                        gdb_jy.gdb_linkImageToExp(curImgUUID, expUUID)

                    landmarkOffsetX = xOff
                    landmarkOffsetY = yOff

                    if c == 0:
                        refImgUUID = curImgUUID
                        print filename
                        tmpImp = curImp.duplicate()
                        IJ.run(tmpImp, "Enhance Contrast...", "saturated=0.4 normalize")
                        curIp = tmpImp.getProcessor()
                        curIp.setColor(Color.magenta)
                        curIp.setLineWidth(1)
                        curIp.drawRect(landmarkOffsetX, landmarkOffsetY, landmarkWidth, landmarkHeight)
                        tmpImp.setRoi(landmarkOffsetX, landmarkOffsetY, landmarkWidth, landmarkHeight)
                        #tmpImp.show()
                        curIp.setRoi(landmarkOffsetX, landmarkOffsetY, landmarkWidth, landmarkHeight)
                        stack.addSlice(None, curIp)
                        #cropIp = curIp.crop()
                        #ip = curImp.getProcessor().convertToRGB()
                        #curImp.setProcessor(None, ip)
                    else:
                        if existingNode == None:
                            gdb_jy.gdb_linkBFImageToFluoImage(refImgUUID, curImgUUID)
                # end of if
            # end of for

            # execute the image processing pipeline on the bright field image
            # the image processing pipeline also returns a ROI manager listing blobs
            curImps[0], roiM = self.executeImageProcessingPipeline(curImps[0])

            curOverlay = curImps[0].getOverlay()
            print curOverlay.size()
            curIp = curImps[0].getProcessor()
            curIp.drawOverlay(curOverlay)
            stack.addSlice(None, curIp)


            self.logger.debug('processImageAnalysisSet: creating image blobs imguuid: ' + str(refImgUUID))

            # catalogue image features in graph db
            self.createImageBlobs(refImgUUID, roiM, xOff, yOff, timestamp)
            self.logger.debug('processImageAnalysisSet: processing intra frame feature relations imguuid: ' + str(refImgUUID))
            self.processIntraFrameFeatureRelations(refImgUUID, maxClusteringDist)

            # correspondence analysis between images at time t and t+1
            if prevImgUUID != None:
                self.logger.debug('processImageAnalysisSet: processing inter frame feature relations imguuid: ' + str(refImgUUID))
                self.processInterFrameFeatureRelations(prevImgUUID, refImgUUID)


            prevImgUUID = refImgUUID
        # end of for
        ##showImp = ImagePlus("stacks", stack)
        ##showImp.show()
    # end of def

    def processImageLandmarkAlignment(self, baseDir, landmarkFilename, fromIdx, toIdx):
        #self.__mmImgManager__ = ImageVSManager(baseDir)
        self.__initImgManager__(baseDir, landmarkFilename)
        numChs = self.__mmImgManager__.numChannels()
        numFrames = self.__mmImgManager__.numFrames()
        if numFrames > (toIdx - fromIdx + 1):
            numFrames = toIdx - fromIdx + 1

        prevImgNode = None
        curImgNode = None

        # create experiment graph node
        metadataFilepath = 'tbd'
        expDetails = ''

        landmarkIp = self.__mmImgManager__.getLandmarkImage()
        #landmark2Ip = self.__mmImgManager__.getLandmarkImage()

        #landmark1Ip.blurGaussian(2)
        landmarkIp.blurGaussian(2)

        #landmark1Ip = ssd.normalizeIp(landmark1Ip)
        #landmark2Ip = ssd.normalizeIp(landmark2Ip)

        stack = ImageStack(self.__mmImgManager__.width(), self.__mmImgManager__.height())
        landmarkWidth = landmarkIp.getWidth()
        landmarkHeight = landmarkIp.getHeight()
        landmarkStack = ImageStack(landmarkWidth, landmarkHeight)
        landmarkStack.addSlice(None, landmarkIp)

        # remove previously registered inter-frame feature relations from the database
        gdb_jy.removeLinksByLabel('at_t1_subsequently_coincides_at_t2')
        gdb_jy.removeLinksByLabel('cluster_corresponds_to')

        roiMs = []
        for t in range(fromIdx, numFrames):
            # initialize local variables
            curImps = []
            filenames = []
            refImgUUID = None
            curImgNode = None
            xOff = 0
            yOff = 0
            timestamp = -1

            for c in range(0, numChs):
                curImp, filename = self.__mmImgManager__.getImagePlusObj(0, c, t)

                if curImp != None:
                    curImps.append(curImp)
                    filenames.append(filename)

                    if c == 0: # find alignment landmark offset for the first BF frame
                        tmpImp = curImp.duplicate()
                        IJ.run(tmpImp, "Enhance Contrast...", "saturated=0.4 normalize")
                        ######xOff, yOff = self.alignImageToLandmark(landmarkIp, tmpImp, self.__DEFAULT_LANDMARK_FRACTION__)
                        print 'landmark offset' + ' for ' + filename + ': (' + str(xOff) + ', ' + str(yOff) + ')'


                    #### create image nodes in graph db
                    uuidStr = self.__mmImgManager__.getUUID(0, c, t)
                    chName = self.__mmImgManager__.__chNames__[c]
                    timestamp = self.__mmImgManager__.getFrameInterval_ms()*t/1000

                    # retrieve image node from gdb
                    #curImgNode = gdb.g.vertices.index.lookup(uuid=uuidStr).next()
                    curImgNode = gdb_jy.gdb_getNodeByUUID(uuidStr)
                    #####landmarkOffsetX = xOff #curImgNode.landmarkOffsetX
                    #####landmarkOffsetY = yOff #curImgNode.landmarkOffsetY
                    landmarkOffsetX = curImgNode['landmarkOffsetX']
                    landmarkOffsetY = curImgNode['landmarkOffsetY']

                    #######curImgNode = gdb.updateImageNode(curImgNode, uuidStr, chName, baseDir + filename, curImp, xOff, yOff, self.__mmImgManager__.getLandmarkFilepath(), self.__mmImgManager__.getMetadataFilepath(), timestamp)

                    print 'landmarkOffset: (' + str(landmarkOffsetX) + ', ' + str(landmarkOffsetY) + ')'
                    if c == 0:
                        refImgNode = curImgNode
                        print filename
                        tmpImp = curImp.duplicate()
                        IJ.run(tmpImp, "Enhance Contrast...", "saturated=0.4 normalize")
                        curIp = tmpImp.getProcessor()
                        curIp.setColor(Color.magenta)
                        curIp.setLineWidth(1)
                        curIp.drawRect(landmarkOffsetX, landmarkOffsetY, landmarkWidth, landmarkHeight)
                        tmpImp.setRoi(landmarkOffsetX, landmarkOffsetY, landmarkWidth, landmarkHeight)
                        tmpImp.show()
                        curIp.setRoi(landmarkOffsetX, landmarkOffsetY, landmarkWidth, landmarkHeight)
                        stack.addSlice(None, curIp)
                        cropIp = curIp.crop()
                        landmarkStack.addSlice(cropIp)
                        #ip = curImp.getProcessor().convertToRGB()
                        #curImp.setProcessor(None, ip)

                # end of if
                # execute the image processing pipeline on the bright field image
                # the image processing pipeline also returns a ROI manager listing blobs
                curImps[0], roiM = self.executeImageProcessingPipeline(curImps[0])
                curOverlay = curImps[0].getOverlay()
                print curOverlay.size()
                curIp = curImps[0].getProcessor()
                curIp.drawOverlay(curOverlay)
                stack.addSlice(None, curIp)

                #curImps[0].show()
                print roiM.count
                roiMs.append(roiM)
            # end of for

            # correspondence analysis between images at time t and t+1
            expUUID = self.__mmImgManager__.getHeaderUUID()
            #expNode = gdb.g.vertices.index.lookup(uuid=expUUID).next()
            expNode = gdb_jy.gdb_getNodeByUUID(expUUID)
            if prevImgNode != None:
                self.processInterFrameFeatureRelations(prevImgUUID, refImgUUID)

            prevImgUUID = refImgUUID
        # end of for
        showImp = ImagePlus("stacks", stack)
        landmarkImp = ImagePlus("landmark stack", landmarkStack)

        #for t in range(fromIdx, numFrames):
        #`	showImp.setSliceWithoutUpdate(t)
        #	rois = roiMs[t].getRoisAsArray()
        #	for roi in rois:
        #		roiMs[t].add(showImp, roi, t)
        #	showImp.setSlice(t)

        #showImp, roiM = self.executeImageProcessingPipeline(showImp) #curImps[0])

        showImp.show()
        landmarkImp.show()
    # end of def


    def processImageAnalysisSetIJOnly(self, baseDir, landmarkFilename, fromIdx, toIdx, doDB):
        #self.__mmImgManager__ = ImageVSManager(baseDir)
        self.__initImgManager__(baseDir, landmarkFilename)
        numChs = self.__mmImgManager__.numChannels()
        numFrames = self.__mmImgManager__.numFrames()
        if numFrames > (toIdx - fromIdx + 1):
            numFrames = toIdx - fromIdx + 1

        prevImgNode = None
        curImgNode = None

        # create experiment graph node
        metadataFilepath = 'tbd'
        expDetails = ''

        landmarkIp = self.__mmImgManager__.getLandmarkImage()

        landmarkIp.blurGaussian(2)

        #landmark1Ip = ssd.normalizeIp(landmark1Ip)
        #landmark2Ip = ssd.normalizeIp(landmark2Ip)

        stack = ImageStack(self.__mmImgManager__.width(), self.__mmImgManager__.height())
        landmarkWidth = landmarkIp.getWidth()
        landmarkHeight = landmarkIp.getHeight()
        landmarkStack = ImageStack(landmarkWidth, landmarkHeight)
        landmarkStack.addSlice(None, landmarkIp)

        roiMs = []
        for t in range(fromIdx, numFrames):
            # initialize local variables
            curImps = []
            filenames = []
            refImgNode = None
            curImgNode = None
            xOff = 0
            yOff = 0
            timestamp = -1

            for c in range(0, numChs):
                curImp, filename = self.__mmImgManager__.getImagePlusObj(0, c, t)

                if curImp != None:
                    curImps.append(curImp)
                    filenames.append(filename)

                    #if c == 0: # find alignment landmark offset for the first BF frame
                    #	xOff, yOff = self.alignImageToLandmark(landmarkIp, curImp.getProcessor(), self.__DEFAULT_LANDMARK_FRACTION__)
                    #	print 'landmark offset' + ' for ' + filename + ': (' + str(xOff) + ', ' + str(yOff) + ')'


                    #### create image nodes in graph db
                    uuidStr = self.__mmImgManager__.getUUID(0, c, t)
                    chName = self.__mmImgManager__.__chNames__[c]
                    timestamp = self.__mmImgManager__.getFrameInterval_ms()*t/1000

                    # retrieve image node from gdb
                    if doDB:
                        curImgNode = gdb.g.vertices.index.lookup(uuid=uuidStr).next()
                        landmarkOffsetX = curImgNode.landmarkOffsetX
                        landmarkOffsetY = curImgNode.landmarkOffsetY
                    else:
                        landmarkOffsetX = landmarkOffsetY = 0

                    print 'landmarkOffset: (' + str(landmarkOffsetX) + ', ' + str(landmarkOffsetY) + ')'
                    if c == 0:
                        refImgNode = curImgNode
                        print filename
                        tmpImp = curImp.duplicate()
                        IJ.run(tmpImp, "Enhance Contrast...", "saturated=0.4 normalize")
                        curIp = tmpImp.getProcessor()
                        curIp.setColor(Color.magenta)
                        curIp.setLineWidth(1)
                        curIp.drawRect(landmarkOffsetX, landmarkOffsetY, landmarkWidth, landmarkHeight)
                        tmpImp.setRoi(landmarkOffsetX, landmarkOffsetY, landmarkWidth, landmarkHeight)
                        tmpImp.show()
                        stack.addSlice(None, curIp)
                        curIp.setRoi(landmarkOffsetX, landmarkOffsetY, landmarkWidth, landmarkHeight)
                        cropIp = curIp.crop()
                        landmarkStack.addSlice(cropIp)
                        #ip = curImp.getProcessor().convertToRGB()
                        #curImp.setProcessor(None, ip)

                # end of if
                # execute the image processing pipeline on the bright field image
                # the image processing pipeline also returns a ROI manager listing blobs
                curImps[0], roiM = self.executeImageProcessingPipeline(curImps[0])
                curOverlay = curImps[0].getOverlay()
                print curOverlay.size()
                curIp = curImps[0].getProcessor()
                curIp.drawOverlay(curOverlay)
                stack.addSlice(None, curIp)

                #curImps[0].show()
                print roiM.count
                roiMs.append(roiM)
            # end of for


            prevImgNode = refImgNode
        # end of for
        showImp = ImagePlus("stacks", stack)
        landmarkImp = ImagePlus("landmark stack", landmarkStack)

        #for t in range(fromIdx, numFrames):
        #`	showImp.setSliceWithoutUpdate(t)
        #	rois = roiMs[t].getRoisAsArray()
        #	for roi in rois:
        #		roiMs[t].add(showImp, roi, t)
        #	showImp.setSlice(t)

        #showImp, roiM = self.executeImageProcessingPipeline(showImp) #curImps[0])

        showImp.show()
        landmarkImp.show()
    # end of def













