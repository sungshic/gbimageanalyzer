import sys
#activate_env_file = '/Users/spark/pyvirtualenv/env/bin/activate_this.py'
#execfile(activate_env_file, dict(__file__=activate_env_file))
#from bulbs.neo4jserver import Graph, Config, NEO4J_URI
from bulbs.rexster import Graph, Config #, RexsterClient

from bulbs.model import Node, Relationship
from bulbs.property import String, Integer, DateTime

import json 
import uuid
import yaml
import logging
import os
from os.path import expanduser

#__gdbinit__ = False
#my_rexster_uri = None
#config = None
g = None
logger = logging.getLogger('gdb')
logger.setLevel(logging.DEBUG)
# create file handler which logs even debug messages
logfile_dir = expanduser("~")+'/.grabia/log/'
logfile_name = 'gdb.log'
if not os.path.exists(logfile_dir):
    os.makedirs(logfile_dir)
fh = logging.FileHandler(logfile_dir+logfile_name)
fh.setLevel(logging.DEBUG)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)
# sysloghandler for remotely logging
#syslog = logging.handlers.SysLogHandler(address=('localhost', 9999))
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
# add the handlers to the logger
logger.addHandler(fh)
logger.addHandler(ch)
logger.debug('logger initialized')

def gdbinit(db_uri, script_path='/ghostos/gbimageanalyzer/neo4jGremlinQueries/ListCluster.groovy'):
    #my_rexster_uri = 'http://localhost:8180/graphs/neo4jsample'
    #my_rexster_uri = 'http://localhost:8182/graphs/tinkergraph' #graphs/neo4jsample'
    #config = Config(my_rexster_uri)
    #g=Graph(config)
   
    config = Config(db_uri+'/graphs/emptygraph')
    #g = globals()['g']
    # globals()['g'] =Graph(config)
    # globals()['g'].scripts.update('gbimageanalyzer/neo4jGremlinQueries/ListCluster.groovy')
    g = Graph(config)
    g.scripts.update(script_path)
    return g

def storeconfig(db_uri, filepath):
    config = dict(db_uri=db_uri)
    with open(filepath, 'w') as outfile:
        outfile.write(yaml.dump(config, default_flow_style=True))

def loadconfig(filepath='/ghostos/gdbconfig.yaml', script_path='/ghostos/gbimageanalyzer/neo4jGremlinQueries/ListCluster.groovy'):
    try:
        logger.debug('loading gdb config...')
        with open(filepath, 'r') as infile:
            config = yaml.load(infile)
            g = gdbinit(config['db_uri'], script_path)
            return g
    except (OSError, IOError) as e:
        print e

g = loadconfig(filepath='/ghostos/gdbconfig.yaml')
#g.scripts.update('/Users/spark/Documents/workspace/gbimageanalyzer/neo4jGremlinQueries/ListCluster.groovy')
#g.scripts.refresh()
##config = Config('http://192.168.59.103:8182/graphs/emptygraph')
##g=Graph(config)
##g.scripts.update('../neo4jGremlinQueries/ListCluster.groovy')



"""
blob
 landmarkOffset (xCoord, yCoord)
 roi coords, width, height
 timestamp

image
 contentType (e.g. BF, GFP, RFP)
 filepath
 ijip
 landmarkOffset (xCoord, yCoord)
 landmarkRefFilepath
 metadataFilepath
 timestamp

experiment
 experimentType (e.g. "Timelapse microscopy")
 metadataFilepath
 details

blob at_t1_is_near_at_t1 blob
blob at_t1_subsequently_coincide_at_t2 blob 

blob belongsToImage image(BF)
blob belongsToImage image(GFP)
blob belongsToImage image(RFP)

image belongsToExp experiment
"""

class Blob(Node):
    element_type = "cellblob"

    landmarkOffsetX = Integer()
    landmarkOffsetY = Integer()
    roi = object()
    timestampe = Integer()

class Image(Node):
    element_type = "image"
    
    contentType = String()
    filepath = String()
    #ijip = object()
    landmarkOffsetX = Integer()
    landmarkOffsetY = Integer()
    landmarkRefFilepath = String()
    metadataFilepath = String()
    timestamp = Integer()

class Experiment(Node):
    element_type = "experiment"

    experimentType = String()
    metadataFilepath = String()
    details = String()

### the following functions do not implement the proxy model graph handling yet
def issueUUID():
    uuidStr = str(uuid.uuid4())   # issue a new uuid
    node = getNodeByUUID(uuidStr) # see if the uuid exists
    while node: # if there is a clash
        uuidStr = str(uuid.uuid4())   # issue a new uuid
        node = getNodeByUUID(uuidStr) # find out if there is a node under that uuid

    # new uuid found
    return uuidStr



def createBlobNode(uuid, offsetX, offsetY, roicoords, timestamp):
    blob = g.vertices.create(name="blob")
    blob.uuid = uuid
    blob.nodetype = 'blob'
    blob.neo4jId = blob.eid
    blob.landmarkOffsetX = offsetX
    blob.landmarkOffsetY = offsetY
    blob.roi = json.dumps(roicoords)
    blob.timestamp = timestamp
    blob.save()
    return blob

def getT2ImageByT1ImgUUID(t1imgUUID):
    script = getScriptByNamespaceMethodName(g, 'ListCluster', 'getT2ImageByT1ImgUUID')
    params = dict(t1imgUUID=t1imgUUID)
    queryResult = g.gremlin.query(script, params)
    if queryResult != None:
        return queryResult.next()
    else:
        return None

def getT1ImageByT2ImgUUID(t2imgUUID):
    script = getScriptByNamespaceMethodName(g, 'ListCluster', 'getT1ImageByT2ImgUUID')
    params = dict(t2imgUUID=t2imgUUID)
    queryResult = g.gremlin.query(script, params)
    if queryResult != None:
        return queryResult.next()
    else:
        return None

def getClustersByImgUUID(imgUUID):
    # return all blob nodes for [temporalIdx]th image sequence in exp
    script1 = getScriptByNamespaceMethodName(g, 'ListCluster', 'listAllClusterNodesByImgUUID')
    params1 = dict(imgUUID=imgUUID)
    queryResult = g.gremlin.query(script1, params1)
    if queryResult != None:
        return list(queryResult) #.next()
    else:
        return None

def getNodeByUUID(uuidStr):
    # return all blob nodes for [temporalIdx]th image sequence in exp
    script1 = getScriptByNamespaceMethodName(g, 'ListCluster', 'getNodeByUUID')
    params1 = dict(uuid=uuidStr)
    queryResult = g.gremlin.query(script1, params1)
    if queryResult != None:
        return queryResult.next()
    else:
        return None

'''
    nodeList = g.vertices.index.lookup(uuid=str(uuid))

    if (nodeList != None):
        return nodeList.next()
    else:
        return None
'''

def getClusterByBlobUUID(blobUUID):
    # return all blob nodes for [temporalIdx]th image sequence in exp
    script = getScriptByNamespaceMethodName(g, 'ListCluster', 'getClusterByBlobUUID')
    params = dict(blobuuid=blobUUID)
    queryResult = g.gremlin.query(script, params)
    if queryResult != None:
        return queryResult.next()
    else:
        return None

def updateImageNode(imageNode, uuid, contentType, filepath, width, height, offsetX, offsetY, landmarkFilepath, metadataFilepath, timestamp):
    imageNode.neo4jId = imageNode.eid
    imageNode.uuid = uuid
    imageNode.contentType = contentType
    imageNode.filepath = filepath
    imageNode.width = width
    imageNode.height = height
    #  imageNode.ijip = ijip
    imageNode.landmarkOffsetX = offsetX
    imageNode.landmarkOffsetY = offsetY
    imageNode.landmarkRefFilepath = landmarkFilepath
    imageNode.metadataFilepath = metadataFilepath
    imageNode.timestamp = timestamp
    imageNode.save()
    return imageNode

'''
    script = getScriptByNamespaceMethodName(g, 'ListCluster', 'createImageNode')
    #g.scripts.get('createImageNode')
    params =dict(uuid=uuid,contentType=contentType,filepath=filepath, offsetX=offsetX, offsetY=offsetY, landmarkFilepath=landmarkFilepath, metadataFilepath=metadataFilepath, timestamp=timestamp)
    queryResult = g.gremlin.query(script, params)

    return queryResult
    #if queryResult != None:
    #	return list(queryResult)[0]
    #else:
    #	return None
'''
def createImageNode(uuid, contentType, filepath, width, height, offsetX, offsetY, landmarkFilepath, metadataFilepath, timestamp):
    imageNode = g.vertices.create(name="image")
    imageNode.neo4jId = imageNode.eid
    imageNode.uuid = uuid
    imageNode.nodetype = 'image'
    imageNode.contentType = contentType
    imageNode.filepath = filepath
    imageNode.width = width
    imageNode.height = height
    #  imageNode.ijip = ijip
    imageNode.landmarkOffsetX = offsetX
    imageNode.landmarkOffsetY = offsetY
    imageNode.landmarkRefFilepath = landmarkFilepath
    imageNode.metadataFilepath = metadataFilepath
    imageNode.timestamp = timestamp
    imageNode.save()
    return imageNode

def createExperimentNode(uuid, expType, metadataFilepath, details):
    expNode = g.vertices.create(name="experiment")
    expNode.uuid = uuid
    expNode.nodetype = 'exp'
    expNode.neo4jId = expNode.eid
    expNode.experimentType = expType
    expNode.metadataFilepath = metadataFilepath
    expNode.details = details
    expNode.save()
    return expNode

def createClusterNodeByImgUUID(cluster, clustercoords, clusterarea, imgUUID, clusterIdx):
    clusterNode = g.vertices.create(name="cluster")
    # retrieve information based on imgUUID
    #imgNode = getLinkedImageNodeByUUID(imgUUID)
    imgNode = getNodeByUUID(imgUUID)
    #expNode = getExpNodeByImageId(imgNode.neo4jId)
    expNode = getExpNodeByImageUUID(imgUUID)
    temporalIdx = imgNode.timestamp
    clusterUUID = issueUUID() #str(uuid.uuid4())

    clusterNode.imageRefUUID = imgUUID
    clusterNode.expRef = expNode.uuid
    clusterNode.neo4jId = clusterNode.eid
    clusterNode.imageRef = temporalIdx
    clusterNode.clusterRef = clusterIdx
    clusterNode.uuid = clusterUUID
    clusterNode.landmarkOffsetX = imgNode.landmarkOffsetX
    clusterNode.landmarkOffsetY = imgNode.landmarkOffsetY
    clusterNode.timestamp = imgNode.timestamp
    clusterNode.nodetype = 'cluster'
    clusterNode.roi = json.dumps(clustercoords)
    clusterNode.blobcount = len(cluster)
    clusterNode.size = clusterarea
    clusterNode.save()
    for blob in cluster:
        linkClusterHasBlob(clusterNode, blob)

    linkImageHasCluster(imgNode, clusterNode)
    
    return clusterNode


def createClusterNode(cluster, expId, temporalIdx, clusterIdx):
    clusterNode = g.vertices.create(name="cluster")

    clusterUUID = issueUUID() #str(uuid.uuid4())

    clusterNode.expRef = expId
    clusterNode.neo4jId = clusterNode.eid
    clusterNode.imageRef = temporalIdx
    clusterNode.clusterRef = clusterIdx
    clusterNode.uuid = clusterUUID
    clusterNode.nodetype = 'cluster'
    clusterNode.save()
    for blob in cluster:
        linkClusterHasBlob(clusterNode, blob)

    return clusterNode

def addImageStatsNode(imageNode, statUUID, statsDict, statsType):
    statsNode = g.vertices.create(name="imgStats")
    statsNode.uuid = statUUID
    statsNode.neo4jId = statsNode.eid
    statsNode.stats = statsDict
    statsNode.statsType = statsType
    statsNode.save()
    linkImageHasStats(imageNode, statsNode)

    #statsNodeDict = statsNode._data
    return statsNode #statsNodeDict

def addBlobStatsNode(imgStatsNode, blobNode, statsDict, statsType):
    statsNode = g.vertices.create(name="blobStats")
    statsNode.neo4jId = statsNode.eid
    statsNode.stats = statsDict
    statsNode.statsType = statsType
    statsNode.save()
    linkBlobHasStats(blobNode, statsNode)
    linkImgStatsHasBlobStats(imgStatsNode, statsNode)
    return statsNode

def addBlobStatsToImageNode(statsUUID, blobNode, statsData, statsType):
    imgStatsNode = getNodeByUUID(statsUUID)
    return addBlobStatsNode(imgStatsNode, blobNode, statsData, statsType)


def linkImageHasStats(imageNode, imgStatsNode):
    edge = g.edges.create(imageNode, "image_has_stats", imgStatsNode)
    edge.save()

def linkImageHasCluster(imageNode, clusterNode):
    edge = g.edges.create(imageNode, "image_has_cluster", clusterNode)
    edge.save()
    
def linkImgStatsHasBlobStats(imgStatsNode, blobStatsNode):
    edge = g.edges.create(imgStatsNode, "imgStats_has_blobStats", blobStatsNode)
    edge.save()
    
def linkBlobHasStats(blobNode, statsNode):
    edge = g.edges.create(blobNode, "blob_has_stats", statsNode)
    edge.save()

def linkClusterHasBlob(clusterNode, blobNode):
    edge = g.edges.create(clusterNode, "cluster_has_blob", blobNode)
    edge.save()

def linkBlob_is_near_Blob(blob1, blob2, weight):
    edge = g.edges.create(blob1, "at_t1_is_near_at_t1", blob2)
    edge.weight = float(weight)
    edge.save()

def linkBlob_subsequently_coincides_Blob(blob1, blob2, weight):
    edge = g.edges.create(blob1, "at_t1_subsequently_coincides_at_t2", blob2)
    edge.weight = float(weight)
    edge.save()

def linkBlob_temporally_corresponds_Blob(blob1, blob2, weight=-1.0):
    edge = g.edges.create(blob1, "at_t1_temporally_corresponds_at_t2", blob2)
    edge.weight = float(weight)
    edge.save()

def linkBlobToImage(blob, image):
    edge = g.edges.create(blob, "belongsToImage", image)
    edge.save()    
    
def linkBFImageToFluoImage(bfImg, fluoImg):
    edge = g.edges.create(bfImg, "bfImg_has_fluoImg", fluoImg)
    edge.save()    

def linkImageToExp(image, exp):
    edge = g.edges.create(image, "belongsToExp", exp)
    edge.save()

def linkCorrespondingClusters(cluster1, cluster2, weight):
    edge = g.edges.create(cluster1, "cluster_corresponds_to", cluster2)
    edge.weight = float(weight)
    edge.save()


def getScriptByNamespaceMethodName(graph, namespace, method):
    return graph.scripts.namespace_map[namespace][method].body


#### #python wrapper function for a gremlin-groovy function
def removeLinksByLabel(labelStr):
    #script = g.scripts.get('removeEdgesByLabelStr')
    script = getScriptByNamespaceMethodName(g, 'ListCluster', 'removeEdgesByLabelStr')
    params = dict(str=labelStr)
    queryResult = g.gremlin.query(script, params)

    return queryResult
    #if queryResult != None:
    #	return list(queryResult)[0]
    #else:
    #	return None

def removeInterFrameLinksByImageId(imageId):
    #script = g.scripts.get('removeInterFrameEdgesByImageId')
    script = getScriptByNamespaceMethodName(g, 'ListCluster', 'removeInterFrameEdgesByImageId')
    params = dict(id=imageId)
    g.gremlin.query(script, params)

def removeIntraFrameLinksByImageId(imageId):
    #script = g.scripts.get('removeIntraFrameEdgesByImageId')
    script = getScriptByNamespaceMethodName(g, 'ListCluster', 'removeIntraFrameEdgesByImageId')
    params = dict(id=imageId)
    g.gremlin.query(script, params)

def removeBlobNodesByImageId(imageId):
    #script = g.scripts.get('removeBlobNodesByImageId')
    script = getScriptByNamespaceMethodName(g, 'ListCluster', 'removeBlobNodesByImageId')
    params = dict(id=imageId)
    g.gremlin.query(script, params)

def removeANodeAndConnectedEdgesByVertexId(nodeId):
    #script = g.scripts.get('removeANodeAndConnectedEdgesByVertexId')
    script = getScriptByNamespaceMethodName(g, 'ListCluster', 'removeANodeAndConnectedEdgesByVertexId')
    params = dict(id=nodeId)
    g.gremlin.query(script, params)

def removeNodesAndConnectedEdgesByImageId(imageId):
    #script = g.scripts.get('removeNodesAndConnectedEdgesByImageId')
    script = getScriptByNamespaceMethodName(g, 'ListCluster', 'removeNodesAndConnectedEdgesByImageId')
    params = dict(id=imageId)
    g.gremlin.query(script, params)

def getBlobNodeByEID(eId):
    script1 = getScriptByNamespaceMethodName(g, 'ListCluster', 'getBlobNodeByEID')
    params1 = dict(eid=eId)
    queryResult = g.gremlin.query(script1, params1)
    if queryResult != None:
        return queryResult.next()
    else:
        return None
'''
    nodeList = g.vertices.index.lookup(eid=str(eId))
    if nodeList:
        return nodeList.next()
    else:
        return None
'''

def getTemporalRefsByExpImgIdx(exp_uuid):
    #print exp
    #print temporalIdx
    #print 'here'
    # return all blob nodes for [temporalIdx]th image sequence in exp
    script1 = getScriptByNamespaceMethodName(g, 'ListCluster', 'listAllTemporalRefsByExpUUIDImgIdx')
    params1 = dict(exp_uuid=exp_uuid)
    queryResult = g.gremlin.query(script1, params1)
    if queryResult != None:
        refslist = [x.timestamp for x in list(queryResult)]
        refslist.sort()
        return refslist
    else:
        return None

def getBlobsByExpImgIdx(exp_uuid, temporalIdx):
    #print exp
    #print temporalIdx
    #print 'here'
    # return all blob nodes for [temporalIdx]th image sequence in exp
    temporalRef_list = getTemporalRefsByExpImgIdx(exp_uuid)
    temporalRef = None
    if len(temporalRef_list) > temporalIdx:
        temporalRef = temporalRef_list[temporalIdx]
    else:
        return None

    script1 = getScriptByNamespaceMethodName(g, 'ListCluster', 'listAllBlobNodesByExpUUIDImgIdx')
    params1 = dict(exp_uuid=exp_uuid, temporalRef=temporalRef)
    queryResult = g.gremlin.query(script1, params1)
    if queryResult != None:
        return list(queryResult)
    else:
        return None

def getBlobsByImgUUID(uuidStr):
    # return all blob nodes for the image with the given uuid 
    script1 = getScriptByNamespaceMethodName(g, 'ListCluster', 'listAllBlobNodesByImgUUID')
    params1 = dict(uuid=uuidStr)
    queryResult = g.gremlin.query(script1, params1)
    if queryResult != None:
        return list(queryResult)
    else:
        return None

def getExpNodeByImageUUID(imgUUID):
    script1 = getScriptByNamespaceMethodName(g, 'ListCluster', 'getExpNodeByImgUUID')
    params1 = dict(uuid=imgUUID)
    queryResult = g.gremlin.query(script1, params1)
    if queryResult != None:
        return queryResult.next()
    else:
        return None

def getExpNodeByImageId(imgId):
    script1 = getScriptByNamespaceMethodName(g, 'ListCluster', 'getExpNodeByImageId')
    params1 = dict(neo4jId=imgId)
    queryResult = g.gremlin.query(script1, params1)
    if queryResult != None:
        return queryResult.next()
    else:
        return None
'''
    nodeList = g.vertices.index.lookup(neo4jId=str(imgId))
    #nodeList = g.vertices.index.lookup(uuid=uuidStr)
    if nodeList:
        return nodeList.next().outV('belongsToExp').next()
    else:
        return None
'''

# the uuid supplied shall be for a fluorescence image as image stats are assigned to a fluorescence image
def getStatsNodesByImgUUID(uuidStr):
    script1 = getScriptByNamespaceMethodName(g, 'ListCluster', 'getStatsNodesByImgUUID')
    params1 = dict(uuid=uuidStr)
    queryResult = g.gremlin.query(script1, params1)
    if queryResult != None:
        return queryResult.next()
    else:
        return None
'''
    nodeList = g.vertices.index.lookup(uuid=uuidStr)
    #nodeList = g.vertices.index.lookup(uuid=uuidStr)
    if nodeList:
        return nodeList.next().outV('image_has_stats').next()
    else:
        return None
'''

# the uuid supplied shall be for a bf image, as a bf image has an out going edge 'bfImg_has_fluoImg' into a fluorescence image
def getLinkedImageNodeByUUID(uuidStr):
    script1 = getScriptByNamespaceMethodName(g, 'ListCluster', 'getLinkedImageNodeByUUID')
    params1 = dict(uuid=uuidStr)
    queryResult = g.gremlin.query(script1, params1)
    if queryResult != None:
        return queryResult.next()
    else:
        return None
'''
    nodeList = g.vertices.index.lookup(uuid=uuidStr)
    #nodeList = g.vertices.index.lookup(uuid=uuidStr)
    if nodeList:
        return nodeList.next().outV('bfImg_has_fluoImg').next()
    else:
        return None
'''

def getImageNodeByBlobUUID(blobUUID):
    script1 = getScriptByNamespaceMethodName(g, 'ListCluster', 'getImageNodeByBlobUUID')
    params1 = dict(blobUUID=blobUUID)
    queryResult = g.gremlin.query(script1, params1)
    if queryResult != None:
        return queryResult.next()
    else:
        return None

def doFindConsensusCluster(idList):
    #script = g.scripts.get('findConsensusCluster')
    script = getScriptByNamespaceMethodName(g, 'ListCluster', 'findConsensusCluster')
    params = dict(vIds=idList)
    queryResult = g.gremlin.query(script, params)
    
    if queryResult != None:
        return list(queryResult)[0]
    else:
        return None

def doFindColocalizedClusterSetsByUUID(clusterUUID):
    script = getScriptByNamespaceMethodName(g, 'ListCluster', 'findColocalizedClusterSets')
    params = dict(clusterUUID=clusterUUID)
    queryResult = g.gremlin.execute(script, params)
    
    if queryResult != None:
        return queryResult.content['results']
    else:
        return None

def doFindCorrespondingClusterChainByUUID(uuid, maxChainLen):
    script = getScriptByNamespaceMethodName(g, 'ListCluster', 'findCorrespondingClusterChain')
    params = dict(maxChainLen=maxChainLen, uuid=uuid)
    queryResult = g.gremlin.query(script, params)
    
    if queryResult != None:
        return list(queryResult)
    else:
        return None

def doFindCorrespondingBlob(groupLen, idList):
    #script = g.scripts.get('findCorrespondingBlob')
    script = getScriptByNamespaceMethodName(g, 'ListCluster', 'findCorrespondingBlob')
    params = dict(howMany=groupLen, vIds=idList)
    queryResult = g.gremlin.query(script, params)
    
    if queryResult != None:
        return list(queryResult)
    else:
        return None

# this function returns a dict with two entries, 'vertices' and 'edges'.
# the vertices entry contains a list of paths consisting of single cells
# the edges entry contains a list of paths consisting of correspondence edges bridging the single cells
def doFindCorrespondingBlobChainsByUUIDs(uuidlist, maxChainLen):
    #script = g.scripts.get('findCorrespondingBlob')
    script = getScriptByNamespaceMethodName(g, 'ListCluster', 'findCorrespondingBlobChain')
    params = dict(maxChainLen=maxChainLen, uuidlist=uuidlist)
    queryResult = g.gremlin.execute(script, params) # use of execute instead of query to allow a list of lists in the results
    
    if queryResult != None:
#        return queryResult.content['results']
        results = queryResult.content['results']
        return dict(vertices=[[b for b in p if 'name' in b] for p in results], edges=[[e for e in p if 'weight' in e] for p in results])
    else:
        return None

def getTemporalCellChainsByInitCellUUIDs(uuidlist, cell_chain_limit):
    return doFindCorrespondingBlobChainsByUUIDs(uuidlist, cell_chain_limit)


def removeListFromList(listA, listB):
    for a in listA:
        try:
            listB.remove(a)
        except ValueError:
            print str(a) + ' not in list, ignoring...'
            pass # do nothing

    return listB

def getLinkedFluoImages(bfImgNodeId):
    #script = g.scripts.get('listAllFluoImgNodesLinkedToBFImgNode')
    script = getScriptByNamespaceMethodName(g, 'ListCluster', 'listAllFluoImgNodesLinkedToBFImgNode')
    params = dict(nodeId=bfImgNodeId)
    fluoNodes = g.gremlin.query(script, params)
    
    if fluoNodes != None:
        return list(fluoNodes)
    else:
        return []

def getLinkedFluoImagesByUUID(bfImgUUID):
    imgNode = getNodeByUUID(bfImgUUID)
    bfImgNodeId = imgNode.eid
    fluoNodes = getLinkedFluoImages(bfImgNodeId)

    fluoNodeDicts = [node._data for node in fluoNodes]
    return fluoNodeDicts

def getLinkedFluoImageUUIDs(bfImgUUID):
    imgNode = getNodeByUUID(bfImgUUID)
    bfImgNodeId = imgNode.eid
    fluoNodes = getLinkedFluoImages(bfImgNodeId)

    uuidList = [node.uuid for node in fluoNodes]

    return uuidList

def getTemporalClusterPairs(exp, temporalIdx):
    #script = g.scripts.get('listAllTemporalClusterPairsByExpIdImgRef')
    script = getScriptByNamespaceMethodName(g, 'ListCluster', 'listAllTemporalClusterPairsByExpIdImgRef')
    params = dict(expId=exp, temporalRef=temporalIdx)
    clusterTPairs = list(g.gremlin.query(script, params))
    
    return clusterTPairs

def getClusterTimestamp(clusterNode):
    return clusterNode.outV('cluster_has_blob').next().timestamp

def getTemporalKeyClusterChain(cId, chainLimit):
    #script = g.scripts.get('listTemporalKeyClusterChainByClusterNodeId')
    script = getScriptByNamespaceMethodName(g, 'ListCluster', 'listTemporalKeyClusterChainByClusterNodeId')
    params = dict(clusterId=cId, n=chainLimit)
    clusterChain = g.gremlin.query(script, params)

    if clusterChain != None:
        return list(clusterChain)
    else:
        return []

def getTemporalClusterChain(cId, chainLimit):
    #script = g.scripts.get('listTemporalClusterChainByClusterNodeId')
    script = getScriptByNamespaceMethodName(g, 'ListCluster', 'listTemporalClusterChainByClusterNodeId')
    params = dict(clusterId=cId, n=chainLimit)
    clusterChain = g.gremlin.query(script, params)

    if clusterChain != None:
        return list(clusterChain)
    else:
        return []

def getClusterStats(nodeId):
    #script = g.scripts.get('listAllBlobStatsByClusterId')
    script = getScriptByNamespaceMethodName(g, 'ListCluster', 'listAllBlobStatsByClusterId')
    params = dict(clusterNodeId=nodeId)
    clusterStats = g.gremlin.query(script, params)

    if clusterStats != None:
        return list(clusterStats)
    else:
        return []

def getCellStatsByUUID(cell_uuid):
    script = getScriptByNamespaceMethodName(g, 'ListCluster', 'getCellStatsByUUID')
    params = dict(cell_uuid=cell_uuid)
    cell_stats = g.gremlin.query(script, params)

    if cell_stats != None:
        return list(cell_stats)
    else:
        return []


def getBlobNodesByClusterUUID(clusterUUID):
    #script = g.scripts.get('listAllBlobStatsByClusterId')
    script = getScriptByNamespaceMethodName(g, 'ListCluster', 'listAllBlobNodesByClusterUUID')
    params = dict(clusterUUID=clusterUUID)
    queryResult = g.gremlin.query(script, params)

    if queryResult != None:
        return list(queryResult)
    else:
        return []

# a cluster consists of a group of blobs in an image
def getClusterObjListByImgUUID(uuid, maxClusteringDist):
    #print exp
    #print temporalIdx
    #print 'here'
    # return all blob nodes for [temporalIdx]th image sequence in exp
    #script1 = g.scripts.get('listAllBlobNodesByExpIdImgIdx')
    #script1 = getScriptByNamespaceMethodName(g, 'ListCluster', 'listAllBlobNodesByExpIdImgIdx')
    script1 = getScriptByNamespaceMethodName(g, 'ListCluster', 'listAllBlobNodesByImgUUID')

    params1 = dict(uuid=uuid)
    #script = g.scripts.get('listClusterByNodeId')
    #params = dict(expId=2760)
    blobs = list(g.gremlin.query(script1, params1))
    
    #print 'blob len: ' + str(len(blobs))
    #script2 = g.scripts.get('listClusterByNodeId')
    script2 = getScriptByNamespaceMethodName(g, 'ListCluster', 'listClusterByNodeId')

    clusters = []
    while len(blobs) > 0:
        blob = blobs.pop()
        params2 = dict(id=blob.neo4jId, maxClusteringDist=maxClusteringDist)
        clusterObj = g.gremlin.query(script2, params2) # retrieve blobs clustered with the current blob
        #print clusterObj
        print 'cur blob: ' + str(blob)
        blobList = []
        if clusterObj != None:
            blobList = list(clusterObj)
        if len(blobList) == 0: # when blob is on its own without neighbors
            blobList.append(blob)

        #blobList.append(blob) # make a full list of clustering blobs, including the current blob
        #def f(x): return x not in blobList # function def
        #blobs = filter(f, blobs) # remove current blobList from the master blobs list
        clusters.append(list(blobList)) # add (the copy of) the current blobList (aka cluster) to the list of identified clusters
        print blobList
        print blob
        if len(blobList) > 1:
            blobList.remove(blob) # remove current blob, which was popped out from the blobs list earlier on
            print blobList
            blobs = removeListFromList(blobList, blobs) # remove blobList (aka cluster) from the blobs list
        
    return clusters


def getClusterObjList(exp, temporalIdx, maxClusteringDist):
    #print exp
    #print temporalIdx
    #print 'here'
    # return all blob nodes for [temporalIdx]th image sequence in exp
    #script1 = g.scripts.get('listAllBlobNodesByExpIdImgIdx')
    script1 = getScriptByNamespaceMethodName(g, 'ListCluster', 'listAllBlobNodesByExpIdImgIdx')
    params1 = dict(expId=exp, temporalRef=temporalIdx)
    #script = g.scripts.get('listClusterByNodeId')
    #params = dict(expId=2760)
    blobs = list(g.gremlin.query(script1, params1))
    
    #print 'blob len: ' + str(len(blobs))
    #script2 = g.scripts.get('listClusterByNodeId')
    script2 = getScriptByNamespaceMethodName(g, 'ListCluster', 'listClusterByNodeId')

    clusters = []
    while len(blobs) > 0:
        blob = blobs.pop()
        params2 = dict(id=blob.neo4jId, maxClusteringDist=maxClusteringDist)
        clusterObj = g.gremlin.query(script2, params2)
        #print clusterObj
        blobList = []
        if clusterObj != None:
            blobList = list(clusterObj)
        blobList.append(blob) # make a full list of clustering blobs
        #def f(x): return x not in blobList # function def
        #blobs = filter(f, blobs) # remove current blobList from the master blobs list
        clusters.append(list(blobList))
        #print blobList
        #print blob
        if len(blobList) > 1:
            blobList.remove(blob)
            #print blobList
            blobs = removeListFromList(blobList, blobs)
        
    return clusters

def getClusterList(exp, temporalIdx):
        #print exp
        #print temporalIdx
        # return all cluster nodes for [temporalIdx]th image sequence in exp
        #script1 = g.scripts.get('listAllClusterNodesByExpIdImgIdx')
        script1 = getScriptByNamespaceMethodName(g, 'ListCluster', 'listAllClusterNodesByExpIdImgIdx')
        params1 = dict(expId=exp, temporalRef=temporalIdx)
        clusters = list(g.gremlin.query(script1, params1))

        return clusters

def getMotherGenerationCells():
    script = getScriptByNamespaceMethodName(g, 'ListCluster', 'listMotherGenerationCells')
    results = g.gremlin.execute(script) # used execute instead of query to deal with complex return data (list of lists)
    r_data, r_code = results.get_results()
    r_list = list(r_data)

    # convert the complex return data into python data struct
    mom_gen_cells = []
    for cpath in r_list:
        mom_gen_cells.append(cpath.data)

    return mom_gen_cells

def getNextGenerationCells(cuuid_list):

    script = getScriptByNamespaceMethodName(g, 'ListCluster', 'listNextGenerationCells')
    params = dict(uuidlist=cuuid_list)
    results = g.gremlin.execute(script,params) # used execute instead of query to deal with complex return data (list of lists)
    r_data, r_code = results.get_results()
    r_list = list(r_data)

    # convert the complex return data into python data struct
    n_gen_cells = []
    for cpath in r_list:
        n_gen_cells.append(cpath.data)

    return n_gen_cells

def getCellsWithStats():
    script = getScriptByNamespaceMethodName(g, 'ListCluster', 'listCellsWithStats')
    results = g.gremlin.query(script)

    if results != None:
        return list(results) #.next()
    else:
        return []

def stepBackNGenerations(cell_uuid, n_gens):
    script = getScriptByNamespaceMethodName(g, 'ListCluster', 'stepBackNGenerations')
    params = dict(cell_uuid=cell_uuid,n_gens=n_gens)
    results = g.gremlin.query(script, params)

    if results != None:
        return list(results) #.next()
    else:
        return []

def getPeers(cell_uuid):
    script = getScriptByNamespaceMethodName(g, 'ListCluster', 'getPeers')
    params = dict(cell_uuid=cell_uuid)
    results = g.gremlin.query(script, params)

    if results != None:
        return list(results) #.next()
    else:
        return []

def getCellStats(cuuid_list):
    script = getScriptByNamespaceMethodName(g, 'ListCluster', 'getCellStats')
    params = dict(cuuid_list=cuuid_list)
    results = g.gremlin.query(script, params)

    if results != None:
        return list(results) #.next()
    else:
        return []


def saveGDBIntoFile(filepath):
    script = getScriptByNamespaceMethodName(g, 'ListCluster', 'saveGDBIntoFile')
    params = dict(filepath=filepath)
    results = g.gremlin.query(script, params)

    if results != None:
        return list(results) #.next()
    else:
        return []

def loadGDBFromFile(filepath):
    script = getScriptByNamespaceMethodName(g, 'ListCluster', 'loadGDBFromFile')
    params = dict(filepath=filepath)
    results = g.gremlin.query(script, params)

    if results != None:
        return list(results) #.next()
    else:
        return []

def clearGDB():
    script = getScriptByNamespaceMethodName(g, 'ListCluster', 'clearGDB')
    results = g.gremlin.query(script)

    if results != None:
        return list(results) #.next()
    else:
        return []

