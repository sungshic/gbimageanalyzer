from __future__ import absolute_import
from .celery_server import celery

import sys
#from bulbs.neo4jserver import Graph, Config, NEO4J_URI
from bulbs.rexster import Graph, Config

from bulbs.model import Node, Relationship
from bulbs.property import String, Integer, DateTime

import json 

my_rexster_uri = 'http://localhost:8180/graphs/neo4jsample'
config = Config(my_rexster_uri)
#g=Graph(config)
g=Graph(config)
g.scripts.update('../neo4jGremlinQueries/ListCluster.groovy')

"""
blob
 landmarkOffset (xCoord, yCoord)
 roi coords, width, height
 timestamp

image
 contentType (e.g. BF, GFP, RFP)
 filepath
 ijip
 landmarkOffset (xCoord, yCoord)
 landmarkRefFilepath
 metadataFilepath
 timestamp

experiment
 experimentType (e.g. "Timelapse microscopy")
 metadataFilepath
 details

blob at_t1_is_near_at_t1 blob
blob at_t1_subsequently_coincide_at_t2 blob 

blob belongsToImage image(BF)
blob belongsToImage image(GFP)
blob belongsToImage image(RFP)

image belongsToExp experiment
"""

class Blob(Node):
    element_type = "cellblob"

    landmarkOffsetX = Integer()
    landmarkOffsetY = Integer()
    roi = object()
    timestampe = Integer()

class Image(Node):
    element_type = "image"
    
    contentType = String()
    filepath = String()
    ijip = object()
    landmarkOffsetX = Integer()
    landmarkOffsetY = Integer()
    landmarkRefFilepath = String()
    metadataFilepath = String()
    timestamp = Integer()

class Experiment(Node):
    element_type = "experiment"

    experimentType = String()
    metadataFilepath = String()
    details = String()

### the following functions do not implement the proxy model graph handling yet

def _createBlobNode(offsetX, offsetY, roi, timestamp):
    blob = g.vertices.create(name="blob")
    blob.neo4jId = blob.eid
    blob.landmarkOffsetX = offsetX
    blob.landmarkOffsetY = offsetY
    blob.roi = roi
    blob.timestamp = timestamp
    blob.save()
    return blob

def _getNodeByUUID(uuidStr):
	nodeList = g.vertices.index.lookup(uuid=uuidStr)
	
	if (nodeList != None):
		return nodeList.next()
	else:
		return None

def _updateImageNode(imageNode, uuid, contentType, filepath, ijip, offsetX, offsetY, landmarkFilepath, metadataFilepath, timestamp):
    imageNode.neo4jId = imageNode.eid
    imageNode.uuid = uuid
    imageNode.contentType = contentType
    imageNode.filepath = filepath
    #  imageNode.ijip = ijip
    imageNode.landmarkOffsetX = offsetX
    imageNode.landmarkOffsetY = offsetY
    imageNode.landmarkRefFilepath = landmarkFilepath
    imageNode.metadataFilepath = metadataFilepath
    imageNode.timestamp = timestamp
    imageNode.save()
    return imageNode

def _createImageNode(uuid, contentType, filepath, ijip, offsetX, offsetY, landmarkFilepath, metadataFilepath, timestamp):
    imageNode = g.vertices.create(name="image")
    imageNode.neo4jId = imageNode.eid
    imageNode.uuid = uuid
    imageNode.contentType = contentType
    imageNode.filepath = filepath
    #  imageNode.ijip = ijip
    imageNode.landmarkOffsetX = offsetX
    imageNode.landmarkOffsetY = offsetY
    imageNode.landmarkRefFilepath = landmarkFilepath
    imageNode.metadataFilepath = metadataFilepath
    imageNode.timestamp = timestamp
    imageNode.save()
    return imageNode

def _createExperimentNode(uuid, expType, metadataFilepath, details):
    expNode = g.vertices.create(name="experiment")
    expNode.uuid = uuid
    expNode.neo4jId = expNode.eid
    expNode.experimentType = expType
    expNode.metadataFilepath = metadataFilepath
    expNode.details = details
    expNode.save()
    return expNode

def _createClusterNode(cluster, expId, temporalIdx, clusterIdx):
    clusterNode = g.vertices.create(name="cluster")
    clusterNode.expRef = expId
    clusterNode.neo4jId = clusterNode.eid
    clusterNode.imageRef = temporalIdx
    clusterNode.clusterRef = clusterIdx
    clusterNode.save()
    for blob in cluster:
        linkClusterHasBlob(clusterNode, blob)
    
    return clusterNode

def _addImageStatsNode(imageNode, statsDict, statsType):
    statsNode = g.vertices.create(name="imgStats")
    statsNode.neo4jId = statsNode.eid
    statsNode.stats = statsDict
    statsNode.statsType = statsType
    statsNode.save()
    linkImageHasStats(imageNode, statsNode)
    return statsNode

def _addBlobStatsNode(imgStatsNode, blobNode, statsDict, statsType):
    statsNode = g.vertices.create(name="blobStats")
    statsNode.neo4jId = statsNode.eid
    statsNode.stats = statsDict
    statsNode.statsType = statsType
    statsNode.save()
    linkBlobHasStats(blobNode, statsNode)
    linkImgStatsHasBlobStats(imgStatsNode, statsNode)
    return statsNode

	

def _linkImageHasStats(imageNode, imgStatsNode):
    edge = g.edges.create(imageNode, "image_has_stats", imgStatsNode)
    edge.save()
    
def _linkImgStatsHasBlobStats(imgStatsNode, blobStatsNode):
    edge = g.edges.create(imgStatsNode, "imgStats_has_blobStats", blobStatsNode)
    edge.save()
    
def _linkBlobHasStats(blobNode, statsNode):
    edge = g.edges.create(blobNode, "blob_has_stats", statsNode)
    edge.save()

def _linkClusterHasBlob(clusterNode, blobNode):
    edge = g.edges.create(clusterNode, "cluster_has_blob", blobNode)
    edge.save()

def _linkBlob_is_near_Blob(blob1, blob2, weight):
    edge = g.edges.create(blob1, "at_t1_is_near_at_t1", blob2)
    edge.weight = int(weight)
    edge.save()

def _linkBlob_subsequently_coincides_Blob(blob1, blob2, weight):
    edge = g.edges.create(blob1, "at_t1_subsequently_coincides_at_t2", blob2)
    edge.weight = int(weight)
    edge.save()

def _linkBlobToImage(blob, image):
    edge = g.edges.create(blob, "belongsToImage", image)
    edge.save()    
    
def _linkBFImageToFluoImage(bfImg, fluoImg):
    edge = g.edges.create(bfImg, "bfImg_has_fluoImg", fluoImg)
    edge.save()    

def _linkImageToExp(image, exp):
    edge = g.edges.create(image, "belongsToExp", exp)
    edge.save()

def _linkCorrespondingClusters(cluster1, cluster2):
    edge = g.edges.create(cluster1, "cluster_corresponds_to", cluster2)
    edge.save()


def _getScriptByNamespaceMethodName(graph, namespace, method):
    return graph.scripts.namespace_map[namespace][method].body


#### #python wrapper function for a gremlin-groovy function
def _removeLinksByLabel(labelStr):
	#script = g.scripts.get('removeEdgesByLabelStr')
    script = getScriptByNamespaceMethodName(g, 'ListCluster', 'removeEdgesByLabelStr')
    params = dict(str=labelStr)
    queryResult = g.gremlin.query(script, params)

    return queryResult
	#if queryResult != None:
	#	return list(queryResult)[0]
	#else:
	#	return None

def _removeInterFrameLinksByImageId(imageId):
	#script = g.scripts.get('removeInterFrameEdgesByImageId')
    script = getScriptByNamespaceMethodName(g, 'ListCluster', 'removeInterFrameEdgesByImageId')
    params = dict(id=imageId)
    g.gremlin.query(script, params)

def _removeIntraFrameLinksByImageId(imageId):
	#script = g.scripts.get('removeIntraFrameEdgesByImageId')
    script = getScriptByNamespaceMethodName(g, 'ListCluster', 'removeIntraFrameEdgesByImageId')
    params = dict(id=imageId)
    g.gremlin.query(script, params)

def _removeBlobNodesByImageId(imageId):
	#script = g.scripts.get('removeBlobNodesByImageId')
    script = getScriptByNamespaceMethodName(g, 'ListCluster', 'removeBlobNodesByImageId')
    params = dict(id=imageId)
    g.gremlin.query(script, params)

def _removeANodeAndConnectedEdgesByVertexId(nodeId):
	#script = g.scripts.get('removeANodeAndConnectedEdgesByVertexId')
    script = getScriptByNamespaceMethodName(g, 'ListCluster', 'removeANodeAndConnectedEdgesByVertexId')
    params = dict(id=nodeId)
    g.gremlin.query(script, params)
	
def _removeNodesAndConnectedEdgesByImageId(imageId):
	#script = g.scripts.get('removeNodesAndConnectedEdgesByImageId')
    script = getScriptByNamespaceMethodName(g, 'ListCluster', 'removeNodesAndConnectedEdgesByImageId')
    params = dict(id=imageId)
    g.gremlin.query(script, params)

def _getExpNodeByImageId(imgId):
    nodeList = g.vertices.index.lookup(neo4jId=str(imgId))
	#nodeList = g.vertices.index.lookup(uuid=uuidStr)
    if nodeList:
        return nodeList.next().outV('belongsToExp').next()
    else:
        return None

def _getStatNodeByUUID(uuidStr):
    nodeList = g.vertices.index.lookup(uuid=uuidStr)
	#nodeList = g.vertices.index.lookup(uuid=uuidStr)
    if nodeList:
        return nodeList.next().outV('image_has_stats').next()
    else:
        return None

def _getLinkedImageNodeByUUID(uuidStr):
    nodeList = g.vertices.index.lookup(uuid=uuidStr)
	#nodeList = g.vertices.index.lookup(uuid=uuidStr)
    if nodeList:
        return nodeList.next().outV('bfImg_has_fluoImg').next()
    else:
        return None

def _doFindConsensusCluster(idList):
    #script = g.scripts.get('findConsensusCluster')
    script = getScriptByNamespaceMethodName(g, 'ListCluster', 'findConsensusCluster')
    params = dict(vIds=idList)
    queryResult = g.gremlin.query(script, params)
    
    if queryResult != None:
        return list(queryResult)[0]
    else:
        return None


def _doFindCorrespondingBlob(groupLen, idList):
    #script = g.scripts.get('findCorrespondingBlob')
    script = getScriptByNamespaceMethodName(g, 'ListCluster', 'findCorrespondingBlob')
    params = dict(howMany=groupLen, vIds=idList)
    queryResult = g.gremlin.query(script, params)
    
    if queryResult != None:
        return list(queryResult)
    else:
        return None

def _removeListFromList(listA, listB):
    for a in listA:
            listB.remove(a)
    return listB

def _getLinkedFluoImages(bfImgNodeId):
    #script = g.scripts.get('listAllFluoImgNodesLinkedToBFImgNode')
    script = getScriptByNamespaceMethodName(g, 'ListCluster', 'listAllFluoImgNodesLinkedToBFImgNode')
    params = dict(nodeId=bfImgNodeId)
    fluoNodes = g.gremlin.query(script, params)
    
    if fluoNodes != None:
        return list(fluoNodes)
    else:
        return []


def _getTemporalClusterPairs(exp, temporalIdx):
    #script = g.scripts.get('listAllTemporalClusterPairsByExpIdImgRef')
    script = getScriptByNamespaceMethodName(g, 'ListCluster', 'listAllTemporalClusterPairsByExpIdImgRef')
    params = dict(expId=exp, temporalRef=temporalIdx)
    clusterTPairs = list(g.gremlin.query(script, params))
    
    return clusterTPairs

def _getClusterTimestamp(clusterNode):
	return clusterNode.outV('cluster_has_blob').next().timestamp

def _getTemporalKeyClusterChain(cId, chainLimit):
	#script = g.scripts.get('listTemporalKeyClusterChainByClusterNodeId')
    script = getScriptByNamespaceMethodName(g, 'ListCluster', 'listTemporalKeyClusterChainByClusterNodeId')
    params = dict(clusterId=cId, n=chainLimit)
    clusterChain = g.gremlin.query(script, params)

    if clusterChain != None:
        return list(clusterChain)
    else:
        return []

def _getTemporalClusterChain(cId, chainLimit):
	#script = g.scripts.get('listTemporalClusterChainByClusterNodeId')
    script = getScriptByNamespaceMethodName(g, 'ListCluster', 'listTemporalClusterChainByClusterNodeId')
    params = dict(clusterId=cId, n=chainLimit)
    clusterChain = g.gremlin.query(script, params)

    if clusterChain != None:
        return list(clusterChain)
    else:
        return []

def _getClusterStats(nodeId):
	#script = g.scripts.get('listAllBlobStatsByClusterId')
    script = getScriptByNamespaceMethodName(g, 'ListCluster', 'listAllBlobStatsByClusterId')
    params = dict(clusterNodeId=nodeId)
    clusterStats = g.gremlin.query(script, params)

    if clusterStats != None:
        return list(clusterStats)
    else:
        return []


def _getClusterObjList(exp, temporalIdx):
    print exp
    print temporalIdx
    print 'here'
    # return all blob nodes for [temporalIdx]th image sequence in exp
    #script1 = g.scripts.get('listAllBlobNodesByExpIdImgIdx')
    script1 = getScriptByNamespaceMethodName(g, 'ListCluster', 'listAllBlobNodesByExpIdImgIdx')
    params1 = dict(expId=exp, temporalRef=temporalIdx)
    #script = g.scripts.get('listClusterByNodeId')
    #params = dict(expId=2760)
    blobs = list(g.gremlin.query(script1, params1))
    
    print 'blob len: ' + str(len(blobs))
    #script2 = g.scripts.get('listClusterByNodeId')
    script2 = getScriptByNamespaceMethodName(g, 'ListCluster', 'listClusterByNodeId')

    clusters = []
    while len(blobs) > 0:
        blob = blobs.pop()
        params2 = dict(id=blob.neo4jId)
        clusterObj = g.gremlin.query(script2, params2)
        print clusterObj
        blobList = []
        if clusterObj != None:
            blobList = list(clusterObj)
        blobList.append(blob) # make a full list of clustering blobs
        #def f(x): return x not in blobList # function def
        #blobs = filter(f, blobs) # remove current blobList from the master blobs list
        clusters.append(list(blobList))
        print blobList
        print blob
        if len(blobList) > 1:
            blobList.remove(blob)
            print blobList
            blobs = removeListFromList(blobList, blobs)
        
    return clusters

def _getClusterList(exp, temporalIdx):
        print exp
        print temporalIdx
        # return all cluster nodes for [temporalIdx]th image sequence in exp
        #script1 = g.scripts.get('listAllClusterNodesByExpIdImgIdx')
        script1 = getScriptByNamespaceMethodName(g, 'ListCluster', 'listAllClusterNodesByExpIdImgIdx')
        params1 = dict(expId=exp, temporalRef=temporalIdx)
        clusters = list(g.gremlin.query(script1, params1))

        return clusters

