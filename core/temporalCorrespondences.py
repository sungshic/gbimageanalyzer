#import sys
#sys.path.insert(0, '/Users/spark/Documents/workspace/image_analysis/scripts/')
#sys.path.append('gbimageanalyzer/core')
import json
import uuid
import shapely
from shapely.geometry import Polygon
#import shapy
from shapely import affinity
from gbia import GBIAImage, GBIACluster
from CorrespondenceManager import CorrespondenceManager
'''
import geojson
#from bulbs.neo4jserver import Graph
from bulbs.rexster import Graph, Config
my_rexster_uri = 'http://localhost:8180/graphs/neo4jsample'
config = Config(my_rexster_uri)
g=Graph(config)
g.scripts.update('../neo4jGremlinQueries/ListCluster.groovy')
'''

def getIdList(objList):
    ids = []
    for obj in objList:
        ids.append(obj.neo4jId)
    return ids

def findCorrespondingClusterChainByUUID(gdb, clusterUUID, maxChainLen):
    clusterChain = gdb.doFindCorrespondingClusterChainByUUID(clusterUUID, maxChainLen)

    return clusterChain


def setClusterNodes(gdb, exp, temporalIdx_t1, maxClusteringDist):
    clusterListAt_t1 = gdb.getClusterObjList(exp, temporalIdx_t1, maxClusteringDist)
    initializeClusterNodes(gdb, clusterListAt_t1, exp, temporalIdx_t1)

def setClusterNodesByImgUUID(gdb, uuid, maxClusteringDist):
    clusterList = gdb.getClusterObjListByImgUUID(uuid, maxClusteringDist)
    initializeClusterNodesByImgUUID(gdb, clusterList, uuid)

def setCorrespondingBlobClustersByUUID(gdb, uuid): #, maxClusteringDist):
    clusterList = [] #gdb.getClusterObjListByImgUUID(uuid, maxClusteringDist)
    for cluster in clusterList:
        if len(cluster) > 0:
            idList = getIdList(cluster)
            clusterNodeT1 = gdb.doFindConsensusCluster(idList)
            print idList
            correspondingBlobs = gdb.doFindCorrespondingBlob(1, idList)
            if correspondingBlobs != None:
                idList2 = getIdList(correspondingBlobs)
                clusterNodeT2 = gdb.doFindConsensusCluster(idList2)
                if clusterNodeT1 != None and clusterNodeT2 != None:
                    print 'linking clusters: '.join([str(clusterNodeT1), str(clusterNodeT2)])
                    gdb.linkCorrespondingClusters(clusterNodeT1, clusterNodeT2)
                print correspondingBlobs

##def setClusterCorrespondences(gdb, exp, temporalIdx, maxClusteringDist):
##	clusterList = gdb.getClusterObjList(exp, temporalIdx, maxClusteringDist)
##
##	print clusterList
##	for cluster in clusterList:
##		if len(cluster) > 0:
##			idList = getIdList(cluster)
##			clusterNodeT1 = gdb.doFindConsensusCluster(idList)
##			print idList
##			correspondingBlobs = gdb.doFindCorrespondingBlob(1, idList)
##			if correspondingBlobs != None:
##				idList2 = getIdList(correspondingBlobs)
##				clusterNodeT2 = gdb.doFindConsensusCluster(idList2)
##				if clusterNodeT1 != None and clusterNodeT2 != None:
##					print 'linking clusters: '.join([str(clusterNodeT1), str(clusterNodeT2)])
##					gdb.linkCorrespondingClusters(clusterNodeT1, clusterNodeT2)
##				print correspondingBlobs
##	#end for


def setSingleCellCorrespondencesByGBIAClusters(gbiaclist1, gbiaclist2):
    print 'processing single cell correspondences...'
    cm = CorrespondenceManager.fromGBIAClusterLists(gbiaclist1, gbiaclist2)
    cm.processCorrespondences()

def setClusterCorrespondencesByUUID(gdb, imguuidt1, imguuidt2): #exp, temporalIdx):
    cnodeList1 = gdb.getClustersByImgUUID(imguuidt1) #getClusterObjListByImgUUID(imguuidt1)
    cnodeList2 = gdb.getClustersByImgUUID(imguuidt2) #getClusterObjListByImgUUID(imguuidt2)

    gbiaimg1 = GBIAImage.fromUUID(imguuidt1)
    gbiaimg2 = GBIAImage.fromUUID(imguuidt2)

    clusterList1 = [(GBIACluster.fromCNode(cnode, gbiaimg1),cnode) for cnode in cnodeList1]
    clusterList2 = [(GBIACluster.fromCNode(cnode, gbiaimg2),cnode) for cnode in cnodeList2]

    print 'processing cluster correspondences...'
    for cluster1, cnode1 in clusterList1:
        print 'cluster at t1: ' + str(cluster1.getUUID())
        for cluster2, cnode2 in clusterList2:
            #print 'cluster at t2: ' + str(cluster2)
            if cluster1.intersects(cluster2):
                overlap =cluster1.intersection(cluster2)
                if overlap:
                    gdb.linkCorrespondingClusters(cnode1, cnode2, overlap.area)
                else:
                    gdb.linkCorrespondingClusters(cnode1, cnode2, 0)
                print 'corresponding to cluster at t2: ' + str(cluster2.getUUID())
    #end for

    gbiaclusterlist1 = [gbiac for gbiac, cnode in clusterList1]
    gbiaclusterlist2 = [gbiac for gbiac, cnode in clusterList2]

    return gbiaclusterlist1, gbiaclusterlist2

def calculateClusterEnvelope(cluster):
    cluster = list(cluster) # work on a shallow copy
    blob = cluster.pop() #first blob
    clusterEnvelope = Polygon(json.loads(blob.roi)).envelope
    #minx, miny, maxx, maxy = shapy.Polygon(json.loads(blob.roi)).bounds
    #clusterEnvelope = shapy.Polygon([(minx, miny), (minx, maxy), (maxx, maxy), (maxx, miny)])
    ##clusterEnvelope = Polygon(blob.roi).envelope
    for blob in cluster:
        polygon = Polygon(json.loads(blob.roi))
        #polygon = shapy.Polygon(json.loads(blob.roi))
        ##polygon = Polygon(blob.roi)
        clusterEnvelope = clusterEnvelope.union(polygon).envelope
        # minx, miny, maxx, maxy = clusterEnvelope.union(polygon).bounds
        # clusterEnvelope = shapy.Polygon([(minx, miny), (minx, maxy), (maxx, maxy), (maxx, miny)]) #envelope
    return list(clusterEnvelope.exterior.coords), clusterEnvelope.area

def initializeClusterNodesByImgUUID(gdb, clusters, imgUUID): #expId, temporalIdx):
    clusterNodes = []
    print clusters
    for clusterIdx, cluster in enumerate(clusters):
        clustercoords, clusterarea = calculateClusterEnvelope(cluster)
        #clusterNode = gdb.createClusterNodeByImgUUID(cluster, json.dumps(clustercoords), clusterarea, imgUUID, clusterIdx)
        clusterNode = gdb.createClusterNodeByImgUUID(cluster, clustercoords, clusterarea, imgUUID, clusterIdx)
        clusterNodes.append(clusterNode)

    return clusterNodes

def initializeClusterNodes(gdb, clusters, expId, temporalIdx):
    clusterNodes = []
    for clusterIdx, cluster in enumerate(clusters):
        clusterNode = gdb.createClusterNode(cluster, expId, temporalIdx, clusterIdx)
        clusterNodes.append(clusterNode)

    return clusterNodes

