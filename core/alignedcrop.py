import sys

# jhep related libs
sys.path.append('/grabia/gbimageanalyzer/lib/scavis/python/packages')
sys.path.append('/grabia/gbimageanalyzer/lib/scavis/jehep.jar')
#sys.path.append('gbimageanalyzer/lib/scavis/lib/system/jhplot_utils.jar')
#sys.path.append('gbimageanalyzer/lib/scavis/lib/system/jhplot.jar')
#sys.path.append('gbimageanalyzer/lib/scavis/lib/system/colt.jar')
#sys.path.append('gbimageanalyzer/lib/scavis/lib/system/jconvert.jar')
#sys.path.append('gbimageanalyzer/lib/scavis/lib/system/carmetal.jar')
#sys.path.append('gbimageanalyzer/lib/scavis/lib/system/jminhep.jar')
#sys.path.append('gbimageanalyzer/lib/scavis/lib/system/sharptools.jar')
#sys.path.append('gbimageanalyzer/lib/scavis/lib/system/cambria.jar')
#sys.path.append('gbimageanalyzer/lib/scavis/lib/system/exp4j.jar')
#sys.path.append('gbimageanalyzer/lib/scavis/lib/system/jscl-math.jar')
#sys.path.append('gbimageanalyzer/lib/scavis/lib/system/JamaPack.jar')
#sys.path.append('gbimageanalyzer/lib/scavis/lib/system/jfreechart-1.0.14.jar')
#sys.path.append('gbimageanalyzer/lib/scavis/lib/freehep/freehep-graphics2d-3.5.jar')
#sys.path.append('gbimageanalyzer/lib/scavis/lib/freehep/freehep-aid-3.5.jar')
#sys.path.append('gbimageanalyzer/lib/scavis/lib/freehep/freehep-jaida-3.5.jar')
#sys.path.append('gbimageanalyzer/lib/scavis/lib/freehep/aida-3.5.jar')
#sys.path.append('gbimageanalyzer/lib/scavis/lib/freehep/freehep-io-3.5.jar')
#sys.path.append('gbimageanalyzer/lib/scavis/lib/freehep/freehep-rootio-3.5.jar')
#sys.path.append('gbimageanalyzer/lib/scavis/lib/freehep/freehep-util-3.5.jar')
#sys.path.append('gbimageanalyzer/lib/scavis/lib/freehep/freehep-graphicsio-pdf-3.5.jar')

#from jhplot import *

from ij import IJ, ImagePlus, ImageStack
from ij.plugin.filter import ParticleAnalyzer
from ij.gui import Roi
#from loci.plugins.util import BFVirtualStack
#from loci.formats import ChannelSeparator
#from ij.io import DirectoryChooser, FileSaver
#from process3d import Flood_Fill
from java.awt import Rectangle
from random import randint
import os
import math

from functools import wraps
from time import time, sleep

def timed(f):
  @wraps(f)
  def wrapper(*args, **kwds):
    start = time()
    result = f(*args, **kwds)
    elapsed = time() - start
    #print "%s took %d seconds to finish" % (f.__name__, elapsed)
    return result
  return wrapper
'''  
def squaredDiff(ip1, ip2, offsetX, offsetY):

   if ip1.getWidth() >= ip2.getWidth() - offsetX:
    cmpWidth = ip2.getWidth() - offsetX
   else:
    cmpWidth = ip1.getWidth()

   if ip1.getHeight() >= ip2.getHeight() - offsetY:
    cmpHeight = ip2.getHeight() - offsetY
   else:
    cmpHeight = ip1.getHeight()

   ipTemplateArr = ip1.getIntArray()
   ipCmpArr = ip2.getIntArray()

   #print 'dimensions (x,y): ' + str(cmpWidth) + ', ' + str(cmpHeight)
   val = 0 #initialize
   valHistory = []
   #print 'Arr dimensions (x,y): ' + str(len(ipTemplateArr)) + ', ' + str(len(ipTemplateArr[0]))
   #print ipTemplateArr
   
   for x, row in enumerate(ipTemplateArr[:cmpWidth]): #enumerate(ipTemplateArr):
     for y, pixel in enumerate(row[:cmpHeight]): #enumerate(row):
      cmpX = x + offsetX
      cmpY = y + offsetY
      #print ipCmpArr
      #print 'cmpCoords (x,y): ' + str(cmpX) + ', ' + str(cmpY)
      sqrErr = ((ipTemplateArr[x][y] - ipCmpArr[cmpX][cmpY])*256.0)**2/256.0
      val = val + sqrErr 
      #valHistory.append(sqrErr)
      #valHistory = valHistory[::-1][:10][::-1] # maintain five histories
      #valHistoAvg = sum(valHistory)/len(valHistory)
      #if valHistoAvg > 2000 or val > 10000000:
        #print 'valHistoAvg: ' + str(valHistoAvg) + ', val:' + str(val)
      #  return -1    

   return val/(cmpWidth*cmpHeight)
'''

def normalizeIp(ip):
    ipStat = ip.getStatistics()
    ipMean = ipStat.mean
    ipSD = ipStat.stdDev
    ipVar = ipStat.stdDev**2
    ipMin = ipStat.min
    ipMax = ipStat.max


    ipPixArr = ip.getFloatArray()

    ipRange = ipMax - ipMin
    if ipRange == 0:
        ipRange = 1.0

    if ipSD > 1:
        #print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>pukkuk!" + str(ipSD)
        denom = ipSD
    else:
        #print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>pukkuk!"
        denom = 1.0


    for x, row in enumerate(ipPixArr[:]):
        for y, pixel in enumerate(row[:]):
            a1 = ipPixArr[x][y]
            #print '>>>>> (' + str(ipMean) + ', ' + str(ipVar) + ')'
            #ipPixArr[x][y] = (ipPixArr[x][y] - ipMean)/denom
            normalizedPix = (ipPixArr[x][y] - ipMin)/ipRange # normalize to range 0..1
            ipPixArr[x][y] = normalizedPix
            #print '>>> (' + str(a1) + ', ' + str(ipPixArr[x][y]) + ')'

    #ip.setFloatArray(ipPixArr)
    #return ip
    return ipPixArr


def normalizeIpArr(ipPixArr, ipMean, ipVar):
    for x, row in enumerate(ipPixArr[:]):
        for y, pixel in enumerate(row[:]):
            ipPixArr[x][y] = int((ipPixArr[x][y] - ipMean)/ipVar + 0.5)

    return ipPixArr

def getCroppedIp(ip, xOff, yOff, width, height):
    ip.setRoi(xOff, yOff, width, height)
    return ip.crop()

def getRandSampleSquaredDiffs(ip1, ip2, unitN, sampN):
    ip1Width = ip1.getWidth()
    ip1Height = ip1.getHeight()
    unitWidth = int(ip1Width/unitN)
    unitHeight = int(ip1Height/unitN)

    randXLimit = ip1Width - unitWidth
    randYLimit = ip1Height - unitHeight

    #imp = ImagePlus('test', ip1)
    #imp.show()
    sampleSqrDiffs = P0D()
    #sampleSqrDiffDiffs = P0D()
    for i in range(0, sampN):
        randXOff = randint(0, randXLimit)
        randYOff = randint(0, randYLimit)
        ip1.setRoi(randXOff, randYOff, unitWidth, unitHeight)
        ip1Crop = ip1.crop()
        #if sampN == 1:
        #	imp = ImagePlus('test', ip1)
        #	imp2 = ImagePlus('test', ip2)
        #	imp.show()
        #	imp2.show()
        #	imp2.setRoi(randXOff, randYOff, unitWidth, unitHeight)
        #	#sleep(10.0)
        #	imp.getWindow().close()
        #	imp2.getWindow().close()
        sqrDiff = squaredDiff(ip1Crop, ip2, randXOff, randYOff)
        sampleSqrDiffs.add(sqrDiff)
        #imp.setRoi(randXOff, randYOff, unitWidth, unitHeight)
        #sleep(1.0)

    #imp.getWindow().close()
    return sampleSqrDiffs

def squaredDiff(ip1, ip2, offsetX, offsetY):

   if ip1.getWidth() >= ip2.getWidth() - offsetX:
    cmpWidth = ip2.getWidth() - offsetX
   else:
    cmpWidth = ip1.getWidth()

   if ip1.getHeight() >= ip2.getHeight() - offsetY:
    cmpHeight = ip2.getHeight() - offsetY
   else:
    cmpHeight = ip1.getHeight()

   ip2.setRoi(offsetX, offsetY, cmpWidth, cmpHeight)
   ip2Crop = ip2.crop()
   #ip2Crop = normalizeIp(ip2Crop)

   #ipTemplateArr = ip1.getFloatArray()
   #ipCmpArr = ip2.getIntArray()
   #ipCmpArr = ip2Crop.getFloatArray()
   ipTemplateArr = normalizeIp(ip1)
   ipCmpArr = normalizeIp(ip2Crop)
   

   #print 'dimensions (x,y): ' + str(cmpWidth) + ', ' + str(cmpHeight)
   val = 0 #initialize
   valHistory = []
   #print 'Arr dimensions (x,y): ' + str(len(ipTemplateArr)) + ', ' + str(len(ipTemplateArr[0]))
   #print ipTemplateArr
   
   for x, row in enumerate(ipTemplateArr[:cmpWidth]): #enumerate(ipTemplateArr):
     for y, pixel in enumerate(row[:cmpHeight]): #enumerate(row):
      cmpX = x 
      cmpY = y 
      #print ipCmpArr
      #print 'cmpCoords (x,y): ' + str(cmpX) + ', ' + str(cmpY)
      #print 'pix pair: ' + str(ipTemplateArr[x][y]) + ', ' + str(ipCmpArr[cmpX][cmpY])
      sqrErr = (((ipTemplateArr[x][y] - ipCmpArr[cmpX][cmpY])*255)**2)/65535.0
      val = val + sqrErr 
      #valHistory.append(sqrErr)
      #valHistory = valHistory[::-1][:10][::-1] # maintain five histories
      #valHistoAvg = sum(valHistory)/len(valHistory)
      #if valHistoAvg > 2000 or val > 10000000:
        #print 'valHistoAvg: ' + str(valHistoAvg) + ', val:' + str(val)
      #  return -1    

   return val/(cmpWidth*cmpHeight)


def imgDiff(ip1, ip2, offsetX, offsetY):

   if ip1.getWidth() >= ip2.getWidth() and ip1.getHeight() >= ip2.getHeight():
    ipTemplateArr = ip2.getIntArray()
    ipCmpArr = ip1.getIntArray()
   elif ip2.getWidth() >= ip1.getWidth() and ip2.getHeight() >= ip1.getHeight():
    ipTemplateArr = ip1.getIntArray()
    ipCmpArr = ip2.getIntArray()
   else:
    return -1

   val = 0 #initialize
   valHistory = []
   for y, row in enumerate(ipTemplateArr):
     for x, pixel in enumerate(row):
      cmpX = x + offsetX
      cmpY = y + offsetY
      val = val + ipTemplateArr[y][x] - ipCmpArr[cmpY][cmpX]
      valHistory.append(val)
      valHistory = valHistory[::-1][:5][::-1] # maintain five histories
      valHistoAvg = sum(valHistory)/len(valHistory)
      if valHistoAvg > 300:
        #print 'valHistoAvg: ' + str(valHistoAvg)
        print valHistory
        return -1    

   print val
   return val

def determineSquaredDiffConst(landmarkIp, ip1):
    ldWidth = landmarkIp.getWidth()
    ldHeight = landmarkIp.getHeight()
    imgWidth = ip1.getWidth() - ldWidth
    imgHeight = ip1.getHeight() - ldHeight

    sampleSqrDiffs = P0D()
    sampleSDs = P0D()
    for i in range(0, 200):
        randXOff = randint(0, imgWidth)
        randYOff = randint(0, imgHeight)
        #sqrDiff = squaredDiff(landmarkIp, ip1, randXOff, randYOff)
        cropIp = getCroppedIp(ip1, randXOff, randYOff, ldWidth, ldHeight)
        curSampleDiffs = getRandSampleSquaredDiffs(landmarkIp, cropIp, 4, 10) # take squared diff of 10 random 1/8 blocks
        curStat = curSampleDiffs.getStat()
        sqrDiff = curStat['mean']
        sd = curStat['standardDeviation']
        sampleSqrDiffs.add(sqrDiff)
        sampleSDs.add(sd)

    sampleStat1 = sampleSqrDiffs.getStat()
    sampleStat2 = sampleSDs.getStat()

    #cutoffConst1 = sampleStat1['harmonicMean']*0.2 #float(sampleStat1['mean']) - float(sampleStat1['variance']**0.5) # subtract mean by 1 standard deviation
    #cutoffConst2 = sampleStat2['harmonicMean']*0.2 #float(sampleStat2['mean']) - float(sampleStat2['variance']**0.5)/2 # subtract mean by 0.5 standard deviation
    cutoffConst1 = float(sampleStat1['mean']) #- float(sampleStat1['standardDeviation'])*2 # subtract mean by 2 standard deviation
    cutoffConst2 = float(sampleStat2['mean']) #- float(sampleStat2['standardDeviation'])*2 # subtract mean by 2 standard deviation
    cutoffConst3 = float(sampleStat2['mean']) #- float(sampleStat2['standardDeviation'])*2 # subtract mean by 3 standard deviation

    print 'sqrdiff const: ' + str(cutoffConst1) + ', ' + str(cutoffConst2) + ', ' + str(cutoffConst3)

    return cutoffConst1, cutoffConst2, cutoffConst3


def preprocessLandmarkImage(landmarkImp, fraction):
    # preprocess landmark image before searching
    landmarkImpReduced = landmarkImp.createImagePlus()
    ip = landmarkImp.getProcessor().duplicate()
    #IJ.run(landmarkImp, "Enhance Contrast...", "saturated=0.4 normalize")
    #ip.blurGaussian(2)
    ip = ip.resize(ip.getWidth()/fraction, ip.getHeight()/fraction, True)
    landmarkImpReduced.setProcessor("reduced landmark", ip)
    return landmarkImpReduced

def preprocessSearchImage(imp, fraction):
    searchImp = imp.createImagePlus()
    ip = imp.getProcessor().duplicate()
    #ip.blurGaussian(2)
    ip = ip.resize(ip.getWidth()/fraction, ip.getHeight()/fraction, True)
    searchImp.setProcessor("landmark search", ip)
    return searchImp


def getCroppedIp(ip1, ip2, offsetX, offsetY):

   is_ip1_cropped = False
   if ip1.getWidth() >= ip2.getWidth() - offsetX:
    cmpWidth = ip2.getWidth() - offsetX
    is_ip1_cropped = True
   else:
    cmpWidth = ip1.getWidth()

   if ip1.getHeight() >= ip2.getHeight() - offsetY:
    cmpHeight = ip2.getHeight() - offsetY
    is_ip1_cropped = True
   else:
    cmpHeight = ip1.getHeight()

   if is_ip1_cropped:
    ip1.setRoi(0,0,cmpWidth, cmpHeight)
    ip1Crop = ip1.crop()
   else:
    ip1Crop = None
   ip2.setRoi(offsetX, offsetY, cmpWidth, cmpHeight)
   ip2Crop = ip2.crop()
   #ip2Crop = normalizeIp(ip2Crop)

   #ipTemplateArr = ip1.getFloatArray()
   #ipCmpArr = ip2.getIntArray()
   #ipCmpArr = ip2Crop.getFloatArray()
#   ipTemplateArr = normalizeIp(ip1)
#   ipCmpArr = normalizeIp(ip2Crop)
   return ip1Crop, ip2Crop

# ip1 and ip2 must have equal dimensions
def calcPearsonsCorrCoeff(ip1, ip2):
    ip1_arr = ip1.getFloatArray()
    ip1_list = [l for sl in ip1_arr for l in sl] # flatten ip1_arr into a 1D list
    ip2_arr = ip2.getFloatArray()
    ip2_list = [l for sl in ip2_arr for l in sl] # flatten ip2_arr into a 1D list

    list_len = len(ip1_list)

    sumx = sum(ip1_list)
    sumy = sum(ip2_list)

    xmean = sumx/list_len
    ymean = sumy/list_len

    cov_xy = 0.0
    sig_x = sig_y = 0.0
    for idx in range(0, list_len):
        cov_xy += (ip1_list[idx] - xmean)*(ip2_list[idx] - ymean)
        sig_x += (ip1_list[idx]-xmean)**2
        sig_y += (ip2_list[idx]-ymean)**2

    return cov_xy/(math.sqrt(sig_x)*math.sqrt(sig_y))


@timed
def alignTwoImages(imp_t1, imp_t2, fraction):
    imp_t1_reduced = preprocessSearchImage(imp_t1, fraction)
    ip_t1 = imp_t1_reduced.getProcessor()
    imp_t2_reduced = preprocessSearchImage(imp_t1, fraction)
    ip_t2 = imp_t2_reduced.getProcessor()


@timed
def findLandmarkOffsetFullSearch2(landmarkImpReduced, searchImp, fraction):

    #landmarkImpReduced = preprocessLandmarkImage(landmarkImp, fraction)
    landmarkIp = landmarkImpReduced.getProcessor()
    searchImpReduced = preprocessSearchImage(searchImp, fraction)
    searchIp = searchImpReduced.getProcessor()

    lmWidth = landmarkIp.getWidth()
    lmHeight = landmarkIp.getHeight()

    imgWidth = searchIp.getWidth()
    imgHeight = searchIp.getHeight()
    xCoordStart = 0 #landmarkIp1.getWidth()
    xCoordLen = (imgWidth - landmarkIp.getWidth())
    yCoordStart = 0 #landmarkIp1.getHeight()
    yCoordLen = (imgHeight - landmarkIp.getHeight())

    #print 'Coords: ' + str(xCoordStart) + ', ' + str(xCoordLen) + ', ' + str(yCoordStart) + ', ' + str(yCoordLen)
    curXOff = 0
    curYOff = 0
    min_coeff_diff = 2.0 #sys.maxint
    #print 'maxsysint: ' + str(minSqrDiff)
    #print 'searching for landmark offset, img size: (' + str(imgWidth) + ',' + str(imgHeight) + ')'
    searchImpReduced.show()
    for xOff in range(xCoordStart, xCoordLen):
        #print 'xOff: ' + str(xOff)
        for yOff in range(yCoordStart, yCoordLen):
        #       curSqrDiff = squaredDiff(landmarkIp, searchIp, xOff, yOff)
            cropped_ip1, cropped_ip2 = getCroppedIp(landmarkIp, searchIp, xOff, yOff)
            if cropped_ip1:
                corr_coeff = calcPearsonsCorrCoeff(cropped_ip1, cropped_ip2)
            else:
                corr_coeff = calcPearsonsCorrCoeff(landmarkIp, cropped_ip2)

            cur_coeff_diff = 1.0 - corr_coeff
            #print 'curDiff: ' + str(curSqrDiff)
            #print 'yOff: ' + str(yOff) + ', cufDiff:' + str(curSqrDiff)
            if (cur_coeff_diff < min_coeff_diff and cur_coeff_diff >= 0):
                curXOff = xOff
                curYOff = yOff
                min_coeff_diff = cur_coeff_diff
                #searchImp.setRoi(xOff*fraction, yOff*fraction, lmWidth*fraction, lmHeight*fraction)
                searchImpReduced.setRoi(xOff, yOff, lmWidth, lmHeight)
                #print '(' + str(curXOff) + ',' + str(curYOff) + '), minSqrDiff: ' + str(min_coeff_diff)
                #if (minSqrDiff < 1000):
                #  return curXOff, curYOff

   
    #print '(' + str(curXOff) + ',' + str(curYOff) + '), minSqrDiff: ' + str(min_coeff_diff)
    searchImpReduced.getWindow().close()
    return curXOff*fraction, curYOff*fraction


@timed
def findLandmarkOffsetFullSearch(landmarkImpReduced, searchImp, fraction):

   #landmarkImpReduced = preprocessLandmarkImage(landmarkImp, fraction)
   landmarkIp = landmarkImpReduced.getProcessor()
   searchImpReduced = preprocessSearchImage(searchImp, fraction)
   searchIp = searchImpReduced.getProcessor()

   lmWidth = landmarkIp.getWidth()
   lmHeight = landmarkIp.getHeight()

   imgWidth = searchIp.getWidth()
   imgHeight = searchIp.getHeight()
   xCoordStart = 0 #landmarkIp1.getWidth()
   xCoordLen = (imgWidth - landmarkIp.getWidth())
   yCoordStart = 0 #landmarkIp1.getHeight()
   yCoordLen = (imgHeight - landmarkIp.getHeight())

   print 'Coords: ' + str(xCoordStart) + ', ' + str(xCoordLen) + ', ' + str(yCoordStart) + ', ' + str(yCoordLen)
   curXOff = 0
   curYOff = 0
   minSqrDiff = sys.maxint
   #print 'maxsysint: ' + str(minSqrDiff)
   print 'searching for landmark offset, img size: (' + str(imgWidth) + ',' + str(imgHeight) + ')'
   searchImpReduced.show()
   for xOff in range(xCoordStart, xCoordLen):
     #print 'xOff: ' + str(xOff)
     for yOff in range(yCoordStart, yCoordLen):
       curSqrDiff = squaredDiff(landmarkIp, searchIp, xOff, yOff)
       #print 'curDiff: ' + str(curSqrDiff)
       #print 'yOff: ' + str(yOff) + ', cufDiff:' + str(curSqrDiff)
       if (curSqrDiff < minSqrDiff and curSqrDiff >= 0):
         curXOff = xOff
         curYOff = yOff
         minSqrDiff = curSqrDiff
         #searchImp.setRoi(xOff*fraction, yOff*fraction, lmWidth*fraction, lmHeight*fraction)
         searchImpReduced.setRoi(xOff, yOff, lmWidth, lmHeight)
         print '(' + str(curXOff) + ',' + str(curYOff) + '), minSqrDiff: ' + str(minSqrDiff)
         #if (minSqrDiff < 1000):
         #  return curXOff, curYOff

   
   print '(' + str(curXOff) + ',' + str(curYOff) + '), minSqrDiff: ' + str(minSqrDiff)
   searchImpReduced.getWindow().close()
   return curXOff*fraction, curYOff*fraction


@timed
def findLandmarkOffset(landmarkImp, searchImp, fraction):

   landmarkImpReduced = preprocessLandmarkImage(landmarkImp, fraction)
   landmarkIp = landmarkImpReduced.getProcessor()
   searchImpReduced = preprocessSearchImage(searchImp, fraction)
   searchIp = searchImpReduced.getProcessor()
   
   lmWidth = landmarkIp.getWidth()
   lmHeight = landmarkIp.getHeight()

   imgWidth = searchIp.getWidth()
   imgHeight = searchIp.getHeight()
   xCoordStart = 0 #landmarkIp1.getWidth()
   xCoordLen = (imgWidth - lmWidth)
   yCoordStart = 0 #landmarkIp1.getHeight()
   yCoordLen = (imgHeight - lmHeight)

   print 'Coords: ' + str(xCoordStart) + ', ' + str(xCoordLen) + ', ' + str(yCoordStart) + ', ' + str(yCoordLen)
   curXOff = 0
   curYOff = 0

   cropIp = getCroppedIp(searchIp, 0, 0, lmWidth, lmHeight)
   curSampleDiffs = getRandSampleSquaredDiffs(landmarkIp, cropIp, 4, 50) # take squared diff of 50 random 1/8 blocks 
   curStat = curSampleDiffs.getStat()

   curSqrDiff = curStat['mean']

   minSqrDiff = curSqrDiff
   print 'minSqrDiff: ' + str(minSqrDiff)

   #print 'maxsysint: ' + str(minSqrDiff)
   print 'searching for landmark offset, img size: (' + str(imgWidth) + ',' + str(imgHeight) + ')'
   
   searchImpReduced.show()
   landmarkImp.show()
   candidateCoords = []
   cutoffConst1, cutoffConst2, cutoffConst3 = determineSquaredDiffConst(landmarkIp, searchIp)
   print 'consensus cutoff const: ' + str(cutoffConst1) + ', ' + str(cutoffConst2)
   for xOff in range(xCoordStart, xCoordLen):
     #print xOff
     for yOff in range(yCoordStart, yCoordLen):
       cropIp = getCroppedIp(searchIp, xOff, yOff, lmWidth, lmHeight)
       curSampleDiffs = getRandSampleSquaredDiffs(landmarkIp, cropIp, 4, 10)
       curStat = curSampleDiffs.getStat()
       curSqrDiff = curStat['mean']
       curSD = curStat['standardDeviation']
       #print 'curDiff: ' + str(curSqrDiff)
       #print 'x,yOff: ' + str(xOff) + ', ' + str(yOff) + ', cufDiff:' + str(curSqrDiff)
       #if (curSqrDiff < minSqrDiff and curSqrDiff >= 0): # a good default cutoff val is 0.2
       #print 'curSqrDiff: ' + str(curSqrDiff) + ', stdDev: ' + str(curSD)
       #if (curSqrDiff < cutoffConst1 and curSD < cutoffConst2 and curSqrDiff >= 0): # a good default cutoff val is 0.2
       #print 'curSqrDiff: ' + str(curSqrDiff)

       if (curSqrDiff < cutoffConst1 and curSqrDiff >= 0): # a good default cutoff val is 0.2

        #print 'curSqrDiff: ' + str(curSqrDiff) + ', stdDev: ' + str(curSD)
        #print '(' + str(xOff) + ',' + str(yOff) + '), minSqrDiff: ' + str(minSqrDiff)
            ####cropIp = getCroppedIp(searchIp, xOff, yOff, lmWidth, lmHeight)
            ####curSampleDiffs2 = getRandSampleSquaredDiffs(landmarkIp, cropIp, 1, 10)
            ####curStat = curSampleDiffs2.getStat()

            ####curSqrDiff2 = curStat['mean']
            ####curSD2 = curStat['standardDeviation']
        diffRatio = curSqrDiff/minSqrDiff

        if diffRatio <= 1.2: # if the difference is not by more than 20%
            curSqrDiff2 = squaredDiff(landmarkIp, searchIp, xOff, yOff) # do a thourough diff calc

            if (curSqrDiff2 < minSqrDiff):
                searchImpReduced.setRoi(xOff, yOff, lmWidth, lmHeight)
                print curStat
                print 'offset candidate: (' + str(xOff) + ',' + str(yOff) + '), landmark avg curSqrDiff2: ' + str(curSqrDiff2)
                minSqrDiff = curSqrDiff2
                curXOff = xOff
                curYOff = yOff

        #curSqrDiffDiff = abs(curSqrDiff2 - curSqrDiff)
        #curSDDiff = abs(curSD2 - curSD)

    ###	print '(' + str(xOff) + ',' + str(yOff) + '), curSqrDiffDiff: ' + str(curSqrDiffDiff) + ', curSqrDiff1,2: ' + str(curSqrDiff) + ', ' + str(curSqrDiff2)
    ###	print '(' + str(xOff) + ',' + str(yOff) + '), curSDDiff: ' + str(curSDDiff) + ', curSD: ' + str(curSD)
        #if (curSqrDiffDiff < curSqrDiff*0.05 and curSDDiff < curSD*0.05 ):  #minSqrDiffDiff):

        ###if (curSqrDiff2 < cutoffConst3):  #minSqrDiffDiff):
        ###	print 'offset found early: (' + str(xOff) + ',' + str(yOff) + '), landmark avg curSqrDiff: ' + str(curSqrDiff)
        ###	curXOff = xOff
        ###	curYOff = yOff
        ###	searchImp.getWindow().close()
        ###	return curXOff*fraction, curYOff*fraction


   print '(' + str(curXOff) + ',' + str(curYOff) + '), minSqrDiff: ' + str(minSqrDiff)
   searchImpReduced.getWindow().close()
   return curXOff*fraction, curYOff*fraction
         
'''	
@timed
def findLandmarkOffset(landmarkIp1, landmarkIp2, imp1, fraction):
   
   landmarkIp1 = landmarkIp1.resize(landmarkIp1.getWidth()/fraction, landmarkIp1.getHeight()/fraction, True)
   landmarkIp2 = landmarkIp2.resize(landmarkIp2.getWidth()/fraction, landmarkIp2.getHeight()/fraction, True)
   ip1 = imp1.getProcessor()
   ip1.blurGaussian(2)
   ip1 = ip1.resize(ip1.getWidth()/fraction, ip1.getHeight()/fraction, True)

   l1Width = landmarkIp1.getWidth()
   l1Height = landmarkIp1.getHeight()
   l2Width = landmarkIp2.getWidth()
   l2Height = landmarkIp2.getHeight()

   landmarkRatio = (l2Width*l2Height)/(l1Width*l1Height)

   imgWidth = ip1.getWidth()
   imgHeight = ip1.getHeight()
   xCoordStart = 0 #landmarkIp1.getWidth()
   xCoordLen = (imgWidth - landmarkIp1.getWidth())
   yCoordStart = 0 #landmarkIp1.getHeight()
   yCoordLen = (imgHeight - landmarkIp1.getHeight())

   print 'Coords: ' + str(xCoordStart) + ', ' + str(xCoordLen) + ', ' + str(yCoordStart) + ', ' + str(yCoordLen)
   curXOff = 0
   curYOff = 0
   curSqrDiff = squaredDiff(landmarkIp1, ip1, 0, 0)
   curSqrDiff2 = squaredDiff(landmarkIp2, ip1, 0, 0)
   minSqrDiff = curSqrDiff
   minSqrDiffDiff = abs(curSqrDiff2 - curSqrDiff)
   #print 'maxsysint: ' + str(minSqrDiff)
   print 'searching for landmark offset, img size: (' + str(imgWidth) + ',' + str(imgHeight) + ')'
   
   imp1.show()
   candidateCoords = []
   cutoffConst1, cutoffConst2, cutoffConst3 = determineSquaredDiffConst(landmarkIp1, landmarkIp2, ip1)
   print 'consensus cutoff consts: ' + str(cutoffConst1) + ', ' + str(cutoffConst2)
   for xOff in range(xCoordStart, xCoordLen):
     #print xOff
     for yOff in range(yCoordStart, yCoordLen):
       curSqrDiff = squaredDiff(landmarkIp1, ip1, xOff, yOff)
       #print 'curDiff: ' + str(curSqrDiff)
       #print 'x,yOff: ' + str(xOff) + ', ' + str(yOff) + ', cufDiff:' + str(curSqrDiff)
       #if (curSqrDiff < minSqrDiff and curSqrDiff >= 0):
       if (curSqrDiff < cutoffConst1 and curSqrDiff >= 0): # a good default cutoff val is 0.2
        #print '(' + str(curXOff) + ',' + str(curYOff) + '), minSqrDiff: ' + str(minSqrDiff)
        #minSqrDiff = curSqrDiff
        #if (curSqrDiff): # < 1e-6):
        #candidateCoords.append((curXOff, curYOff))
        curSqrDiff2 = squaredDiff(landmarkIp2, ip1, xOff, yOff)
        curSqrDiffDiff = abs(curSqrDiff2 - curSqrDiff)
        print 'offset candidate: (' + str(xOff) + ',' + str(yOff) + '), landmark2 curSqrDiff: ' + str(curSqrDiff2)
        imp1.setRoi(xOff*fraction, yOff*fraction, l2Width*fraction, l2Height*fraction)
        if curSqrDiffDiff < minSqrDiffDiff: #abs(curSqrDiff2 - curSqrDiff) < 1e-6:
            print 'offset candidate: (' + str(xOff) + ',' + str(yOff) + '), landmark1 curSqrDiff: ' + str(curSqrDiff)
            minSqrDiffDiff = curSqrDiffDiff
            curXOff = xOff
            curYOff = yOff
            if curSqrDiff2 < cutoffConst2 or minSqrDiffDiff < cutoffConst2:
                imp1.getWindow().close()
                return curXOff*fraction, curYOff*fraction


   print '(' + str(curXOff) + ',' + str(curYOff) + '), minSqrDiffDiff: ' + str(minSqrDiffDiff)
   imp1.getWindow().close()
   return curXOff*fraction, curYOff*fraction
'''      
   
""" temporary comment block
#srcDir = "/Users/spark/Documents/workspace/image_analysis/data/220413_BSU_agarose_40um_Pos1/BF/bf/"
#srcDir = "/Users/spark/Documents/workspace/image_analysis/data/220413_BSU_agarose_40um_Pos1/BF/bf/"
srcDir = "/Users/spark/Documents/workspace/image_analysis/data/090413_Ecoli_rrnBP1_gfp_Pos1/"
refImageFile = srcDir + "image004_landmark.tif"
#srcDir = "/Users/spark/Downloads/test/"
fluoDir = "/Users/spark/Documents/workspace/image_analysis/data/220413_BSU_agarose_40um_Pos1/RFP/"
#destDir= "/Users/spark/Documents/workspace/image_analysis/data/220413_BSU_agarose_40um_Pos1/BF/cropped/"
#fluodestDir= "/Users/spark/Documents/workspace/image_analysis/data/220413_BSU_agarose_40um_Pos2/RFP/cropped/"
print '#############DIRECTORY: ' + srcDir

roiMs = []
idx = -1
imgArr = []
roi_ip = []
cropRect = Rectangle(361, 0, 43, 468) 
landmarkImg = IJ.openImage(srcDir + 'image001_landmark.tif')
print landmarkImg
landmarkImp = landmarkImg.createImagePlus()
print landmarkImp
landmarkIp = landmarkImg.getProcessor().duplicate()
#landmarkImp.setProcessor("landmark",landmarkIp)
#IJ.run(landmarkImp, "Convolve...", "text1=[0 0 3 2 2 2 3 0 0\n0 2 3 5 5 5 3 2 0\n3 3 5 3 0 3 5 3 3\n2 5 3 -12 -23 -12 3 5 2\n 2 5 0 -23 -40 -23 0 5 2\n2 5 3 -12 -23 -12 3 5 2\n3 3 5 3 0 3 5 3 3\n0 2 3 5 5 5 3 2 0\n0 0 3 2 2 2 3 0 0\n]")
    
landmarkImg.show()
print landmarkIp


for filename in os.listdir(srcDir):
  if not filename in 'image001_landmark.tif' and filename.endswith(".tif"):
    img = IJ.openImage(srcDir + filename)
    imp = img.createImagePlus()
    ip = img.getProcessor().duplicate()
    xOff, yOff = findLandmarkOffset(landmarkIp, ip)
    print 'best landmark offset: ' + str(xOff) + ', ' + str(yOff)
    #ip.setRoi(cropRect.getX()+xOff, cropRect.getY()+yOff, cropRect.getWidth(), cropRect.getHeight())
    #ip = ip.crop()
    #imp.setProcessor("test",ip)
  else:
    continue  
  print '####### filename: ' + filename   
"""


    

      
