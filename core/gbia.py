__author__='spark'
import sys
import dographdb as gdb
from shapely.geometry import Polygon, LineString
from shapely.affinity import translate, scale
##from sympy.geometry import Polygon, Line, Segment, Point
##import shapy
import json
import logging

class GBIANode():
    _uuid = None

    def getUUID(self):
        return self._uuid

class GBIAGeoNode(GBIANode):
    IS_NOT_ALIGNED = -1
    IS_ALIGNED = 0

    _polygon = None
    __transpolygon = None # _polygon with its centroid translated to (0,0)
    _alignstatus = IS_NOT_ALIGNED
    logger = logging.getLogger('grabia_app.gbimageanalyzer.core.gbia.GBIAGeoNode')

    def __init__(self, polygon):
        self._polygon = polygon
        self._translateCentroidToZeros()

    @classmethod
    def fromCoords(cls, coords):
        return cls(Polygon(coords))
        #return cls(Polygon(*coords)) # sympy method



    def intersects(self, gbiacluster2, istrans=False):
        if istrans:
            return self.__transpolygon.intersects(gbiacluster2._polygon)
        else:
            return self._polygon.intersects(gbiacluster2._polygon)
    # def intersects(self, gbiacluster2, istrans=False):
    #     if istrans:
    #         plist = self.__transpolygon.intersection(gbiacluster2._polygon)
    #     else:
    #         plist = self._polygon.intersection(gbiacluster2._polygon)
    #     return len(plist) > 0

    def intersection(self, gbiacluster2, istrans=False):
        if istrans:
            return self.__transpolygon.intersection(gbiacluster2.getPolygon())
        else:
            return self._polygon.intersection(gbiacluster2.getPolygon())

    # def intersection(self, gbiacluster2, istrans=False):
    #     if istrans:
    #         plist = self.__transpolygon.intersection(gbiacluster2.getPolygon())
    #     else:
    #         plist = self._polygon.intersection(gbiacluster2.getPolygon())
    #     skimmed_plist = [p for p in plist if p is Point]
    #     if skimmed_plist:
    #         return Polygon(*skimmed_plist)
    #     else:
    #         return None

    # def getPolygon(self, istrans=False):
    #     if istrans:
    #         return self.__transpolygon
    #     else:
    #         return self._polygon

    def getCoordsList(self, istrans=False):
        if istrans:
            return list(self.__transpolygon.exterior.coords)
        else:
            return list(self._polygon.exterior.coords)

    # def getCoordsList(self, istrans=False):
    #     if istrans:
    #         return list(self.__transpolygon.args) # list of points
    #     else:
    #         return list(self._polygon.args) # list of points

    # returns (minx, miny, maxx, maxy)
    def getBounds(self, istrans=False):
        if istrans:
            if self.__transpolygon:
                return self.__transpolygon.bounds
            else:
                return None
        else: # _polygon
            if self._polygon:
                return self._polygon.bounds
            else:
                return None

        return self._polygon.bounds

    # # returns (minx, miny, maxx, maxy)
    # def getBounds(self, istrans=False):
    #     if istrans:
    #         if self.__transpolygon:
    #             return tuple(shapy.Polygon([p.args for p in self.__transpolygon.args]).bounds)
    #         else:
    #             return None
    #     else: # _polygon
    #         if self._polygon:
    #             return tuple(shapy.Polygon([p.args for p in self._polygon.args]).bounds)
    #         else:
    #             return None

    def getCentroid(self, istrans=False):
        if istrans:
            return self.__transpolygon.centroid
        else:
            return self._polygon.centroid

    def getArea(self, istrans=False):
        if istrans:
            return self.__transpolygon.area
        else:
            return self._polygon.area

    def getPolygon(self, istrans=False):
        if istrans:
            return self.__transpolygon
        else:
            return self._polygon

    def getDistance(self, othernode, istrans=False):
        return self.getPolygon(istrans).distance(othernode.getPolygon(istrans))

    # returns a new instance of GBIAGeoNode with the current and the other nodes' polygons fused
    def fuseGeoNode(self, othernode): #, connected=True):
        #if connected:
        #	dist = self.getDistance(othernode)
        geonode = GBIAGeoNode(self._polygon.union(othernode.getPolygon()))
        return geonode

    # returns a new instance of GBIAGeoNode with the current and the other nodes' polygons fused


    def _shiftTransPolygon(self, xshift, yshift):
        self.__transpolygon = translate(self.__transpolygon, xshift, yshift)

    # def _shiftTransPolygon(self, xshift, yshift):
    #     self.__transpolygon = self.__transpolygon.translate(xshift, yshift)

    def _translateCentroidToZeros(self):
        centcoord = self.getCentroid()
        self.__transpolygon = translate(self._polygon, -(centcoord.x), -(centcoord.y))
        self._alignstatus = GBIAGeoNode.IS_ALIGNED

    # def _translateCentroidToZeros(self):
    #     centcoord = self.getCentroid()
    #     self.__transpolygon = self._polygon.translate(-(centcoord.x), -(centcoord.y))
    #     self._alignstatus = GBIAGeoNode.IS_ALIGNED

    def scaleAlignToRefBounds(self, refbounds):
        self._translateCentroidToZeros() # this refreshes the __transpolygon from _polygon
        bounds = self.getBounds(istrans=True)
        width = bounds[2] - bounds[0]
        height = bounds[3] - bounds[1]

        refwidth = refbounds[2] - refbounds[0]
        refheight = refbounds[3] - refbounds[1]

        xfactor = float(refwidth)/width
        yfactor = float(refheight)/height
        self.__transpolygon = scale(self.__transpolygon, xfact=xfactor, yfact=yfactor)

        scaledbounds = self.getBounds(istrans=True)
        xshift = refbounds[0] - scaledbounds[0]
        yshift = refbounds[1] - scaledbounds[1]
        self._shiftTransPolygon(xshift, yshift) # this aligns the sampling coords

    # def scaleAlignToRefBounds(self, refbounds):
    #     self._translateCentroidToZeros() # this refreshes the __transpolygon from _polygon
    #     bounds = self.getBounds(istrans=True)
    #     width = bounds[2] - bounds[0]
    #     height = bounds[3] - bounds[1]
    #
    #     refwidth = refbounds[2] - refbounds[0]
    #     refheight = refbounds[3] - refbounds[1]
    #
    #     xfactor = float(refwidth)/width
    #     yfactor = float(refheight)/height
    #     self.__transpolygon = self.__transpolygon.scale(xfactor, yfactor)
    #
    #     scaledbounds = self.getBounds(istrans=True)
    #     xshift = refbounds[0] - scaledbounds[0]
    #     yshift = refbounds[1] - scaledbounds[1]
    #     self._shiftTransPolygon(xshift, yshift) # this aligns the sampling coords

    # height >= skip >= 0
    def sampleLinesAlongYAxis(self, skip=0):
        minx,miny,maxx,maxy = self.getBounds(istrans=True) # get the bounds for __transpolygon
        samplelines = []

        height = maxy - miny
        samplesize = int(round(height/(skip+1)))
        #print 'sampling lines...'
        for ycoord in [idx+miny for idx in range(0, samplesize)]:
            sampleline = self.__transpolygon.intersection(LineString([(minx,ycoord), (maxx, ycoord)]))
            #print sampleline
            samplelines.append(sampleline)

        return samplelines

    # # height >= skip >= 0
    # def sampleLinesAlongYAxis(self, skip=0):
    #     minx,miny,maxx,maxy = self.getBounds(istrans=True) # get the bounds for __transpolygon
    #     samplelines = []
    #
    #     height = maxy - miny
    #     samplesize = int(round(height/(skip+1)))
    #     #print 'sampling lines...'
    #     for ycoord in [idx+miny for idx in range(0, samplesize)]:
    #         sampleline = [seg for seg in self.__transpolygon.intersection(Segment((minx,ycoord), (maxx, ycoord))) if seg.__class__ is Segment]
    #         #print sampleline
    #         samplelines.append(sampleline) #add to the list even if sampleline is []
    #             # if len(sampleline) > 1:
    #             #     self.logger.debug('sampleline is in multiple segments.')
    #             #     # current cheap strategy is to use the first segment and discard others
    #             #     # this needs to be refactored to work with conjoined multiple segments
    #
    #     return samplelines


    # width >= skip >= 0
    def sampleLinesAlongXAxis(self, skip=0):
        minx,miny,maxx,maxy = self.getBounds(istrans=True) # get the bounds for __transpolygon
        samplelines = []

        width = maxx - minx
        samplesize = int(round(width/(skip+1)))
        for xcoord in [idx+minx for idx in range(0, samplesize)]:
            sampleline = self.__transpolygon.intersection(LineString([(xcoord,miny), (xcoord, maxy)]))
            samplelines.append(sampleline)

        return samplelines

    # # width >= skip >= 0
    # def sampleLinesAlongXAxis(self, skip=0):
    #     minx,miny,maxx,maxy = self.getBounds(istrans=True) # get the bounds for __transpolygon
    #     samplelines = []
    #
    #     width = maxx - minx
    #     samplesize = int(round(width/(skip+1)))
    #     for xcoord in [idx+minx for idx in range(0, samplesize)]:
    #         sampleline = [seg for seg in self.__transpolygon.intersection(Segment((xcoord,miny), (xcoord, maxy))) if seg.__class__ is Segment]
    #         samplelines.append(sampleline) #add to the list even if sampleline is []
    #               # if len(sampleline) > 1:
    #             #     logger.debug('sampleline is in multiple segments.')
    #             #     # current cheap strategy is to use the first segment and discard others
    #             #     # this needs to be refactored to work with conjoined multiple segments
    #
    #     return samplelines

    def _calcSkipFromLinecount(self, linecount, isyaxis=True):
        minx,miny,maxx,maxy = self.getBounds(istrans=True)

        if isyaxis:
            height = maxy - miny
            skip = height/linecount - 1
        else: # x-axis
            width = maxx - minx
            skip = width/linecount - 1

        return skip




    def _calcSampleMeanSquaredError(self, samplelines1, samplelines2):
        len1 = len(samplelines1)
        len2 = len(samplelines2)

        #mse = -1
        mse = float("inf")
        if len1 == len2:
            totsquared = 0
            #print 'number of sample lines to compare: ' + str(len1)
            sys.stdout.write('.')
            for i in range(0, len1):
                line1 = samplelines1[i]
                line2 = samplelines2[i]
                overlaplength = line1.intersection(line2).length
                error = (line1.length - overlaplength) + (line2.length - overlaplength)
                totsquared += error**2

            mse = float(totsquared)/len1
        else:
            raise ValueError('aligned GBIAGeoNodes have different sample numbers for MSE calculation')

        return mse


    # def _diffSegmentLists(self, seglist1, seglist2):
    #     error = 0
    #
    #     if seglist1 and seglist2:
    #         for seg1 in seglist1:
    #             for seg2 in seglist2:
    #                 intersected_seglist = seg1.intersection(seg2)
    #                 for iseg in intersected_seglist:
    #                     if iseg.__class__ is Segment:
    #                         error += seg1.length + seg2.length - (iseg.length)*2
    #     elif not seglist1:
    #         for seg2 in seglist2:
    #             error += seg2.length
    #     elif not seglist2:
    #         for seg1 in seglist1:
    #             error += seg1.length
    #
    #     return error
    #
    #
    # def _calcSampleMeanSquaredError(self, samplelines1, samplelines2):
    #     len1 = len(samplelines1)
    #     len2 = len(samplelines2)
    #
    #     #mse = -1
    #     mse = float("inf")
    #     if len1 == len2:
    #         totsquared = 0
    #         #print 'number of sample lines to compare: ' + str(len1)
    #         #sys.stdout.write('.')
    #         for i in range(0, len1):
    #             line1 = samplelines1[i]
    #             line2 = samplelines2[i]
    #             # seglist = line1.intersection(line2)
    #             # overlaplength = 0
    #             # if seglist:
    #             #     overlaplength = seglist[0].length
    #             # error = (line1.length - overlaplength) + (line2.length - overlaplength)
    #             error = self._diffSegmentLists(line1, line2)
    #             totsquared += error**2
    #
    #         mse = float(totsquared)/len1
    #     else:
    #         raise ValueError('aligned GBIAGeoNodes have different sample numbers for MSE calculation')
    #
    #     return mse


    # isyaxis=False for lines along x-axis, and isyaxis=True for lines along y-axis
    def calcMeanSquaredErrorAgainstLines(self, otherlines, skip=None, isyaxis=True):
        if skip == None:
            skip = self._calcSkipFromLinecount(len(otherlines), isyaxis)
        if isyaxis:
            lines = self.sampleLinesAlongYAxis(skip)
        else: # x-axis
            lines = self.sampleLinesAlongXAxis(skip)

        if lines and otherlines:
            mse = self._calcSampleMeanSquaredError(lines, otherlines)
        else:
            self.logger.debug('calcMeaSquaredErrorAgainstLines(): empty line list ' + str(lines) + ' ' + str(otherlines))
            mse = float("inf")

        return mse

    def calcGrowthRateAgainstGeoNodeList(self, geonode_list):
        t2_area = 0
        for geonode in geonode_list:
            t2_area += geonode.getArea()

        t1_area = self.getArea()

        return (t2_area - t1_area)/t1_area

    def calcGrowthRateAgainstGeoNode(self, geonode):
        t2_area = geonode.getArea()
        t1_area = self.getArea()

        return (t2_area - t1_area)/t1_area

    def calcMeanSquaredErrorAgainstGeoNode(self, geonode, skip=0, isyaxis=True, realign=False):
        if realign:
            self.scaleAlignToRefBounds(geonode.getBounds(istrans=True))
        else:
            if self._alignstatus == GBIAGeoNode.IS_NOT_ALIGNED:
                self.scaleAlignToRefBounds(geonode.getBounds(istrans=True))
        if isyaxis:
            otherlines = geonode.sampleLinesAlongYAxis(skip)
        else: # x-axis
            otherlines = geonode.sampleLinesAlongXAxis(skip)

        mse = self.calcMeanSquaredErrorAgainstLines(otherlines, skip, isyaxis)

        return mse

    def calcMeanSquaredErrorAgainstGeoNodeList(self, geonode_list, skip=0, isyaxis=True, realign=False):

        # calculate the combined geonode bounds, stacked up in the y-axis direction
        # the reason for defaulting to sampling horizontal lines in the Y-axis direction
        # is to minimize the risk of sampling disjoint lines in non-convex-hull (or concave) polygons
        # concave curves happen quite frequently with bacterial cells if sampled length-wise
        for idx, geonode in enumerate(geonode_list):
            if idx == 0:
                xmin_base,ymin_base,xmax_base,ymax_base = geonode.getBounds(istrans=True)
                cur_width = xmax_base - xmin_base
                cur_height = ymax_base - ymin_base
                base_height = cur_height
            else:
                xmin, ymin, xmax, ymax = geonode.getBounds(istrans=True)
                if xmin < xmin_base:
                    xmin_base = xmin
                if xmax > xmax_base:
                    xmax_base = xmax
                cur_height = ymax - ymin
                base_height += cur_height # now stacking up in the y-axis direction

        stacked_bounds = (xmin_base, ymin_base, xmax_base, ymin_base+base_height)

        if realign:
            self.scaleAlignToRefBounds(stacked_bounds) #geonode.getBounds(istrans=True))
        else:
            if self._alignstatus == GBIAGeoNode.IS_NOT_ALIGNED:
                self.scaleAlignToRefBounds(stacked_bounds) #geonode.getBounds(istrans=True))
        if isyaxis:
            otherlines = []
            for idx, geonode in enumerate(reversed(geonode_list)):
                otherlines += geonode.sampleLinesAlongYAxis(skip)
        else: # x-axis (this option is not to be used till non-convex hull issues is resolved)
            otherlines = []
            for idx, geonode in enumerate(reversed(geonode_list)):
                otherlines += geonode.sampleLinesAlongXAxis(skip)

        mse = self.calcMeanSquaredErrorAgainstLines(otherlines, skip, isyaxis)

        return mse

    def calcAdjustedMeanSquaredErrorAgainstGeoNodeList(self, geonode_list, expgrowthrate, skip=0, isyaxis=True, realign=False, isadjusted=False):
        mse = self.calcMeanSquaredErrorAgainstGeoNodeList(geonode_list, skip, isyaxis, realign)

        if isadjusted:
            curgrowthrate = self.calcGrowthRateAgainstGeoNodeList(geonode_list)
            growthrate_diff_squared = 2*(1+abs(expgrowthrate - curgrowthrate))**2
            adjusted_mse = mse + growthrate_diff_squared
            mse = adjusted_mse

        return mse

    def calcAdjustedMeanSquaredErrorAgainstGeoNode(self, geonode, expgrowthrate, skip=0, isyaxis=True, realign=False, isadjusted=False):
        # if realign:
        #     self.scaleAlignToRefBounds(geonode.getBounds(istrans=True))
        # else:
        #     if self._alignstatus == GBIAGeoNode.IS_NOT_ALIGNED:
        #         self.scaleAlignToRefBounds(geonode.getBounds(istrans=True))
        # if isyaxis:
        #     otherlines = geonode.sampleLinesAlongYAxis(skip)
        # else: # x-axis
        #     otherlines = geonode.sampleLinesAlongXAxis(skip)
        #
        # mse = self.calcMeanSquaredErrorAgainstLines(otherlines, skip, isyaxis)
        mse = self.calcMeanSquaredErrorAgainstGeoNode(geonode, skip, isyaxis, realign)

        if isadjusted:
            curgrowthrate = self.calcGrowthRateAgainstGeoNode(geonode)
            growthrate_diff_squared = 2*(1+abs(expgrowthrate - curgrowthrate))**2
            adjusted_mse = mse + growthrate_diff_squared
            mse = adjusted_mse

        return mse



class GBIAImage(GBIAGeoNode):
    __timestamp = None
    __filepath = None
    __height = None
    __width = None
    __landmarkfilepath = None
    __metadatafilepath = None
    __contenttype = None
    __landmarkoffsetx = None
    __landmarkoffsety = None

    def __init__(self, imgnode):
        try:
            self.__timestampe = int(imgnode.timestamp)
            self.__filepath = imgnode.filepath
            self.__height = int(imgnode.height)
            self.__width = int(imgnode.width)
            self.__landmarkfilepath = imgnode.landmarkRefFilepath
            self.__metadatafilepath = imgnode.metadataFilepath
            self.__contenttype = imgnode.contentType
            self.__landmarkoffsetx = imgnode.landmarkOffsetX
            self.__landmarkoffsety = imgnode.landmarkOffsetY

            p1,p2,p3,p4 = ((0,0), (0,self.__height), (self.__width, self.__height), (self.__width, 0))
            #self._polygon = Polygon([(0,0), (0,self.__height), (self.__width, self.__height), (self.__width, 0)])
            self._polygon = Polygon([p1, p2, p3, p4])
            self._boundary = [LineString([p1,p2]),LineString([p1,p2]),LineString([p3,p4]),LineString([p4,p1])]
            #[Segment(p1, p2), Segment(p2,p3), Segment(p3,p4), Segment(p4,p1)] # sympy boundary def
            self._uuid = imgnode.uuid
        except Exception as inst:
            print type(inst)
            print inst.args
            print inst
            raise # re-raise Exception

    @classmethod
    def fromUUID(cls, imguuid):
        return cls(gdb.getNodeByUUID(imguuid))

    def isBoundaryCondition(self, polygon):
        # return True if polygon touches the boundary, False otherwise
        return self._polygon.boundary.intersects(polygon)

    # def isBoundaryCondition(self, polygon):
    #     # return True if polygon touches the boundary, False otherwise
    #     for seg in self._boundary:
    #         if polygon.intersection(seg):
    #             return True
    #     return False


class GBIACluster(GBIAGeoNode):
    __gbiaimg = None
    __size = None
    __blobcount = None

    def __init__(self, size, polygon, uuid, blobcount, gbiaimg):
        try:
            self.__gbiaimg = gbiaimg
            self.__size = size
            self.__blobcount = blobcount
            self._polygon = polygon
            self._uuid = uuid
        except Exception as inst:
            print type(inst)
            print inst.args
            print inst
            raise # re-raise Exception

    @classmethod
    def fromCNodeUUID(cls, cnodeuuid, gbiaimg=None, isaligned=True):
        try:
            cnode = gdb.getNodeByUUID(cnodeuuid)
            return cls.fromCNode(cnode, gbiaimg, isaligned)
        except Exception as inst:
            print type(inst)
            print inst.args
            print inst
            raise # re-raise Exception

    @classmethod
    def fromCNode(cls, cnode, gbiaimg=None, isaligned=True):
        try:
            arg_gbiaimg = None
            if gbiaimg:
                arg_gbiaimg = gbiaimg
            else:
                imguuid = cnode.imageRefUUID
                arg_gbiaimg = GBIAImage(gdb.getNodeByUUID(imguuid))

            arg_size = int(cnode.size)
            arg_polygon = Polygon(json.loads(cnode.roi))
            #arg_polygon = Polygon(*(json.loads(cnode.roi))) # sympy method
            #arg_polygon = Polygon(cnode.roi)
            if isaligned:
                arg_polygon = translate(arg_polygon, -(int(cnode.landmarkOffsetX)), -(int(cnode.landmarkOffsetY)))
                #arg_polygon = arg_polygon.translate(-(int(cnode.landmarkOffsetX)), -(int(cnode.landmarkOffsetY)))
            arg_uuid = cnode.uuid
            arg_blobcount = int(cnode.blobcount)
            return cls(arg_size, arg_polygon, arg_uuid, arg_blobcount, arg_gbiaimg)
        except Exception as inst:
            print type(inst)
            print inst.args
            print inst
            raise # re-raise Exception

    def getGBIAImage(self):
        return self.__gbiaimg




class GBIABlob(GBIAGeoNode):
    __gbiaimg = None
    __gbiacluster = None
    __isboundary = None
    __bnode = None


    def __init__(self, bnode, gbiacluster=None, gbiaimg=None):
        try:
            self._uuid = bnode.uuid
            self._polygon = Polygon(json.loads(bnode.roi))
            #self._polygon = Polygon(*(json.loads(bnode.roi)))
            #self._polygon = Polygon(bnode.roi)
            self._translateCentroidToZeros()

            self.__bnode = bnode
            if gbiacluster:
                self.__gbiacluster = gbiacluster
            else:
                self.__gbiacluster = GBIACluster(gdb.getClusterByBlobUUID(bnode.uuid, gbiaimg))

            if gbiaimg:
                self.__gbiaimg = gbiaimg
            else:
                self.__gbiaimg = self.__gbiacluster.getGBIAImage()

            self.__isboundary = self.__gbiaimg.isBoundaryCondition(self._polygon)

        except Exception as inst:
            print type(inst)
            print inst.args
            print inst
            raise # re-raise Exception

    def getGBIACluster(self):
        return self.__gbiacluster

    def isBoundary(self):
        return self.__isboundary

    def getGraphNode(self):
        return self.__bnode


class GBIASingleCellStats():
    _generations = None
    _cuuids_with_stats = None
    _mother_lt = None
    _lineage_lt = None
    _filtered_gens = None
    _stats_lt = None

    def __init__(self, gen_limit=0):
        try:
            self._lineage_lt = {} # initialize
            self._mother_lt = {} # initialize
            self._stats_lt = {} # initialize

            self._generations = []
            self._filtered_gens = [] # filtered generations, or a generations list formed out of cells with statistics
            gen0 = gdb.getMotherGenerationCells()
            self._generations.append(gen0)

            for lineage_list in gen0:
                cuuid_list = [c['uuid'] for c in lineage_list]
                mother_cell_uuid = lineage_list[0]['uuid']
                self.initLineageLTMother(mother_cell_uuid)
                self.buildLookupTables(cuuid_list, mother_cell_uuid, 0)

            # obtain a list of uuids of cells with statistics
            self._cuuids_with_stats = [v.uuid for v in gdb.getCellsWithStats()]

            mcells = gen0
            gen_no = 1
            while len(mcells) > 0:
                nth_gen_clist = self.getNthGenCellList(mcells)
                nth_gen_clist_clipped = []
                for lineage_list in nth_gen_clist: # for each cell group belonging to the same lineage and generation
                    cuuid_list = [c['uuid'] for c in lineage_list[1:]]  # get corresponding uuids
                    mother_cell_uuid = self._mother_lt[lineage_list[0]['uuid']] # retrieve the mother cell of the lineage
                    self.buildLookupTables(cuuid_list, mother_cell_uuid, gen_no) # build a lookup table, without the first cell
                    nth_gen_clist_clipped.append(lineage_list[1:]) # remove the first cell, belonging to the previous generation of cells
                if len(nth_gen_clist_clipped) > 0:
                    self._generations.append(nth_gen_clist_clipped)
                mcells = nth_gen_clist_clipped
                gen_no += 1 # increment generation no

            # initialize filtered generation
            self.filterGenerationsList()
            # build a lookup table for stats
            self.buildStatsLookupTable()

        except Exception as inst:
            print type(inst)
            print inst.args
            print inst
            raise # re-raise Exception

    def initLineageLTMother(self, mcell_uuid):
        self._lineage_lt[mcell_uuid] = {}

    def buildLookupTables(self, cuuid_list, mcell_uuid, gen_no):
        if mcell_uuid not in self._lineage_lt:
            print 'buildMotherLT 1: this should not have happened.'
        elif str(gen_no) in self._lineage_lt[mcell_uuid]:
            #print 'buildMotherLT 2: this should not have happened.'
            self._lineage_lt[mcell_uuid][str(gen_no)].append(cuuid_list)
        else:
            # build lineage lookup table based on the current mother cell and a generation of cells in the same lineage
            self._lineage_lt[mcell_uuid][str(gen_no)] = []
            self._lineage_lt[mcell_uuid][str(gen_no)].append(cuuid_list)
        # have each constituent point to the mother cell
        for cuuid in cuuid_list:
            self._mother_lt[cuuid] = mcell_uuid

    def buildStatsLookupTable(self):
        for cur_gen in self._filtered_gens:
            cur_gen_stat_vertices = gdb.getCellStats(cur_gen)
            for cur_v in cur_gen_stat_vertices:
                cuuid = list(cur_v.inV('blob_has_stats'))[0].uuid
                self._stats_lt[cuuid] = cur_v.data()

    def filterGenerationsList(self):
        self._filtered_gens = []
        for cur_gen in self._generations:
            cur_uuid_list = [c['uuid'] for clist in cur_gen for c in clist] #self.getUUIDListFromCellList(cur_gen)
            filtered_list = list(set([c for c in cur_uuid_list if c in self._cuuids_with_stats])) # a conversion to set then back to list removes duplicate entries
            self._filtered_gens.append(filtered_list)


    def getUUIDListFromCellList(self, gen_clist):
        cuuidlist = [p[-1:][0]['uuid'] for p in gen_clist]
        return cuuidlist

    def filterNthGenWithStats(self, gen_no):
        gen_clist = self._generations[gen_no]
        nth_gen_with_stats = []
        for alist in gen_clist:
            filtered_list = [c for c in alist if c['uuid'] in self._cuuids_with_stats]
            if len(filtered_list) > 0:
                nth_gen_with_stats.append(filtered_list)
        #nth_gen_with_stats = [c for alist in gen_clist for c in alist if c['uuid'] in self._cuuids_with_stats]
        return nth_gen_with_stats

    def getNthGenCellList(self, mother_clist):
        nth_gen_clist = gdb.getNextGenerationCells(self.getUUIDListFromCellList(mother_clist))
        return nth_gen_clist
