__author__='spark'

import dographdb as gdb
import json
from gbia import GBIAImage, GBIACluster, GBIABlob
import networkx as nx
from networkx import dijkstra_path

class CorrespondenceManager():
    #constants
    CMSTAT_INIT = 0
    CMSTAT_READY = 1
    CMSTAT_LOADING_CLUSTERS = 2
    CMSTAT_LOADING_BLOBS = 3

    __colocalizedClusterList_t1 = None
    __colocalizedBlobList_t1 = None
    #__colocalizedBlobRoiList_t1 = None
    __colocalizedClusterList_t2 = None
    __colocalizedBlobList_t2 = None
    #__colocalizedBlobRoiList_t2 = None

    __curPoolIdx_t1 = -1
    __clusterPool_t1 = None
    __clusterPool_t2 = None
    __pool_status_map_t1 = None
    __pool_status_map_t2 = None

    __status = None

    def __init__(self): #, cnodelist1=None, cnodelist2=None):
        try:
            self.__status = CorrespondenceManager.CMSTAT_INIT
            print 'initialized'
        except Exception as inst:
            print type(inst)
            print inst.args
            print inst
            raise # re-raise Exception

    @classmethod
    def fromCNodeLists(cls, cnodelist1, cnodelist2):
        if cnodelist1 and cnodelist2:
            cm = cls()
            cm.initializeClusterPool(list(cnodelist1), 1)
            cm.initializeClusterPool(list(cnodelist2), 2)
            return cm
        else:
            return None

    @classmethod
    def fromGBIAClusterLists(cls, gbiaclist1, gbiaclist2):
        if gbiaclist1 and gbiaclist2:
            cm = cls()
            cm.initializeClusterPoolFromGBIAClusters(gbiaclist1, 1)
            cm.initializeClusterPoolFromGBIAClusters(gbiaclist2, 2)
            return cm
        else:
            return None

    def initializeClusterPoolFromGBIAClusters(self, gbiaclusters, timepoint):
        clusterpool = None
        poolstatmap = None

        if timepoint == 1:
            self.__clusterPool_t1 = [] #initialize
            self.__pool_status_map_t1 = [] #initialize
            clusterpool = self.__clusterPool_t1
            poolstatmap = self.__pool_status_map_t1
        else:
            self.__clusterPool_t2 = [] #initialize
            self.__pool_status_map_t2 = [] #initialize
            clusterpool = self.__clusterPool_t2
            poolstatmap = self.__pool_status_map_t2

        clusterpool += gbiaclusters
        poolstatmap += [0]*len(clusterpool)

    def initializeClusterPool(self, cnodelist, timepoint):
        clusterpool = None
        poolstatmap = None

        if timepoint == 1:
            self.__clusterPool_t1 = [] #initialize
            self.__pool_status_map_t1 = [] #initialize
            clusterpool = self.__clusterPool_t1
            poolstatmap = self.__pool_status_map_t1
        else:
            self.__clusterPool_t2 = [] #initialize
            self.__pool_status_map_t2 = [] #initialize
            clusterpool = self.__clusterPool_t2
            poolstatmap = self.__pool_status_map_t2

        gbiac = GBIACluster.fromCNode(cnodelist.pop())
        gbiaimg = gbiac.getGBIAImage()
        clusterpool.append(gbiac)

        for cnode in cnodelist:
            clusterpool.append(GBIACluster.fromCNode(cnode, gbiaimg))

        poolstatmap += [0]*len(clusterpool)

    def loadClusterPoolByT2ImgUUID(self, t2imgUUID):
        t1imgUUID = gdb.getT1ImageByT2ImgUUID(t2imgUUID)
        if t1imgUUID:
            clist_t1 = gdb.getClustersByImgUUID(t1imgUUID)
            clist_t2 = gdb.getClustersByImgUUID(t2imgUUID)
            self.initializeClusterPool(list(cnodelist1), 1)
            self.initializeClusterPool(list(cnodelist2), 2)
            self.__status = CorrespondenceManager.CMSTAT_INIT

    def loadClusterPoolByT1ImgUUID(self, t1imgUUID):
        t2imgUUID = gdb.getT1ImageByT1ImgUUID(t1imgUUID)
        if t2imgUUID:
            clist_t1 = gdb.getClustersByImgUUID(t1imgUUID)
            clist_t2 = gdb.getClustersByImgUUID(t2imgUUID)
            self.initializeClusterPool(list(cnodelist1), 1)
            self.initializeClusterPool(list(cnodelist2), 2)
            self.__status = CorrespondenceManager.CMSTAT_INIT



    def _getNextUnseenT1IdxFromPool(self):
        noidx = -1
        for i, stat in enumerate(self.__pool_status_map_t1):
            if stat == 0:
                return i
        return noidx

    def _markIdxAsLeafCluster(self, idx, timepoint):
        if timepoint == 1:
            if self.__pool_status_map_t1[idx] > 0:
                print 'already seen t1!'
            self.__pool_status_map_t1[idx] = 2
        else:
            if self.__pool_status_map_t2[idx] > 0:
                print 'already seen t2!'
            self.__pool_status_map_t2[idx] = 2

    def _markIdxAsSeen(self, idx, timepoint):
        if timepoint == 1:
            if self.__pool_status_map_t1[idx] > 0:
                print 'already seen t1!'
            self.__pool_status_map_t1[idx] = 1
        else:
            if self.__pool_status_map_t2[idx] > 0:
                print 'already seen t2!'
            self.__pool_status_map_t2[idx] = 1

    def _getClusterNodesByUUIDList(self, uuidlist, timepoint, markasseen=False):
        clusterpool = None
        poolstatmap = None

        if timepoint == 1:
            clusterpool = self.__clusterPool_t1
            poolstatmap = self.__pool_status_map_t1
        else:
            clusterpool = self.__clusterPool_t2
            poolstatmap = self.__pool_status_map_t2

        markcount = 0
        marktarget = len(uuidlist)
        clist = []
        isvalidreq = False
        # iterate through clusterpool and mark as seen the clusters with
        # matching uuids from uuidlist
        for idx, cluster in enumerate(clusterpool):
            if cluster.getUUID() in uuidlist:
                clist.append(cluster)
                if markasseen:
                    self._markIdxAsSeen(idx, timepoint)
                markcount += 1 # increment counter
                if markcount == marktarget:
                    isvalidreq = True
                    break

        return clist, isvalidreq

    def _getUnseenClustersByUUIDList(self, uuidlist, timepoint):
        clusterpool = None
        poolstatmap = None

        if timepoint == 1:
            clusterpool = self.__clusterPool_t1
            poolstatmap = self.__pool_status_map_t1
        else:
            clusterpool = self.__clusterPool_t2
            poolstatmap = self.__pool_status_map_t2

        markcount = 0
        marktarget = len(uuidlist)
        clist = []
        isvalidreq = False
        # iterate through clusterpool and mark as seen the clusters with
        # matching uuids from uuidlist
        for idx, cluster in enumerate(clusterpool):
            if cluster.getUUID() in uuidlist:
                clist.append(cluster)
                self._markIdxAsSeen(idx, timepoint)
                markcount += 1 # increment counter
                if markcount == marktarget:
                    isvalidreq = True
                    break

        return clist, isvalidreq


    def loadColocalizedBlobs(self, clist, timepoint):
        self.__status = CorrespondenceManager.CMSTAT_LOADING_BLOBS
        bloblist = None

        if timepoint == 1:
            self.__colocalizedBlobList_t1 = [] #initialize
            bloblist = self.__colocalizedBlobList_t1
        else:
            self.__colocalizedBlobList_t2 = [] #initialize
            bloblist = self.__colocalizedBlobList_t2

        bnodetuplelist = []
        for cluster in clist:
            #bloblist.append(gdb.getBlobNodesByClusterUUID(clusterUUID))
            cur_bnodes = gdb.getBlobNodesByClusterUUID(cluster.getUUID())
            bc_tuples = [(bnode, cluster) for bnode in cur_bnodes]
            bnodetuplelist += bc_tuples

        for bnode, gbiacluster in bnodetuplelist:
            bloblist.append(GBIABlob(bnode, gbiacluster, gbiacluster.getGBIAImage()))


        #roilist = []
        #for blob in bloblist:
        #	roi = Polygon(json.loads(blob.roi))
        #	roilist.append(roi)

        #self.__colocalizedBlobRoiList_t1 = roilist

    def loadColocalizedClusterSetsByClusterUUID(self, clusteruuid_t1, markasseen=False):
        self.__status = CorrespondenceManager.CMSTAT_LOADING_CLUSTERS
        cocluster_uuidlist_t1, cocluster_uuidlist_t2 = gdb.doFindColocalizedClusterSetsByUUID(clusteruuid_t1)
        clist1, t1listvalid = self._getClusterNodesByUUIDList(cocluster_uuidlist_t1, 1, markasseen)
        clist2, t2listvalid = self._getClusterNodesByUUIDList(cocluster_uuidlist_t2, 2, markasseen)

        if t1listvalid and t2listvalid:
            # load them in place for further processing
            self.__colocalizedClusterList_t1 = clist1
            self.__colocalizedClusterList_t2 = clist2
        else:
            print 'cannot load colocalized clusters due to error' # may or may not need to introduce proper error handling


        # processing clist1, a cluster list for time t1
        self.loadColocalizedBlobs(clist1, 1)

        # processing clist2, a cluster list for time t2
        self.loadColocalizedBlobs(clist2, 2)

        self.__status = CorrespondenceManager.CMSTAT_READY

    def _loadNextColocalizedClusterSets(self, nextidx):
        nextt1cluster = self.__clusterPool_t1[nextidx]
        cocluster_uuidlist_t1, cocluster_uuidlist_t2 = gdb.doFindColocalizedClusterSetsByUUID(nextt1cluster.getUUID())

        clist1 = []
        clist2 = []
        retstat = 0
        if cocluster_uuidlist_t1 and cocluster_uuidlist_t2:
            clist1, t1listvalid = self._getUnseenClustersByUUIDList(cocluster_uuidlist_t1, 1)
            clist2, t2listvalid = self._getUnseenClustersByUUIDList(cocluster_uuidlist_t2, 2)
            if t1listvalid and t2listvalid:
                # load them in place for further processing
                self.__colocalizedClusterList_t1 = clist1
                self.__colocalizedClusterList_t2 = clist2
            else:
                print 'cannot load colocalized clusters due to error' # may or may not need to introduce proper error handling
                retstat = -2
        else:
            retstat = -1
        return clist1, clist2, retstat

    def loadNextColocalizedClusterSets(self): #, clist1, clist2):
        self.__status = CorrespondenceManager.CMSTAT_LOADING_CLUSTERS
        nextidx = self._getNextUnseenT1IdxFromPool()
        self.__curPoolIdx_t1 = nextidx
        while nextidx >= 0:
            clist1, clist2, retstat = self._loadNextColocalizedClusterSets(nextidx)
            if retstat == 0:
                # processing clist1, a cluster list for time t1
                self.loadColocalizedBlobs(clist1, 1)
                # processing clist2, a cluster list for time t2
                self.loadColocalizedBlobs(clist2, 2)
                self.__status = CorrespondenceManager.CMSTAT_READY
                break
            elif retstat == -1:
                self._markIdxAsLeafCluster(nextidx, 1)
                nextidx = self._getNextUnseenT1IdxFromPool()
            else:
                break;

        return nextidx


    def sortByXCoord(self, isreverse=False):
        t1sortedblobs = None
        t2sortedblobs = None
        if self.__status == CorrespondenceManager.CMSTAT_READY: #self.__colocalizedBlobList_t1:
            t1sortedblobs = sorted([(idx, gbiablob, gbiablob.getCentroid().x) for idx, gbiablob in enumerate(self.__colocalizedBlobList_t1)], key=lambda roix: roix[2], reverse=isreverse)

            t2sortedblobs = sorted([(idx, gbiablob, gbiablob.getCentroid().x) for idx, gbiablob in enumerate(self.__colocalizedBlobList_t2)], key=lambda roix: roix[2], reverse=isreverse)

        return t1sortedblobs, t2sortedblobs

    def sortByYCoord(self, isreverse=False):
        t1sortedblobs = None
        t2sortedblobs = None
        if self.__status == CorrespondenceManager.CMSTAT_READY: #self.__colocalizedBlobList_t1:
            t1sortedblobs = sorted([(idx, gbiablob, gbiablob.getCentroid().y) for idx, gbiablob in enumerate(self.__colocalizedBlobList_t1)], key=lambda roiy: roiy[2], reverse=isreverse)

            t2sortedblobs = sorted([(idx, gbiablob, gbiablob.getCentroid().y) for idx, gbiablob in enumerate(self.__colocalizedBlobList_t2)], key=lambda roiy: roiy[2], reverse=isreverse)

        return t1sortedblobs, t2sortedblobs

    def calcExpectedGrowthRate(self, t1cells, t2cells):
        t1sizetotal = sum([cell.getArea() for cell in t1cells])
        t2sizetotal = sum([cell.getArea() for cell in t2cells])
        #expectedgrowth = float(t2sizetotal)/len(t2cells) - float(t1sizetotal)/len(t1cells)
        growthsum = t2sizetotal - t1sizetotal
        #t1sizemean = float(t1sizetotal)/len(t1cells)

        #return expectedgrowth/t1sizemean
        return float(growthsum)/t1sizetotal

    # t1idx is an integer representing the current index in t1sortedtuples list
    # t2idx is an integer representing the current index in t2sortedtuples list
    # t1idx denotes m, and m' is a virtual index in between m and m+1
    # t2idx denotes n, and n' is a virtual index in between n and n+1
    #
    # correspondence topology unit:
    #       n   n'
    #
    # m     1   1
    # m'    1   0
    #
    # indice update rules:
    #       one2one
    #       1   0
    #       0   0
    #       jump 2 columns to the right and 2 rows to the bottom
    #       m+1, n+1
    #
    #       one2two
    #       0   1
    #       0   0
    #       jump 4 columns to the right and 2 rows to the bottom
    #       m+1, n+2
    #
    #       two2one
    #       0   0
    #       1   0
    #       jump 2 columns to the right and 4 rows to the bottom
    #       m+2, n+1
    #
    # def processCorrespondenceUnit(self, t1sortedtuples, t2sortedtuples, expgrowthrate, t1idx, t2idx):
    #     gbiablob_t1 = t1sortedtuples[t1idx][1]
    #     gbiablob_t2 = t2sortedtuples[t2idx][1]
    #     one2one = gbiablob_t1.calcAdjustedMeanSquaredErrorAgainstGeoNode(gbiablob_t2, expgrowthrate, realign=True)
    #
    #     two2one = float("inf") # positive infinity
    #     one2two = float("inf")
    #     if len(t1sortedtuples) > t1idx + 1: # if t1idx+1 exists
    #         gbiablob_t1_list = [gbiablob_t1, t1sortedtuples[t1idx+1][1]]
    #         two2one = gbiablob_t2.calcAdjustedMeanSquaredErrorAgainstGeoNodeList(gbiablob_t1_list, expgrowthrate, realign=True)
    #     if len(t2sortedtuples) > t2idx + 1: # if t2idx+1 exists
    #         gbiablob_t2_list = [gbiablob_t2, t2sortedtuples[t2idx+1][1]]
    #         one2two = gbiablob_t1.calcAdjustedMeanSquaredErrorAgainstGeoNodeList(gbiablob_t2_list, expgrowthrate, realign=True)
    #     sorted_mse_list = sorted(enumerate([one2one, one2two, two2one]), key=lambda x: x[1])
    #
    #     correspondence = sorted_mse_list[0][0] # get the index of the sorted list
    #     nextt1idx = t1idx
    #     nextt2idx = t2idx
    #
    #     if correspondence == 0: # one2one
    #         nextt1idx += 1
    #         nextt2idx += 1
    #     elif correspondence == 1: # one2two
    #         nextt1idx += 1
    #         nextt2idx += 2
    #     elif correspondence == 2: # two2one
    #         nextt1idx += 2
    #         nextt2idx += 1
    #
    #     return correspondence, (nextt1idx, nextt2idx)

    def processCorrespondenceUnit(self, t1sortedtuples, t2sortedtuples, expgrowthrate, t1idx, t2idx):
        gbiablob_t1 = t1sortedtuples[t1idx][1]
        gbiablob_t2 = t2sortedtuples[t2idx][1]
        one2one = gbiablob_t1.calcAdjustedMeanSquaredErrorAgainstGeoNode(gbiablob_t2, expgrowthrate, realign=True)

        two2one = float("inf") # positive infinity
        one2two = float("inf")
        if len(t1sortedtuples) > t1idx + 1: # if t1idx+1 exists
            gbiablob_t1_fused = gbiablob_t1.fuseGeoNode(t1sortedtuples[t1idx+1][1])
            two2one = gbiablob_t1_fused.calcAdjustedMeanSquaredErrorAgainstGeoNode(gbiablob_t2, expgrowthrate, realign=True)
        if len(t2sortedtuples) > t2idx + 1: # if t2idx+1 exists
            gbiablob_t2_fused = gbiablob_t2.fuseGeoNode(t2sortedtuples[t2idx+1][1])
            one2two = gbiablob_t1.calcAdjustedMeanSquaredErrorAgainstGeoNode(gbiablob_t2_fused, expgrowthrate, realign=True)
        sorted_mse_list = sorted(enumerate([one2one, one2two, two2one]), key=lambda x: x[1])

        correspondence = sorted_mse_list[0][0] # get the index of the sorted list
        nextt1idx = t1idx
        nextt2idx = t2idx

        if correspondence == 0: # one2one
            nextt1idx += 1
            nextt2idx += 1
        elif correspondence == 1: # one2two
            nextt1idx += 1
            nextt2idx += 2
        elif correspondence == 2: # two2one
            nextt1idx += 2
            nextt2idx += 1

        return correspondence, (nextt1idx, nextt2idx)

    def _getAssignmentTriplicateIndices(self, ag, curnode):
        nodedata = ag.node.get(curnode)
        xidxlist = nodedata['x']
        yidxlist = nodedata['y']
        xidx = xidxlist[len(xidxlist)-1] # grab the last idx in the list
        yidx = yidxlist[len(yidxlist)-1] # grab the last idx in the list

        idxtuples = []
        #for i in range(0,3):
        # one2one
        idxtuples.append(([xidx+1], [yidx+1]))
        # one2two
        idxtuples.append(([xidx+1], [yidx+1,yidx+2]))
        # two2one
        idxtuples.append(([xidx+1,xidx+2], [yidx+1]))

        #for idxs in idxtuples:
        #	nodeidstr = str(idxs[0]) + '_' + str(idxs[1])
        #	ag.add_node(nodeidstr, {'x':idxs[0], 'y':idxs[1]})

        return idxtuples


    def _addTriplicateNodes(self, ag, curnode, idxtuples, t1sortedtuples, t2sortedtuples):
        leafnodes = []
        t1len = len(t1sortedtuples)
        t2len = len(t2sortedtuples)
        # one2one
        t1idxs = idxtuples[0][0]
        t2idxs = idxtuples[0][1]
        if sum([v1 < t1len for v1 in t1idxs]) == 1 and sum([v2 < t2len for v2 in t2idxs]) == 1:
            gbiablob_t1 = t1sortedtuples[t1idxs[0]][1]
            gbiablob_t2 = t2sortedtuples[t2idxs[0]][1]
            mse = gbiablob_t1.calcMeanSquaredErrorAgainstGeoNode(gbiablob_t2, realign=True)
            nextnode = str(t1idxs)+ str(t2idxs)
            ag.add_node(nextnode, {'x':t1idxs, 'y':t2idxs})
            ag.add_edge(curnode,nextnode, weight=mse)
            leafnodes.append(nextnode)
        #else:
        #	raise ValueError('_addTriplicateNodes received out of range idxtuples!')

        # one2two
        t1idxs = idxtuples[1][0]
        t2idxs = idxtuples[1][1]
        mse = float("inf") # positive infinity
        if sum([v1 < t1len for v1 in t1idxs]) == 1 and sum([v2 < t2len for v2 in t2idxs]) == 2:
            gbiablob_t2_fused = gbiablob_t2.fuseGeoNode(t2sortedtuples[t2idxs[1]][1])
            mse = gbiablob_t1.calcMeanSquaredErrorAgainstGeoNode(gbiablob_t2_fused, realign=True)
            #gbiablob_t2_list = [gbiablob_t2, t2sortedtuples[t2idxs[1]][1]]
            #mse = gbiablob_t1.calcMeanSquaredErrorAgainstGeoNodeList(gbiablob_t2_list, realign=True)
            nextnode = str(t1idxs) + '[' + str(t2idxs[0]) + '+' + str(t2idxs[1]) + ']'
            ag.add_node(nextnode, {'x':t1idxs, 'y':t2idxs})
            ag.add_edge(curnode,nextnode, weight=mse)
            leafnodes.append(nextnode)
        #else:
        #	raise ValueError('_addTriplicateNodes received out of range idxtuples!')

        # two2one
        t1idxs = idxtuples[2][0]
        t2idxs = idxtuples[2][1]
        mse = float("inf")
        if sum([v1 < t1len for v1 in t1idxs]) == 2 and sum([v2 < t2len for v2 in t2idxs]) == 1:
            gbiablob_t1_fused = gbiablob_t1.fuseGeoNode(t1sortedtuples[t1idxs[1]][1])
            mse = gbiablob_t1_fused.calcMeanSquaredErrorAgainstGeoNode(gbiablob_t2, realign=True)
            # gbiablob_t1_list = [gbiablob_t1, t1sortedtuples[t1idxs[1]][1]]
            # mse = gbiablob_t2.calcMeanSquaredErrorAgainstGeoNodeList(gbiablob_t1_list, realign=True)
            nextnode = '['+str(t1idxs[0])+'+'+str(t1idxs[1]) + ']' + str(t2idxs)
            ag.add_node(nextnode, {'x':t1idxs, 'y':t2idxs})
            ag.add_edge(curnode,nextnode, weight=mse)
            leafnodes.append(nextnode)
        #else:
        #	raise ValueError('_addTriplicateNodes received out of range idxtuples!')

        return leafnodes

    def _isNodeTheLastCellPair(self, ag, cellnode, t1len, t2len):
        t1idxs = ag.node[cellnode]['x']
        t2idxs = ag.node[cellnode]['y']
        if (t1len - 1) in t1idxs and (t2len - 1) in t2idxs:
            return True
        else:
            return False

    def _constructAssignmentGraph(self, t1sortedtuples, t2sortedtuples):
        is_valid_graph = False
        ag = nx.DiGraph()

        ag.add_node('start',{'x': [-1], 'y': [-1]})
        ag.add_node('end',{'x':[-2], 'y':[-2]})
        endnode = 'end'
        curnode = 'start'
        idxtuples = self._getAssignmentTriplicateIndices(ag, curnode)
        leafnodepool = self._addTriplicateNodes(ag, curnode, idxtuples, t1sortedtuples, t2sortedtuples)
        t1len = len(t1sortedtuples)
        t2len = len(t2sortedtuples)

        processed_nodes = []
        while leafnodepool:
            curnode = leafnodepool.pop()
            processed_nodes.append(curnode) # add to the processed list
            idxtuples = self._getAssignmentTriplicateIndices(ag, curnode)
            leafnodes = self._addTriplicateNodes(ag, curnode, idxtuples, t1sortedtuples, t2sortedtuples)
            # filter any new leafnodes if already been processed
            leafnodes = [node for node in leafnodes if node not in processed_nodes]
            if leafnodes:
                leafnodepool = leafnodepool + leafnodes
            elif self._isNodeTheLastCellPair(ag, curnode, t1len, t2len): # curnode is a leafnode qualifying to be connected to the endnode (node pairing the last cells of two temporal clusters)
                ag.add_edge(curnode, endnode, weight=1.0)
                is_valid_graph = True # the graph is now valid in a sense that the endnode is reacheable from the startnode
            # nodes resulting in a premature path ending shall not be connected to the endnode, in order to stop the path to a deadend leafnode from being chosen as a shortest path.

        return ag, is_valid_graph

    def _determineSingleCellCorrespondenceByGraph(self, t1sortedtuples, t2sortedtuples):
        t1list = [gbiablob for idx, gbiablob, ycoord in t1sortedtuples]
        t2list = [gbiablob for idx, gbiablob, ycoord in t2sortedtuples]

        correspondingpairs = []
        ag, is_valid_graph = self._constructAssignmentGraph(t1sortedtuples, t2sortedtuples)
        try:
            if is_valid_graph:
                sol_path = dijkstra_path(ag, 'start', 'end')
                trimmed_sol_path = sol_path[1:len(sol_path)-1]

                #prev_t1_idx = prev_t2_idx = -1 #initialize
                prev_node = 'start' # initialize
                for sol_node in trimmed_sol_path:
                    cur_t1_idx = ag.node[sol_node]['x']
                    cur_t2_idx = ag.node[sol_node]['y']
                    assignment_cost = ag.get_edge_data(prev_node, sol_node)['weight'] # get the edge weight

                    # convert graph node indices to cluster-blob_list indices
                    cur_t1_blob_idx = [t1sortedtuples[idx][0] for idx in cur_t1_idx]
                    cur_t2_blob_idx = [t2sortedtuples[idx][0] for idx in cur_t2_idx]

                    cur_cell_pair = (cur_t1_blob_idx[0], cur_t2_blob_idx[0])
                    correspondingpairs.append((cur_cell_pair, assignment_cost))
                    if len(cur_t1_idx) > 1: # the seconde of the two two2one cell pairs
                        correspondingpairs.append(((cur_t1_blob_idx[1], cur_t2_blob_idx[0]), assignment_cost))
                    elif len(cur_t2_idx) > 1: # the second of the two one2two cell pairs
                        correspondingpairs.append(((cur_t1_blob_idx[0], cur_t2_blob_idx[1]), assignment_cost))
                    prev_node = sol_node # save the current node as prev_node for next round
            else:
                curT1ClusterUUID = self.__clusterPool_t1[self.__curPoolIdx_t1].getUUID()
                print 'skipping an invalid graph based on the cluster uuid: ' + str(curT1ClusterUUID)
        except nx.NetworkXNoPath as e:
            print type(e)
            print e.args
            print e
            curT1ClusterUUID = self.__clusterPool_t1[self.__curPoolIdx_t1].getUUID()
            cocluster_uuidlist_t1, cocluster_uuidlist_t2 = gdb.doFindColocalizedClusterSetsByUUID(curT1ClusterUUID)
            print 'current T1 cluster UUID: ' + str(curT1ClusterUUID)
            print 'colocalized cluster t1: ' + str(cocluster_uuidlist_t1)
            print 'colocalized cluster t2: ' + str(cocluster_uuidlist_t2)


            # save the indices for next turn
            #prev_t1_idx = cur_t1_idx
            #prev_t2_idx = cur_t2_idx

        return correspondingpairs


    def _determineSingleCellCorrespondence(self, t1sortedtuples, t2sortedtuples):
        t1list = [gbiablob for idx, gbiablob, ycoord in t1sortedtuples]
        t2list = [gbiablob for idx, gbiablob, ycoord in t2sortedtuples]
        t2list.reverse() # reversed so that pop() matchs indices from 0,1,2...
        expectedgrowthrate = self.calcExpectedGrowthRate(t1list, t2list)

        correspondences = []

        carryover = 0 # fillsize carried over from previous bucket
        t1len = len(t1list)
        t2len = len(t2list)

        t1idx = 0
        t2idx = 0
        while t1idx+1 <= t1len and t2idx+1 <= t2len:
            cur_correspondence, nextidxtuple = self.processCorrespondenceUnit(t1sortedtuples, t2sortedtuples, expectedgrowthrate, t1idx, t2idx)
            if cur_correspondence == 0: #one2one
                correspondences.append((t1sortedtuples[t1idx][0], t2sortedtuples[t2idx][0]))
            elif cur_correspondence == 1: #one2two
                correspondences.append((t1sortedtuples[t1idx][0], t2sortedtuples[t2idx][0]))
                if t2idx+1 < t2len:
                    correspondences.append((t1sortedtuples[t1idx][0], t2sortedtuples[t2idx+1][0]))
            elif cur_correspondence == 2: #two2one
                correspondences.append((t1sortedtuples[t1idx][0], t2sortedtuples[t2idx][0]))
                if t1idx+1 < t1len:
                    correspondences.append((t1sortedtuples[t1idx+1][0], t2sortedtuples[t2idx][0]))

            t1idx = nextidxtuple[0]
            t2idx = nextidxtuple[1]

        return correspondences
    # end of def

    def _determineSingleCellCorrespondence_old(self, t1sortedtuples, t2sortedtuples):
        t1list = [gbiablob for idx, gbiablob, ycoord in t1sortedtuples]
        t2list = [gbiablob for idx, gbiablob, ycoord in t2sortedtuples]
        t2list.reverse() # reversed so that pop() matchs indices from 0,1,2...
        expectedgrowthrate = self.calcExpectedGrowthRate(t1list, t2list)

        correspondences = []

        carryover = 0 # fillsize carried over from previous bucket
        t2idx = -1
        t1len = len(t1list)
        for t1idx, t1c in enumerate(t1list):
            bucketsize = t1c.getArea()
            fillsize = carryover

            if expectedgrowthrate >= 0:
                while bucketsize >= fillsize:
                    t2c = t2list.pop()
                    t2idx+=1 # increment t2idx
                    correspondences.append((t1sortedtuples[t1idx][0], t2sortedtuples[t2idx][0]))
                    fillsize += t2c.getArea()

                if float(fillsize - bucketsize)/bucketsize > expectedgrowthrate*3:
                    if t1idx < t1len-1:
                        carryover = fillsize - bucketsize
                        correspondences.append((t1sortedtuples[t1idx+1][0], t2sortedtuples[t2idx][0]))
                    else:
                        print 'The cell (idx:'+str(t1sortedtuples[t1idx][0])+') from t1 grew unexpectedly'
                else:
                    carryover = 0
            else: # expectedgrowthrate < 0
                loopcount = 0
                while (1 + 3*expectedgrowthrate)*bucketsize >= fillsize:
                    t2c = t2list.pop()
                    t2idx+=1 # increment t2idx
                    correspondences.append((t1sortedtuples[t1idx][0], t2sortedtuples[t2idx][0]))
                    fillsize += t2c.getArea()
                    loopcount+=1

                if float(fillsize - bucketsize)/bucketsize > expectedgrowthrate*3:
                    carryover = fillsize - bucketsize(1 + expectedgrowthrate*3)
                    correspondences.append((t1sortedtuples[t1idx+1][0], t2sortedtuples[t2idx][0]))
                else:
                    carryover = 0

            #raise # re-raise Exception


        return correspondences
    # end of def

    def determineSingleCellCorrespondenceByGraph(self): #, t1cells, t2cells):
        try:
            # the coord space of Polygon is based on Fiji's image coord space: (0,0) on the top left corner and (max_x, max_y) on the bottom right corner.
            # this makes sorting in an ascending order (default) makes cells on the bottom come later in the the list
            # the stacking up of cells for one2two or two2one mapping is done in the order of the sorted list
            # therefore the sorted list should have bottom cells come earlier in the list: in this case the reverse order.
            # mse calculation of one2two or two2one using fuseGeoNode() method works either way
            t1sortedtuples, t2sortedtuples = self.sortByYCoord(isreverse=True)
            correspondences = self._determineSingleCellCorrespondenceByGraph(t1sortedtuples, t2sortedtuples)

            errorstatus = 0

        except IndexError as inst:
            print type(inst)
            print inst.args
            print inst
            correspondences = []
            errorstatus =  -1

        return correspondences

    def determineSingleCellCorrespondence(self): #, t1cells, t2cells):
        try:
            t1sortedtuples, t2sortedtuples = self.sortByYCoord(isreverse=True)
            forward_correspondences = self._determineSingleCellCorrespondence(t1sortedtuples, t2sortedtuples)

            t1sortedtuples, t2sortedtuples = self.sortByYCoord(isreverse=True)
            backward_correspondences = self._determineSingleCellCorrespondence(t1sortedtuples, t2sortedtuples)
            forwardset = set(forward_correspondences)
            backwardset = set(backward_correspondences)
            emptyset = set([])
            isidentical = (forwardset.difference(backwardset) == emptyset) and (backwardset.difference(forwardset) == emptyset)
            errorstatus = 0

        except IndexError as inst:
            print type(inst)
            print inst.args
            print inst
            forward_correspondences = []
            backward_correspondences = []
            isidentical = True
            errorstatus =  -1

        return forward_correspondences, backward_correspondences, isidentical, errorstatus

    def _linkCorrespondingBlobPair(self, idxpair, cost):
        gbiablob_t1 = self.__colocalizedBlobList_t1[idxpair[0]]
        gbiablob_t2 = self.__colocalizedBlobList_t2[idxpair[1]]

        gdb.linkBlob_temporally_corresponds_Blob(gbiablob_t1.getGraphNode(), gbiablob_t2.getGraphNode(), weight=cost)

    def processCorrespondences(self, is_graph_method=True):
        if is_graph_method:
            while self.loadNextColocalizedClusterSets() >= 0:
                cur_correspondences = self.determineSingleCellCorrespondenceByGraph()
                for cpair, assignment_cost in cur_correspondences:
                    self._linkCorrespondingBlobPair(cpair, assignment_cost)
        else:
            while self.loadNextColocalizedClusterSets() >= 0:
                cur_correspondences = self.determineSingleCellCorrespondence()
                if cur_correspondences[3] == 0: # errorstatus check, 0 means things are under control
                    if cur_correspondences[2]: # isidentical test
                        indice_pairs = cur_correspondences[0] # pick the forward set
                        # link up single cell level correspondences
                        for cpair in indice_pairs:
                            self._linkCorrespondingBlobPair(cpair)



