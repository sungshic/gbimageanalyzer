import sys

sys.path.append('/grabia/gbimageanalyzer/lib/ij.jar')
#sys.path.append('../lib/ij-1.47q.jar')
#sys.path.append('./jars/Micro-Manager/MMAcqEngine.jar')
#sys.path.append('./jars/Micro-Manager/MMCoreJ.jar')
#sys.path.append('./jars/Micro-Manager/MMJ_.jar')

sys.path.append('/grabia/gbimageanalyzer/lib/OME/bio-formats.jar')
#sys.path.append('./jars/OME/loci-common.jar')
#sys.path.append('./jars/OME/ome-xml.jar')
#sys.path.append('./jars/OME/loci_plugins.jar')

sys.path.append('/grabia/gbimageanalyzer/lib/OME/loci_tools.jar')

from ij import IJ, ImageStack, ImagePlus, CompositeImage
from loci.plugins.util import ImageProcessorReader
from loci.formats import ChannelMerger, ChannelSeparator

from MicroManagerDAO import MicroManagerDAO
import logging

class ImageVSManager(MicroManagerDAO):
#	__baseDir__ = None
#	__mmDAO__ = None
    def __init__(self, baseDir, landmarkFilename):
        try:
            self.logger = logging.getLogger('grabia_app.gbimageanalyzer.core.ImageVSManager')
            self.logger.info('initializing an instance of ImageVSManager')
#			self.__baseDir__ = baseDir
            MicroManagerDAO.__init__(self, baseDir, landmarkFilename) # jython style super init call
            self.logger.debug('IIIIIIIIIIIIIIIIIIMAGEVSMANAGER init finished, ' + self.__landmarkFilename__)

        except Exception as inst:
            print type(inst)
            print inst.args
            print inst
            raise # re-raise Exception

    def makeComposite(self, imps):
        stack = ImageStack(imps[0].width, imps[0].height)
        baseIp = imps[0].getProcessor() #.toFloat(0, None) #.convertToRGB()
        #baseIp2 = imps[0].getProcessor().toFloat(1, None) #.convertToRGB()
        #baseIp3 = imps[0].getProcessor().toFloat(2, None) #.convertToRGB()
        stack.addSlice(None, baseIp)
        #stack.addSlice(None, baseIp2)
        #stack.addSlice(None, baseIp3)
        for imp in imps[1:len(imps)]:
            chIp = imp.getProcessor().toFloat(1, None)
            stack.addSlice(chIp)

        impFlattened = ImagePlus("composite", stack)
        impFlattened.setDimensions(2, 1, 1)
        comp = CompositeImage(impFlattened, CompositeImage.COMPOSITE)

        comp.setOpenAsHyperStack(True)
        return comp

    def createVS(self):
        stack = ImageStack(self.width(), self.height())
        for t in range(0, self.numFrames()):
            imps = []
            for c in range(0, self.numChannels()):
                filename = self.getFilename(0, c, t)
                if filename != None:
                    imps.append(IJ.openImage(self.__baseDir__ + filename))
            print imps
            if len(imps) > 1:
                comp = self.makeComposite(imps)
                stack.addSlice(None, comp.getProcessor())

#		if comp != None:
#			IJ.run(comp, "Enhance Contrast...", "saturated=0.4 process_all")
#			comp.show()

        stackedImp = ImagePlus("timelapse", stack)
        #stackedImp.setDimensions(2, 1, self.__mmDAO__.numFrames())
        IJ.run(stackedImp, "Enhance Contrast...", "saturated=0.4 process_all")
        stackedImp.show()

    def getImagePlusObjByFilepath(self, filepath, gain=1.0):
        self.logger.debug('cur file: ' + filepath)
        if (filepath != None):
            img = IJ.openImage(filepath)
            imp = img.createImagePlus()
            ip = img.getProcessor()
            if gain != 1.0:
                ip.multiply(gain)
            imp.setProcessor(filepath, ip)

            return imp
        else:
            return None

    def getImagePlusObj(self, zIdx, cIdx, tIdx, gain=1.0):
        filename = self.getFilename(zIdx, cIdx, tIdx)
        #print 'cur file: ' + filename
        if (filename != None):
            img = IJ.openImage(self.__baseDir__ + filename)
            ip = img.getProcessor()

            if gain != 1.0:
                ip.multiply(gain)

            imp = img.createImagePlus()
            imp.setProcessor(filename, ip)

            return imp, filename
        else:
            return None, None

    def getLandmarkImage(self):
        landmarkImg = IJ.openImage(self.getLandmarkFilepath())
        landmarkImp = landmarkImg.createImagePlus()

        landmarkIp = landmarkImg.getProcessor()
        landmarkImp.setProcessor("landmark", landmarkIp)

        #landmarkWidth = landmarkImp.getWidth()
        #landmarkHeight = landmarkImp.getHeight()

        return landmarkImp

    def getLandmarkFilepath(self):
        #landmarkFilename = "landmark.tif"
        #return self.__baseDir__ + self.__landmarkFilename__
        #print 'IIIIIIIIIIIIIIIIIIIMAGEVSMANAGER getLandmarkFilepath, ', self.__landmarkFilename__
        return self.__landmarkFilename__

    def getMetadataFilepath(self):
        return self.__baseDir__ + self.__METADATA_FILE__


    def getFrameInterval_ms(self):
        return self.__frameInterval_ms__






