__author__='spark'

import json
import re

class ImageListManager:
    __METADATA_FILE__ = 'metadata.txt'
    __metadata__ = None
    __baseDir__ = None
    __headerUUID__ = None
    __numFrames__ = None
    __numChannels__ = None
    __chNames__ = []
    __imgHeight__ = None
    __imgWidth__ = None
    __frameInterval_ms__ = None
    __refTimestamp__ = None
    __landmarkFilename__ = None

    def __init__(self, baseDir, landmarkFilename=None):
        try:
            self.__baseDir__ = baseDir + '/'
            self.__landmarkFilename__ = landmarkFilename
            with open (self.__baseDir__ + self.__METADATA_FILE__) as mdFile:
                mdStr = mdFile.read()
            self.__metadata__ = json.loads(mdStr) #parse the json string into python
            mdHeader = self.__metadata__['Summary']
            self.__numFrames__ = mdHeader['Frames']
            self.__numChannels__ = mdHeader['Channels']
            self.__imgWidth__ = mdHeader['Width']
            self.__imgHeight__ = mdHeader['Height']
            self.__frameInterval_ms__ = mdHeader['Interval_ms']
            self.__refTimestamp__ = mdHeader['Time']
            self.__chNames__ = mdHeader['ChNames']
            self.__headerUUID__ = mdHeader['UUID']

        except Exception as inst:
            print type(inst)
            print inst.args
            print inst
            raise # re-raise Exception

    def width(self):
        return self.__imgWidth__

    def height(self):
        return self.__imgHeight__

    def numFrames(self):
        return self.__numFrames__

    def numChannels(self):
        return self.__numChannels__

    def getBaseDir(self):
        return self.__baseDir__

    def getFilename(self, zIdx, cIdx, tIdx):
        try:
            keyStr = 'FrameKey-' + str(tIdx) + '-' + str(cIdx) + '-' + str(zIdx)

            return str(self.__metadata__[keyStr]['FileName'])

        except Exception:
            return None

    def getHeaderUUID(self):
        try:
            return str(self.__headerUUID__)

        except Exception:
            return None

    def getZCTFromUUID(self, uuidStr):
        try:
            for k, v in self.__metadata__.iteritems():
                if k.find('FrameKey') == 0:
                    if v['UUID'] == uuidStr:
                        reObj = re.match( r'FrameKey\-(\d*)\-(\d*)\-(\d*)', k)
                        if reObj:
                            return int(reObj.group(3)), int(reObj.group(2)), int(reObj.group(1))
                        else:
                            return None, None, None
        except Exception as e:
            print type(e)
            print e.args
            print e
            return None, None, None

    def getUUID(self, zIdx, cIdx, tIdx):
        try:
            keyStr = 'FrameKey-' + str(tIdx) + '-' + str(cIdx) + '-' + str(zIdx)

            return str(self.__metadata__[keyStr]['UUID'])

        except Exception:
            return None

    def listImageFiles(self, zIdx, cIdx):
        try:
            #cs = ChannelMerger()
            #cs.setId(
            for i in range(0, self.__numFrames__):
                for c in range(0, self.__numChannels__):
                    if cIdx == c:
                        filename = self.getFilename(0, c, i)
                        if filename != None:
                            print filename
        except Exception:
            raise

    def getImagePathList(self, from_f_idx, to_f_idx, c_idx):
        path_list = []
        if to_f_idx == 0:
            to_f_idx = self.numFrames()
        for i in range(from_f_idx, to_f_idx+1):
            for c in range(0, self.__numChannels__):
                if c_idx == c:
                    filename = self.getFilename(0, c, i)
                    if filename != None:
                        path_list.append((i, filename, self.getBaseDir()+filename))

        return path_list


