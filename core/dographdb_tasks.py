from __future__ import absolute_import
from .celery_client import celery
import sys
import subprocess

class Blob(Node):
    element_type = "cellblob"

    landmarkOffsetX = Integer()
    landmarkOffsetY = Integer()
    roi = object()
    timestampe = Integer()

class Image(Node):
    element_type = "image"
    
    contentType = String()
    filepath = String()
    ijip = object()
    landmarkOffsetX = Integer()
    landmarkOffsetY = Integer()
    landmarkRefFilepath = String()
    metadataFilepath = String()
    timestamp = Integer()

class Experiment(Node):
    element_type = "experiment"

    experimentType = String()
    metadataFilepath = String()
    details = String()

### the following functions do not implement the proxy model graph handling yet
@celery.task
def _createBlobNode(offsetX, offsetY, roi, timestamp):
	print 'not implemented locally'
	return None
_createBlobNode.name = 'dev.gos_gia.tasks_server.createBlobNode'
def createBlobNode(offsetX, offsetY, roi, timestamp):
	return _createBlobNode.delay(offsetX, offsetY, roi, timestamp)

def _getNodeByUUID(uuidStr):
	print 'not implemented locally'
	return None
_getNodeByUUID.name = 'dev.gos_gia.tasks_server.getNodeByUUID'
def getNodeByUUID(uuidStr):
	return _getNodeByUUID.delay(uuidStr)

def _updateImageNode(imageNode, uuid, contentType, filepath, ijip, offsetX, offsetY, landmarkFilepath, metadataFilepath, timestamp):
	print 'not implemented locally'
	return None
_updateImageNode.name = 'dev.gos_gia.tasks_server.updateImageNode'
def updateImageNode(imageNode, uuid, contentType, filepath, ijip, offsetX, offsetY, landmarkFilepath, metadataFilepath, timestamp):
	return _updateImageNode.delay(imageNode, uuid, contentType, filepath, ijip, offsetX, offsetY, landmarkFilepath, metadataFilepath, timestamp)
 
def _createImageNode(uuid, contentType, filepath, ijip, offsetX, offsetY, landmarkFilepath, metadataFilepath, timestamp):
	print 'not implemented locally'
	return None
_createImageNode.name = 'dev.gos_gia.tasks_server.createImageNode'
def createImageNode(uuid, contentType, filepath, ijip, offsetX, offsetY, landmarkFilepath, metadataFilepath, timestamp):
	return _createImageNode.delay(uuid, contentType, filepath, ijip, offsetX, offsetY, landmarkFilepath, metadataFilepath, timestamp)

def _createExperimentNode(uuid, expType, metadataFilepath, details):
	print 'not implemented locally'
	return None
_createExperimentNode.name = 'dev.gos_gia.tasks_server.createExperimentNode'
def createExperimentNode(uuid, expType, metadataFilepath, details):
	return _createExperimentNode.delay(uuid, expType, metadataFilepath, details)

def _createClusterNode(cluster, expId, temporalIdx, clusterIdx):
	print 'not implemented locally'
	return None
_createClusterNode.name = 'dev.gos_gia.tasks_server.createClusterNode'
def createClusterNode(cluster, expId, temporalIdx, clusterIdx):
	return _createClusterNode.delay(cluster, expId, temporalIdx, clusterIdx)

def _addImageStatsNode(imageNode, statsDict, statsType):
	print 'not implemented locally'
	return None
_addImageStatsNode.name = 'dev.gos_gia.tasks_server.addImageStatsNode'
def addImageStatsNode(imageNode, statsDict, statsType):
	return _addImageStatsNode(imageNode, statsDict, statsType)


def _addBlobStatsNode(imgStatsNode, blobNode, statsDict, statsType):
	print 'not implemented locally'
	return None
_addBlobStatsNode.name = 'dev.gos_gia.tasks_server.addBlobStatsNode'
def addBlobStatsNode(imgStatsNode, blobNode, statsDict, statsType):
	return _addBlobStatsNode.delay(imgStatsNode, blobNode, statsDict, statsType)

	

def _linkImageHasStats(imageNode, imgStatsNode):
	print 'not implemented locally'

_linkImageHasStats.name = 'dev.gos_gia.tasks_server.linkImageHasStats'
def linkImageHasStats(imageNode, imgStatsNode):
	_linkImageHasStats.delay(imageNode, imgStatsNode)
    
def _linkImgStatsHasBlobStats(imgStatsNode, blobStatsNode):
	print 'not implemented locally'

_linkImgStatsHasBlobStats.name = 'dev.gos_gia.tasks_server.linkImgStatsHasBlobStats'
def linkImgStatsHasBlobStats(imgStatsNode, blobStatsNode):
	_linkImgStatsHasBlobStats.delay(imgStatsNode, blobStatsNode)
    
def _linkBlobHasStats(blobNode, statsNode):
	print 'not implemented locally'

_linkBlobHasStats.name = 'dev.gos_gia.tasks_server.linkBlobHasStats'
def linkBlobHasStats(blobNode, statsNode):
	_linkBlobHasStats(blobNode, statsNode)


def _linkClusterHasBlob(clusterNode, blobNode):
	print 'not implemented locally'

_linkClusterHasBlob.name = 'dev.gos_gia.tasks_server.linkClusterHasBlob'
def linkClusterHasBlob(clusterNode, blobNode):
	_linkClusterHasBlob.delay(clusterNode, blobNode)

def _linkBlob_is_near_Blob(blob1, blob2, weight):
	print 'not implemented locally'
	
_linkBlob_is_near_Blob.name = 'dev.gos_gia.tasks_server.linkBlob_is_near_Blob'
def linkBlob_is_near_Blob(blob1, blob2, weight):
	_linkBlob_is_near_Blob.delay(blob1, blob2, weight)

def _linkBlob_subsequently_coincides_Blob(blob1, blob2, weight):
	print 'not implemented locally'
	
_linkBlob_subsequently_coincides_Blob.name = 'dev.gos_gia.tasks_server.linkBlob_subsequently_coincides_Blob'
def linkBlob_subsequently_coincides_Blob(blob1, blob2, weight):
	_linkBlob_subsequently_coincides_Blob.delay(blob1, blob2, weight)

def _linkBlobToImage(blob, image):
	print 'not implemented locally'
_linkBlobToImage.name = 'dev.gos_gia.tasks_server.linkBlobToImage'
def linkBlobToImage(blob, image):
	_linkBlobToImage.delay(blob, image)
    
def _linkBFImageToFluoImage(bfImg, fluoImg):
	print 'not implemented locally'
_linkBFImageToFluoImage.name = 'dev.gos_gia.tasks_server.linkBFImageToFluoImage'
def linkBFImageToFluoImage(bfImg, fluoImg):
	_linkBFImageToFluoImage.delay(bfImg, fluoImg)

def _linkImageToExp(image, exp):
	print 'not implemented locally'
_linkImageToExp.name = 'dev.gos_gia.tasks_server.linkImageToExp'
def linkImageToExp(image, exp):
	_linkImageToExp.delay(image, exp)

def _linkCorrespondingClusters(cluster1, cluster2):
	print 'not implemented locally'
_linkCorrespondingClusters.name = 'dev.gos_gia.tasks_server.linkCorrespondingClusters'
def linkCorrespondingClusters(cluster1, cluster2):
	_linkCorrespondingClusters.delay(cluster1, cluster2)


def _getScriptByNamespaceMethodName(graph, namespace, method):
	print 'not implemented locally'
	return None
_getScriptByNamespaceMethodName.name = 'dev.gos_gia.tasks_server.getScriptByNamespaceMethodName'
def getScriptByNamespaceMethodName(graph, namespace, method):
	return _getScriptByNamespaceMethodName.delay(graph, namespace, method)


#### #python wrapper function for a gremlin-groovy function
def _removeLinksByLabel(labelStr):
	print 'not implemented locally'
	return None
_removeLinksByLabel.name = 'dev.gos_gia.tasks_server.removeLinksByLabel'
def removeLinksByLabel(labelStr):
	return _removeLinksByLabel.delay(labelStr)

def _removeInterFrameLinksByImageId(imageId):
	print 'not implemented locally'
_removeInterFrameLinksByImageId.name = 'dev.gos_gia.tasks_server.removeInterFrameLinksByImageId'
def removeInterFrameLinksByImageId(imageId):
	_removeInterFrameLinksByImageId.delay(imageId)

def _removeIntraFrameLinksByImageId(imageId):
	print 'not implemented locally'
_removeIntraFrameLinksByImageId.name = 'dev.gos_gia.tasks_server.removeIntraFrameLinksByImageId'
def removeIntraFrameLinksByImageId(imageId):
	_removeIntraFrameLinksByImageId.delay(imageId)

def _removeBlobNodesByImageId(imageId):
	print 'not implemented locally'
_removeBlobNodesByImageId.name = 'dev.gos_gia.tasks_server.removeBlobNodesByImageId'
def removeBlobNodesByImageId(imageId):
	_removeBlobNodesByImageId.delay(imageId)

def _removeANodeAndConnectedEdgesByVertexId(nodeId):
	print 'not implemented locally'
_removeANodeAndConnectedEdgesByVertexId.name = 'dev.gos_gia.tasks_server.removeANodeAndConnectedEdgesByVertexId'
def removeANodeAndConnectedEdgesByVertexId(nodeId):
	_removeANodeAndConnectedEdgesByVertexId.delay(nodeId)
	
def _removeNodesAndConnectedEdgesByImageId(imageId):
	print 'not implemented locally'
_removeNodesAndConnectedEdgesByImageId.name = 'dev.gos_gia.tasks_server.removeNodesAndConnectedEdgesByImageId'
def removeNodesAndConnectedEdgesByImageId(imageId):
	_removeNodesAndConnectedEdgesByImageId.delay(imageId)

def _getExpNodeByImageId(imgId):
	print 'not implemented locally'
	return None
_getExpNodeByImageId.name = 'dev.gos_gia.tasks_server.getExpNodeByImageId'
def getExpNodeByImageId(imgId):
	return _getExpNodeByImageId.delay(imgId)

def _getStatNodeByUUID(uuidStr):
	print 'not implemented locally'
	return None
_getStatNodeByUUID.name = 'dev.gos_gia.tasks_server.getStatNodeByUUID'
def getStatNodeByUUID(uuidStr):
	return _getStatNodeByUUID.delay(uuidStr)

def _getLinkedImageNodeByUUID(uuidStr):
	print 'not implemented locally'
	return None
_getLinkedImageNodeByUUID.name = 'dev.gos_gia.tasks_server.getLinkedImageNodeByUUID'
def getLinkedImageNodeByUUID(uuidStr):
	return _getLinkedImageNodeByUUID.delay(uuidStr)

def _doFindConsensusCluster(idList):
	print 'not implemented locally'
	return None
_doFindConsensusCluster.name = 'dev.gos_gia.tasks_server.doFindConsensusCluster'
def doFindConsensusCluster(idList):
	return _doFindConsensusCluster.delay(idList)


def _doFindCorrespondingBlob(groupLen, idList):
	print 'not implemented locally'
	return None
_doFindCorrespondingBlob.name = 'dev.gos_gia.tasks_server.doFindCorrespondingBlob'
def doFindCorrespondingBlob(groupLen, idList):
	return _doFindCorrespondingBlob.delay(groupLen, idList)

def _removeListFromList(listA, listB):
	print 'not implemented locally'
	return None
_removeListFromList.name = 'dev.gos_gia.tasks_server.removeListFromList'
def removeListFromList(listA, listB):
	return _removeListFromList.delay(listA, listB)

def _getLinkedFluoImages(bfImgNodeId):
	print 'not implemented locally'
	return None
_getLinkedFluoImages.name = 'dev.gos_gia.tasks_server.getLinkedFluoImages'
def getLinkedFluoImages(bfImgNodeId):
	return _getLinkedFluoImages.delay(bfImgNodeId)


def _getTemporalClusterPairs(exp, temporalIdx):
	print 'not implemented locally'
	return None
_getTemporalClusterPairs.name = 'dev.gos_gia.tasks_server.getTemporalClusterPairs'
def getTemporalClusterPairs(exp, temporalIdx):
	return _getTemporalClusterPairs.delay(exp, temporalIdx)

def _getClusterTimestamp(clusterNode):
	print 'not implemented locally'
	return None
_getClusterTimestamp.name = 'dev.gos_gia.tasks_server.getClusterTimestamp'
def getClusterTimestamp(clusterNode):
	return _getClusterTimestamp.delay(clusterNode)

def _getTemporalKeyClusterChain(cId, chainLimit):
	print 'not implemented locally'
	return None
_getTemporalKeyClusterChain.name = 'dev.gos_gia.tasks_server.getTemporalKeyClusterChain'
def getTemporalKeyClusterChain(cId, chainLimit):
	return _getTemporalKeyClusterChain.delay(cId, chainLimit)

def _getTemporalClusterChain(cId, chainLimit):
	print 'not implemented locally'
	return None
_getTemporalClusterChain.name = 'dev.gos_gia.tasks_server.getTemporalClusterChain'
def getTemporalClusterChain(cId, chainLimit):
	return _getTemporalClusterChain.delay(cId, chainLimit)

def _getClusterStats(nodeId):
	print 'not implemented locally'
	return None
_getClusterStats.name = 'dev.gos_gia.tasks_server.getClusterStats'
def getClusterStats(nodeId):
	return _getClusterStats.delay(nodeId)


def _getClusterObjList(exp, temporalIdx):
	print 'not implemented locally'
	return None
_getClusterObjList.name = 'dev.gos_gia.tasks_server.getClusterObjList'
def getClusterObjList(exp, temporalIdx):
	return _getClusterObjList.delay(exp, temporalIdx)

def _getClusterList(exp, temporalIdx):
	print 'not implemented locally'
	return None
_getClusterList.name = 'dev.gos_gia.tasks_server.getClusterList'
def getClusterList(exp, temporalIdx):
	return _getClusterList.delay(exp, temporalIdx)

