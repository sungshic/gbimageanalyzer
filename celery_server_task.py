from __future__ import absolute_import

from ghostos.gbimageanalyzer.celery_server import celery

from core import dographdb as gdb
import logging
from os.path import expanduser, isfile, getmtime
import os
import json
import pickle
#from sympy.geometry import Polygon
from core.temporalCorrespondences import setClusterNodesByImgUUID, setClusterCorrespondencesByUUID, setSingleCellCorrespondencesByGBIAClusters
from shapely.geometry import Polygon
import datetime
from glob import glob
from uuid import uuid4
import cv2

# create logger with 'grabia_backend'
logger = logging.getLogger('grabia_backend.gbimageanalysis.celery_server_task')
logger.setLevel(logging.DEBUG)
# create file handler which logs even debug messages
logfile_dir = expanduser("~")+'/.grabia/log/'
logfile_name = 'grabia_backend_servertask.log'
if not os.path.exists(logfile_dir):
    os.makedirs(logfile_dir)
fh = logging.FileHandler(logfile_dir+logfile_name)
fh.setLevel(logging.DEBUG)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)
# sysloghandler for remotely logging
#syslog = logging.handlers.SysLogHandler(address=('localhost', 9999))
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
# add the handlers to the logger
logger.addHandler(fh)
logger.addHandler(ch)
logger.debug('logger initialized')



@celery.task
def doClearGDBData():
    logger.debug('entering doClearGDBData()')
    gdb.g = gdb.loadconfig('/ghostos/gdbconfig.yaml', '/ghostos/gbimageanalyzer/neo4jGremlinQueries/ListCluster.groovy')
    gdb.clearGDB()

@celery.task
def saveGDBIntoFile(exp_uuid):
    logger.debug('entering saveGDBIntoFile()')
    gdb.g = gdb.loadconfig('/ghostos/gdbconfig.yaml', '/ghostos/gbimageanalyzer/neo4jGremlinQueries/ListCluster.groovy')
    exp_uuid = json.loads(exp_uuid)
    dbfile_dir = expanduser("~")+'/.grabia/gdb/'
    if not os.path.exists(dbfile_dir):
        os.makedirs(dbfile_dir)

    gdb_filepath = dbfile_dir + str(exp_uuid) + '_' + datetime.datetime.now().strftime("%Y-%m-%d_%H:%M:%S") + '.json'

    logger.debug('Preprocessing call, saving GDB into a file: '+gdb_filepath)
    gdb.saveGDBIntoFile(gdb_filepath)

@celery.task
def loadFileToGDB(filepath):
    filepath = json.loads(filepath)
    logger.debug('Selected GDB file: ' + filepath)
    gdb.g = gdb.loadconfig('/ghostos/gdbconfig.yaml', '/ghostos/gbimageanalyzer/neo4jGremlinQueries/ListCluster.groovy')
    gdb.clearGDB() # for some reason this resets the graph handle
    #gdb.g = gdb.loadconfig('/ghostos/gdbconfig.yaml', '/ghostos/gbimageanalyzer/neo4jGremlinQueries/ListCluster.groovy')
    gdb.loadGDBFromFile(filepath)

@celery.task
def getGDBFiles():
    logger.debug('entering loadGDBFiles()')
    gdb.g = gdb.loadconfig('/ghostos/gdbconfig.yaml', '/ghostos/gbimageanalyzer/neo4jGremlinQueries/ListCluster.groovy')

    dir_base = expanduser("~") + '/.grabia/gdb/'
    gdb_filepath_list = filter(isfile, glob(dir_base + '*.json'))
    gdb_filepath_list.sort(key=getmtime, reverse=True) # sort the list by modification time
    fileinfo_list = []
    for filepath in gdb_filepath_list:
        filemtime = getmtime(filepath)
        fileinfo_list.append((filepath, filemtime))
    return json.dumps(fileinfo_list)

@celery.task
def createExperiment(exp_uuid, exp_title, metadata_filepath, exp_desc):
    logger.debug('entering createExperiment()')
    gdb.g = gdb.loadconfig('/ghostos/gdbconfig.yaml', '/ghostos/gbimageanalyzer/neo4jGremlinQueries/ListCluster.groovy')
    exp_node = gdb.createExperimentNode(json.loads(exp_uuid), json.loads(exp_title), json.loads(metadata_filepath), json.loads(exp_desc))
    return json.dumps(exp_node.map())

@celery.task
def doProcessGDBDataSingle(exp_uuid, exp_data, previmg_uuid=None):
    logger.debug('entering doProcessGDBDataSingle()')
    gdb.g = gdb.loadconfig('/ghostos/gdbconfig.yaml', '/ghostos/gbimageanalyzer/neo4jGremlinQueries/ListCluster.groovy')

    exp_uuid = json.loads(exp_uuid)
    exp_data = pickle.loads(exp_data) # unmarshalling the data
    if previmg_uuid:
        previmg_uuid = json.loads(previmg_uuid)

    # create experiment graph node
    metadata_filepath = 'tbd'
    landmark_filepath = 'tdb'
    exp_details = 'tbd'
    ####exp_uuid = exp_data['exp_uuid']
    #gdb.g = gdb.loadconfig() # need to do this for process being invoked from kivy button
    logger.debug('current gdb.g: '+str(gdb.g)+exp_uuid+metadata_filepath+exp_details)

    exp_node = gdb.getNodeByUUID(exp_uuid)
    logger.debug('doProcessGDBDataSingle: exp_node by uuid '+str(exp_uuid) + ' = ' + str(exp_node))

    #refimg_uuid = None
    #curimg_uuid = None
    #previmg_uuid = None
    img_stats = exp_data['data']
    t = exp_data['seq_idx']

    roi_stats = img_stats['roi_stats'] # roi_stats is a list of dict(polygon=roi_coordlist, ch_stats=roi_ch_statslist)
    img_info_list = img_stats['img_stats']  # img_info is a list of dict(roi_stats=roi_stats, img_stats=img_stats)
    # img_stats is a list of dict(filepath=filepath, mean=stats.mean, chname=chname, img_uuid=img_uuid, timestamp=img_timestamp)
    #  dict(filepath=filepath, mean=stats.mean, chname=chname, uuid=img_uuid, timestamp=img_timestamp, landmarkOffset=(xOff, yOff), img_size=(imgwidth, imgheight))
    bf_filepath = img_stats['bf_filepath'] # path to the bright field image

    # initialize local variables
    refimg_uuid = None


    for c, img_info in enumerate(img_info_list):
        #### create image nodes in graph db
        offset_xy = img_info['landmarkOffset']
        img_size = img_info['img_size']
        curimg_uuid = img_info['img_uuid']
        ch_name = img_info['chname']
        timestamp = img_info['timestamp']
        filepath = img_info['img_filepath']

        logger.debug('doProcessGDBData: checking for an existing imaging node: '+str(curimg_uuid))
        existing_node = gdb.getNodeByUUID(curimg_uuid)
        # imgwidth = curImp.getWidth()
        # imgheight = curImp.getHeight()
        if existing_node != None:
            logger.debug('doProcessGDBData: existing imaging node found: '+str(curimg_uuid))
            img_node = existing_node
            gdb.updateImageNode(existing_node, curimg_uuid, ch_name, filepath, img_size[0], img_size[1], offset_xy[0], offset_xy[1], landmark_filepath, metadata_filepath, timestamp)
            gdb.linkImageToExp(existing_node, exp_node) #curimg_uuid, exp_uuid)
        else:
            logger.debug('doProcessGDBData: creating an image node (uuid: ' + str(curimg_uuid) + ')')
            img_node = gdb.createImageNode(curimg_uuid, ch_name, filepath, img_size[0], img_size[1], offset_xy[0], offset_xy[1], landmark_filepath, metadata_filepath, timestamp)
            logger.debug('doProcessGDBData: an image node (uuid: ' + str(curimg_uuid) + ') = ' + str(img_node))
            gdb.linkImageToExp(img_node, exp_node) #curimg_uuid, exp_uuid)

        if c == 0:
            refimg_uuid = curimg_uuid
            bfimg_node = img_node
        elif c != 0 and existing_node == None:
            gdb.linkBFImageToFluoImage(bfimg_node, img_node) #refimg_uuid, curimg_uuid)
            # end of if
    # end of for

    logger.debug('doProcessGDBData: creating image blobs imguuid: ' + str(refimg_uuid))

    # catalogue image features in graph db
    createImageBlobs(refimg_uuid, img_info_list, roi_stats, offset_xy[0], offset_xy[1], timestamp)
    logger.debug('doProcessGDBData: processing intra frame feature relations imguuid: ' + str(refimg_uuid))
    processIntraFrameFeatureRelations(refimg_uuid, 30) #maxClusteringDist)

    # correspondence analysis between images at time t and t+1
    if previmg_uuid != None and refimg_uuid != None:
        logger.debug('doProcessGDBData: processing inter frame feature relations imguuid: ' + str(refimg_uuid))
        processInterFrameFeatureRelations(previmg_uuid, refimg_uuid)

    return json.dumps(refimg_uuid)
# end of def

@celery.task
def doProcessGDBData(exp_data):
    logger.debug('entering doProcessGDBData()')
    exp_data = pickle.loads(exp_data)
    gdb.g = gdb.loadconfig('/ghostos/gdbconfig.yaml', '/ghostos/gbimageanalyzer/neo4jGremlinQueries/ListCluster.groovy')


    # create experiment graph node
    metadata_filepath = 'tbd'
    landmark_filepath = ''
    exp_details = ''
    exp_uuid = exp_data['exp_uuid']
    #gdb.g = gdb.loadconfig() # need to do this for process being invoked from kivy button
    exp_node = gdb.createExperimentNode(exp_uuid, "Timelapse microscopy", metadata_filepath, exp_details)
    logger.debug('doProcessGDBData: exp node created')

    #refimg_uuid = None
    #curimg_uuid = None
    #previmg_uuid = None
    img_stats_list = exp_data['data']
    #import pdb; pdb.set_trace()
    previmg_uuid = None
    for t, img_stats in enumerate(img_stats_list):
        roi_stats = img_stats['roi_stats'] # roi_stats is a list of dict(polygon=roi_coordlist, ch_stats=roi_ch_statslist)
        img_info_list = img_stats['img_stats']  # img_info is a list of dict(roi_stats=roi_stats, img_stats=img_stats)
        # img_stats is a list of dict(filepath=filepath, mean=stats.mean, chname=chname, img_uuid=img_uuid, timestamp=img_timestamp)
        #  dict(filepath=filepath, mean=stats.mean, chname=chname, uuid=img_uuid, timestamp=img_timestamp, landmarkOffset=(xOff, yOff), img_size=(imgwidth, imgheight))
        bf_filepath = img_stats['bf_filepath'] # path to the bright field image

        # initialize local variables

        refimg_uuid = None

        for c, img_info in enumerate(img_info_list):
            #### create image nodes in graph db
            offset_xy = img_info['landmarkOffset']
            img_size = img_info['img_size']
            curimg_uuid = img_info['img_uuid']
            ch_name = img_info['chname']
            timestamp = img_info['timestamp']
            filepath = img_info['img_filepath']

            logger.debug('doProcessGDBData: checking for an existing imaging node: '+str(curimg_uuid))
            existing_node = gdb.getNodeByUUID(curimg_uuid)
            # imgwidth = curImp.getWidth()
            # imgheight = curImp.getHeight()
            if existing_node != None:
                logger.debug('doProcessGDBData: existing imaging node found: '+str(curimg_uuid))
                gdb.updateImageNode(existing_node, curimg_uuid, ch_name, filepath, img_size[0], img_size[1], offset_xy[0], offset_xy[1], landmark_filepath, metadata_filepath, timestamp)
                gdb.linkImageToExp(existing_node, exp_node) #curimg_uuid, exp_uuid)
            else:
                logger.debug('doProcessGDBData: creating an image node (uuid: ' + str(curimg_uuid) + ')')
                img_node = gdb.createImageNode(curimg_uuid, ch_name, filepath, img_size[0], img_size[1], offset_xy[0], offset_xy[1], landmark_filepath, metadata_filepath, timestamp)
                gdb.linkImageToExp(img_node, exp_node) #curimg_uuid, exp_uuid)

            if c == 0:
                refimg_uuid = curimg_uuid
                bfimg_node = img_node
            elif c != 0 and existing_node == None:
                gdb.linkBFImageToFluoImage(bfimg_node, img_node) #refimg_uuid, curimg_uuid)
            # end of if
        # end of for

        logger.debug('doProcessGDBData: creating image blobs imguuid: ' + str(refimg_uuid))

        # catalogue image features in graph db
        createImageBlobs(refimg_uuid, img_info_list, roi_stats, offset_xy[0], offset_xy[1], timestamp)
        logger.debug('doProcessGDBData: processing intra frame feature relations imguuid: ' + str(refimg_uuid))
        processIntraFrameFeatureRelations(refimg_uuid, 30) #maxClusteringDist)

        # correspondence analysis between images at time t and t+1
        if previmg_uuid != None and refimg_uuid != None:
            logger.debug('doProcessGDBData: processing inter frame feature relations imguuid: ' + str(refimg_uuid))
            processInterFrameFeatureRelations(previmg_uuid, refimg_uuid)

        if refimg_uuid != None:
            previmg_uuid = refimg_uuid
    # end of for
# end of def

#@celery.task
def processInterFrameFeatureRelations(imgt1_uuid, imgt2_uuid):
    # imgt1_uuid = json.loads(imgt1_uuid)
    # imgt2_uuid = json.loads(imgt2_uuid)
    #gdb.g = gdb.loadconfig('/ghostos/gdbconfig.yaml', '/ghostos/gbimageanalyzer/neo4jGremlinQueries/ListCluster.groovy')

    # set temporal correspondence relationships between cluster at time t1 to those at time t2
    gbiaclist1, gbiaclist2 = setClusterCorrespondencesByUUID(gdb, imgt1_uuid, imgt2_uuid) #expId, imgNodeAt_t1.timestamp)
    setSingleCellCorrespondencesByGBIAClusters(gbiaclist1, gbiaclist2)
# end of def


#@celery.task
def processIntraFrameFeatureRelations(refimg_uuid, max_clustering_distance):
    # refimg_uuid = json.loads(refimg_uuid)
    #gdb.g = gdb.loadconfig('/ghostos/gdbconfig.yaml', '/ghostos/gbimageanalyzer/neo4jGremlinQueries/ListCluster.groovy')

    img_node = gdb.getNodeByUUID(refimg_uuid)

    blobs = list(img_node.inV('belongsToImage')) # get all blobs that belong to this imgNode

    cmpblobs = list(blobs)
    logger.debug('number of blobs to be processed: '+str(len(cmpblobs)))
    for blob1 in blobs:
        cmpblobs.remove(blob1)
        logger.debug('blob to be processed: '+str(blob1))
        # deserialize the blob's polygonal coordinate points
        #roi1Coords = zip(*geojson.loads(blob1.roi)['coordinates']) # put xCoords in idx 0 yCoords in idx 1
        #roi1coordlist = zip(roi1Coords[0],roi1Coords[1])
        #import pdb; pdb.set_trace()
        #roi1coordlist = json.loads(blob1.roi)
        #roi1 = PolygonRoi(Polygon(roi1Coords[0], roi1Coords[1], len(roi1Coords[0])), Roi.POLYGON)
        roi1 = Polygon(json.loads(blob1.roi)) #roi1coordlist)
        for blob2 in cmpblobs: # [blob for blob in blobs if blob != blob1]:
            #roi2Coords = zip(*geojson.loads(blob2.roi)['coordinates']) # put xCoords in idx 0 yCoords in idx 1
            #roi2coordlist = zip(roi2Coords[0],roi2Coords[1])
            #roi2coordlist = json.loads(blob2.roi)
            #roi2 = PolygonRoi(Polygon(roi2Coords[0], roi2Coords[1], len(roi2Coords[0])), Roi.POLYGON)
            roi2 = Polygon(json.loads(blob2.roi)) #roi2coordlist)

            #blob1Rect = roi1.getBounds()
            #blob2Rect = roi2.getBounds()
            #blob1Rect = roi1.bounds
            #blob2Rect = roi2.bounds
            ###unionRect = roi1.union(roi2).bounds #blob1Rect.createUnion(blob2Rect)

            #polygonUnionBounds = roi1.union(roi2).envelope.bounds

            # calculate the geometrical proximity measure between two rectangles
            #areaProximity = (unionRect.getWidth() * unionRect.getHeight()) - (blob1Rect.getWidth() * blob1Rect.getHeight() + blob2Rect.getWidth() * blob2Rect.getHeight())
            #areaProximity = _getAreaFromPolygonBounds(polygonUnionBounds) - ( _getAreaFromPolygonBounds(roi1.bounds) + _getAreaFromPolygonBounds(roi2.bounds) )

            #polygon_distance = roi1.distance(roi2) sympy returns error on complex polygons
            #polygon_distance = roi1.centroid.distance(roi2.centroid) # sympy methods
            polygon_distance = roi1.distance(roi2)
            # print 'Area proximity: ' + str(areaProximity)
            #pu = [blob1Rect.getX(), blob1Rect.getY()]
            #pv = [blob2Rect.getX(), blob2Rect.getY()]
            #dist = float(math.sqrt(sum(((a-b)**2 for a,b in zip(pu,pv))))) # find the distance between points pu & pv

            if (polygon_distance <= max_clustering_distance):
                gdb.linkBlob_is_near_Blob(blob1, blob2, float(polygon_distance))
                gdb.linkBlob_is_near_Blob(blob2, blob1, float(polygon_distance))

        # end of for
    # end of for

    # set cluster nodes
    setClusterNodesByImgUUID(gdb, refimg_uuid, max_clustering_distance)
# end of def

# roi_stats is a list of dict(polygon=roi_coordlist, ch_stats=roi_ch_statslist)
# img_info_list is a list of dict(filepath=filepath, mean=stats.mean, chname=chname, uuid=img_uuid, timestamp=img_timestamp, landmarkOffset=(xOff, yOff), img_size=(imgwidth, imgheight))
# raw_bf_img is a image data as read by cv2
#@celery.task
def createImageBlobs(refimg_uuid, img_info_list, roi_stats, xoff, yoff, timestamp):

    # refimg_uuid = json.loads(refimg_uuid)
    # img_info_list = json.loads(img_info_list)
    # roi_stats = json.loads(roi_stats)
    #gdb.g = gdb.loadconfig('/ghostos/gdbconfig.yaml', '/ghostos/gbimageanalyzer/neo4jGremlinQueries/ListCluster.groovy')

    logger.debug('image node uuid: ' + str(refimg_uuid))

    fluoimg_info_list = []
    # gather some general stats on fluorescent images
    #img_stats_list = [item['mean'] for item in img_info_list]
    #for img_node in fluoimg_nodes:
    for img_info in img_info_list: #img_stats_list:
        #logger.debug('fluoImg Filepath:' + img_node.filepath)
        # create a graph node for image level statistics
        #stats = imp.getStatistics(Measurements.MEAN)
        marshalled_stats = pickle.dumps(img_info['mean'])#pickle.dumps(dict(mean=img_stats))
        chname = img_info['chname']

        image_node = gdb.getNodeByUUID(refimg_uuid)
        stats_uuid = gdb.issueUUID() # generate a unique id for the stat
        img_stats_node = gdb.addImageStatsNode(image_node, stats_uuid, marshalled_stats, chname)
        fluoimg_info_list.append(dict(img_node=image_node, content_type=image_node.contentType, stats_node=img_stats_node))

    # process all ROIs
    # roi_stats is a list of dict(polygon=roi_coordlist, ch_stats=roi_ch_statslist)
    # ch_stats is a list of dict(ch = chname_list[idx] stats = dict(mean=stats.mean, median=stats.median, area=stats.area, std_dev=stats.stdDev))
    for roistat in roi_stats: #roiM.getRoisAsArray():
        #print('DEBUG printing roi polygon:')
        coordlist = roistat['polygon'] #roi.getPolygon()
        #import pdb; pdb.set_trace()
        #coordlist = zip(p.xpoints, p.ypoints)
        statslist = roistat['ch_stats']

        # add to graph db
        blob_uuid = gdb.issueUUID() # generate a unique id for the stat
        image_node = gdb.getNodeByUUID(refimg_uuid)
        #fluoimg_node = gdb.getLinkedImageNodeByUUID(refimg_uuid)

        blob_node = gdb.createBlobNode(blob_uuid, xoff, yoff, coordlist, timestamp)
        gdb.linkBlobToImage(blob_node, image_node)

        # for each channel and associated stats, add blob-stat-node
        for idx, chstat in enumerate(statslist):
            img_stats_node = fluoimg_info_list[idx]['stats_node']
            #import pdb; pdb.set_trace()
            gdb.addBlobStatsNode(img_stats_node, blob_node, chstat['stats'], chstat['ch'])
    # end for
# end of def

@celery.task
def getMotherGenerationCells():
    logger.debug('entering getMotherGenerationCells()')
    gdb.g = gdb.loadconfig('/ghostos/gdbconfig.yaml', '/ghostos/gbimageanalyzer/neo4jGremlinQueries/ListCluster.groovy')
    mcells = gdb.getMotherGenerationCells()
    return json.dumps(mcells)


@celery.task
def getCellsWithStats():
    logger.debug('entering getCellsWithStats()')
    gdb.g = gdb.loadconfig('/ghostos/gdbconfig.yaml', '/ghostos/gbimageanalyzer/neo4jGremlinQueries/ListCluster.groovy')
    statcells = gdb.getCellsWithStats()
    celldictlist = []
    for cell in statcells:
        celldictlist.append({'uuid':cell.uuid})
    return json.dumps(celldictlist)

@celery.task
def getCellStats(cur_gen):
    logger.debug('entering getCellStats()')
    cur_gen = json.loads(cur_gen)
    gdb.g = gdb.loadconfig('/ghostos/gdbconfig.yaml', '/ghostos/gbimageanalyzer/neo4jGremlinQueries/ListCluster.groovy')
    statcells = gdb.getCellStats(cur_gen)
    cur_gen_stat_vertices = gdb.getCellStats(cur_gen)
    stats_lt = {}
    for cur_v in cur_gen_stat_vertices:
        cuuid = list(cur_v.inV('blob_has_stats'))[0].uuid
        stats_lt[cuuid] = cur_v.data()
    return json.dumps(stats_lt)

@celery.task
def getNextGenerationCells(cuuid_list):
    cuuid_list = json.loads(cuuid_list) # un-marshal
    logger.debug('entering getNextGenerationCells()')
    gdb.g = gdb.loadconfig('/ghostos/gdbconfig.yaml', '/ghostos/gbimageanalyzer/neo4jGremlinQueries/ListCluster.groovy')
    nth_gen_clist = gdb.getNextGenerationCells(cuuid_list)
    return json.dumps(nth_gen_clist)

@celery.task
def getCNodeByUUID(uuid):
    logger.debug('entering getNodeByUUID()')
    gdb.g = gdb.loadconfig('/ghostos/gdbconfig.yaml', '/ghostos/gbimageanalyzer/neo4jGremlinQueries/ListCluster.groovy')
    cnode = gdb.getNodeByUUID(json.loads(uuid))
    return json.dumps(cnode.map())

@celery.task
def getSubCNodeList(cnode_uuid, weightlimit):
    logger.debug('entering getSubCNodeList()')
    gdb.g = gdb.loadconfig('/ghostos/gdbconfig.yaml', '/ghostos/gbimageanalyzer/neo4jGremlinQueries/ListCluster.groovy')
    cnode_uuid = json.loads(cnode_uuid)
    cnode = gdb.getNodeByUUID(cnode_uuid)
    cur_edge_list = cnode.outE('at_t1_temporally_corresponds_at_t2')
    if cur_edge_list:
        sub_cnode_list = [(e.inV(), e.weight) for e in cur_edge_list if e.weight < weightlimit]
    else:
        sub_cnode_list = []
    return json.dumps([(cn.map(), ew) for cn, ew in sub_cnode_list])

@celery.task
def getImageNodeByBlobUUID(buuid):
    logger.debug('entering getImageNodeByBlobUUID('+str(buuid)+')')
    gdb.g = gdb.loadconfig('/ghostos/gdbconfig.yaml', '/ghostos/gbimageanalyzer/neo4jGremlinQueries/ListCluster.groovy')
    imgnode = gdb.getImageNodeByBlobUUID(json.loads(buuid))
    return json.dumps(imgnode.map())


@celery.task
def getCellStatsByUUID(uuid):
    logger.debug('entering getCellStatsByUUID('+str(uuid)+')')
    gdb.g = gdb.loadconfig('/ghostos/gdbconfig.yaml', '/ghostos/gbimageanalyzer/neo4jGremlinQueries/ListCluster.groovy')
    cellstats = gdb.getCellStatsByUUID(json.loads(uuid))
    return json.dumps([cell.map() for cell in cellstats])


@celery.task
def getTemporalCellChainsByInitCellUUIDs(uuid_list, chain_limit):
    logger.debug('entering getTemporalCellChainsByInitCellUUIDs('+str(uuid_list)+','+str(chain_limit)+')')
    gdb.g = gdb.loadconfig('/ghostos/gdbconfig.yaml', '/ghostos/gbimageanalyzer/neo4jGremlinQueries/ListCluster.groovy')
    uuid_list = json.loads(uuid_list)
    cell_chains = gdb.getTemporalCellChainsByInitCellUUIDs(uuid_list, chain_limit)
    return json.dumps(cell_chains)


@celery.task
def getBlobUUIDsByExpImgIdx(exp_uuid, temporalidx):
    logger.debug('entering getBlobUUIDsByExpImgIdx('+str(exp_uuid)+','+str(temporalidx)+')')
    gdb.g = gdb.loadconfig('/ghostos/gdbconfig.yaml', '/ghostos/gbimageanalyzer/neo4jGremlinQueries/ListCluster.groovy')
    cells_list = gdb.getBlobsByExpImgIdx(json.loads(exp_uuid),temporalidx)
    uuid_list = [cnode.uuid for cnode in cells_list]
    return json.dumps(uuid_list)


@celery.task
def cv2EqualizeHistAndCvtGrayToRGB(img):
    logger.debug('entering cv2EqualizeHistAndCvtGrayToRGB()')
    img = pickle.loads(img)
    img_eq = cv2.equalizeHist(img)
    rgb_img = cv2.cvtColor(img_eq, cv2.COLOR_GRAY2RGB)
    return pickle.dumps(rgb_img)

@celery.task
def cv2CropImg(img, pt1, pt2, frame_color, frame_thickness):
    logger.debug('entering cv2CropImg()')
    img = pickle.loads(img)
    img_eq = cv2.equalizeHist(img)
    rgb_img = cv2.cvtColor(img_eq, cv2.COLOR_GRAY2RGB)
    cv2.rectangle(rgb_img, pt1, pt2, frame_color, frame_thickness)
    return pickle.dumps(rgb_img)

@celery.task
def cv2ImDecode(img_buf):
    logger.debug('entering cv2ImDecode()')
    img_buf = pickle.loads(img_buf)
    decoded_img = cv2.imdecode(img_buf, cv2.IMREAD_GRAYSCALE) # the argument needs to be more generalized down the road.
    return pickle.dumps(decoded_img)

import networkx as nx
####################
# methods requiring access to dot library
####################
@celery.task
def nxGetGraphvizDotLayout(graph_data):
    logger.debug('entering nxGetGraphvizDotLayout()')
    graph_data = pickle.loads(graph_data)
    layout_pos = nx.graphviz_layout(graph_data, prog='dot')
    return pickle.dumps(layout_pos)


