from __future__ import absolute_import

from celery import Celery

#celery = Celery(main='ghostos.mpa_node.celery', broker='amqp://guest@localhost:5672', backend='amqp', include=['celery_client_task'])
#celery = Celery(main='ghostos.gbimageanalyzer.celery_server', broker='pyamqp://guest@localhost', backend='rpc://')#, include=['celery_client_task'])
celery = Celery(main='ghostos.gbimageanalyzer.celery_server', broker='pyamqp://grabia:grabia@localhost', backend='rpc://', include=['celery_client_task'])

# Optional configuration, see the application user guide.
celery.conf.update(
    CELERY_TASK_RESULT_EXPIRES=3600,
)

# if __name__ == '__main__':
#     celery.start()

@celery.task
def _add(x,y):
    return None

_add.name = 'ghostos.gbimageanalyzer.celery_server.add'

def add(x,y):
    return _add.delay(x,y)
    
