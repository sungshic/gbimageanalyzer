__author__ = 'spark'

from celery.bin.worker import worker
import amqp
import kombu.transport.pyamqp

from billiard import freeze_support
import billiard.reduction
#import billiard.process, billiard.reduction, billiard.py2.reduction
import sys
#from cobra.core.DictList import DictList
import fractions
from kombu.utils.encoding import default_encoding

print 'default sys encoding: ' +  str(sys.getfilesystemencoding())
print 'kombu default encoding: ' + str(default_encoding(None))

print 'sys.modules[billiard.process]: ' + str(sys.modules['billiard.process'])
#print 'sys.modules[cobra.core.DictList]: ' + str(sys.modules['cobra.core.DictList'])


import os
print 'listing sys.path'
# sys.path can be dirty and in unicode! :-p
sys_path = [str(d) for d in sys.path if os.path.isdir(d)]

prefix = None
#print "listing current sys.path"
for d in [p + "/graph_tool" for p in sys_path]:
        print d
        if os.path.exists(d):
                print 'existent'
        else:
                print 'non-existent'

print 'launching a worker process: ghostos.gbimageanalyzer.tasks'
freeze_support()
w1 = worker(app='ghostos.gbimageanalyzer.celery_server_task')
w1.execute_from_commandline()

print 'end of program'
