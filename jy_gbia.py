# set virtual env
#activate_env_file = '/Users/spark/pyvirtualenv/env/bin/activate_this.py'
#execfile(activate_env_file, dict(__file__=activate_env_file))

import sys

#sys.path.insert(0, '/Users/spark/pyvirtualenv/env/python2.7/site-packages/')
#sys.path.append('/Applications/Fiji.app/jars/ij-1.47q.jar')
sys.path.append('/grabia/gbimageanalyzer/lib/ij.jar')

# jhep related libs
sys.path.append('/grabia/gbimageanalyzer/lib/scavis/python/packages')
sys.path.append('/grabia/gbimageanalyzer/lib/scavis/jehep.jar')
#sys.path.append('gbimageanalyzer/lib/scavis/lib/system/jhplot.jar')
#sys.path.append('gbimageanalyzer/lib/scavis/lib/system/jhplot_utils.jar')
#sys.path.append('gbimageanalyzer/lib/scavis/lib/system/colt.jar')
#sys.path.append('gbimageanalyzer/lib/scavis/lib/system/jconvert.jar')
#sys.path.append('gbimageanalyzer/lib/scavis/lib/system/carmetal.jar')
#sys.path.append('gbimageanalyzer/lib/scavis/lib/system/jminhep.jar')
#sys.path.append('gbimageanalyzer/lib/scavis/lib/system/sharptools.jar')
#sys.path.append('gbimageanalyzer/lib/scavis/lib/system/cambria.jar')
#sys.path.append('gbimageanalyzer/lib/scavis/lib/system/exp4j.jar')
#sys.path.append('gbimageanalyzer/lib/scavis/lib/system/jscl-math.jar')
#sys.path.append('gbimageanalyzer/lib/scavis/lib/system/JamaPack.jar')
#sys.path.append('gbimageanalyzer/lib/scavis/lib/system/jfreechart-1.0.14.jar')
#sys.path.append('gbimageanalyzer/lib/scavis/lib/freehep/freehep-graphics2d-3.5.jar')
#sys.path.append('gbimageanalyzer/lib/scavis/lib/freehep/freehep-aid-3.5.jar')
#sys.path.append('gbimageanalyzer/lib/scavis/lib/freehep/freehep-jaida-3.5.jar')
#sys.path.append('gbimageanalyzer/lib/scavis/lib/freehep/aida-3.5.jar')
#sys.path.append('gbimageanalyzer/lib/scavis/lib/freehep/freehep-io-3.5.jar')
#sys.path.append('gbimageanalyzer/lib/scavis/lib/freehep/freehep-rootio-3.5.jar')
#sys.path.append('gbimageanalyzer/lib/scavis/lib/freehep/freehep-util-3.5.jar')
#sys.path.append('gbimageanalyzer/lib/scavis/lib/freehep/freehep-graphicsio-pdf-3.5.jar')

sys.path.append('/grabia/gbimageanalyzer/core/')

#from jhplot import *

from java.util import Random
from java.awt import Polygon, Color
from ij import IJ, ImageStack, ImagePlus
from ij.gui import Roi, PolygonRoi, Overlay

#from bulbs.neo4jserver import Graph

import getopt
import logging

import os
from os.path import expanduser
# create logger with 'grabia_app'
logger = logging.getLogger('grabia_app.gbimageanalyzer')
logger.setLevel(logging.DEBUG)
# create file handler which logs even debug messages
logfile_dir = expanduser("~")+'/.grabia/log/'
logfile_name = 'grabia_app_jy.log'
if not os.path.exists(logfile_dir):
    os.makedirs(logfile_dir)
fh = logging.FileHandler(logfile_dir+logfile_name)
fh.setLevel(logging.DEBUG)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
# add the handlers to the logger
logger.addHandler(fh)
logger.addHandler(ch)
logger.debug('logger initialized')

#logging.getLogger('').handlers = []
#logging.basicConfig(filename="testRuns.log", filemode="w", level=logging.DEBUG)
#logging.basicConfig(filename="testRuns.log", filemode="w", level=logging.INFO)
#logging.debug('This message should go to the log file')

import GraphBasedImageAnalysisManager as gbm
import re

def main():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hr:d:l:", ["help", "range", "dir", "landmark"])
    except getopt.GetoptError, err:
        print str(err)
        #usage()
        sys.exit(2)

    #baseDir = '/Volumes/lupin64/workspace/gbimageanalyzer/data/221015_2310_sample2_Pos1/'
    #landmarkFilename = 'landmark.tif'
    base_dir = None
    landmark_filename = 'landmark.tif'
    fromIdx = 0
    toIdx = 0
    #print opts
    for opt, o_val in opts:
        logger.debug('argument: ' + opt + ' ' + o_val)
        if opt in ("-h", "-help"):
            #usage()
            sys.exit()
        elif opt in ("-r", "-range"):
            #print 'range...'
            range_str = re.findall('(\d*):(\d*)', o_val)
            from_str = None
            to_str = None
            if range_str:
                from_str = range_str[0][0]
                to_str = range_str[0][1]

            if from_str:
                fromIdx = int(from_str)
            else:
                fromIdx = 0

            if to_str:
                toIdx = int(to_str)
            else:
                toIdx = 0
        elif opt in ("-d", "-dir"):
            #print 'dir...'
            base_dir = o_val
        elif opt in ("-l", "-landmark"):
            landmark_filename = o_val
        else:
            assert False, "unhandled option"
    
    
    if base_dir:
        #print base_dir, landmark_filename, fromIdx, toIdx
        gbiam = gbm.GraphBasedImageAnalysisManager()
        #gbiam.processImageAnalysisSet(base_dir, landmark_filename, fromIdx, toIdx, 30)
        result = gbiam.getImageStatsForAnalysis(base_dir, landmark_filename, fromIdx, toIdx, 30)
        #print result

    #baseDir = '/Volumes/AMPHIBIAN/workspace/timelapse images/040413/ecoli_rrnBP1gfp__1/Pos2/'
    #baseDir = '/Users/spark/Documents/workspace/image_analysis/data/090413_Ecoli_rrnBP1_gfp_Pos1/Pos1/'
    #baseDir = '../data/090413_Ecoli_rrnBP1_gfp_Pos1/Pos1/'
    #landmarkFilename = 'landmark2.tif'
    #baseDir = '../data/sample__5/Pos5/'
    #baseDir = '../data/221015/'
    #baseDir = '/Volumes/AMPHIBIAN/workspace/timelapse images/050213/ecoli__7/Pos2/'
    #baseDir = '/Volumes/AMPHIBIAN/workspace/timelapse images/020413/ecoli_rrnBP1gfp__1/Pos0/'
    #fromIdx = 0
    #toIdx = 15
    #toIdx = 4


    #gbIAM.processImageAnalysisSetIJOnly(baseDir, fromIdx, toIdx, False)
    #gbIAM.processImageLandmarkAlignment(baseDir, fromIdx, toIdx)

    #print 'end of script'

if __name__ == "__main__":
    main()
